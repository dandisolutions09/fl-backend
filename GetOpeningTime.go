package main

import (
	"fmt"
	"time"
)

//import "time"


func getTodayAvailability(partner Partner) (openingTime, closingTime string, err error) {
    // Get the Africa/Nairobi time zone
    africaNairobi := time.FixedZone("Africa/Nairobi", 3*60*60)

    // Get the current time in the Africa/Nairobi time zone
    now := time.Now().In(africaNairobi)

    // Get today's day of the week
    today := now.Weekday().String()

    // Check if the partner has availability for today
    var day Time
    switch today {
    case "Monday":
        day = partner.PartnerAvailability.Monday
    case "Tuesday":
        day = partner.PartnerAvailability.Tuesday
    case "Wednesday":
        day = partner.PartnerAvailability.Wednesday
    case "Thursday":
        day = partner.PartnerAvailability.Thursday
    case "Friday":
        day = partner.PartnerAvailability.Friday
    case "Saturday":
        day = partner.PartnerAvailability.Saturday
    case "Sunday":
        day = partner.PartnerAvailability.Sunday
    default:
        return "", "", fmt.Errorf("no availability set for today (%s)", today)
    }

    return day.OpeningTime, day.ClosingTime, nil
}