package main

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
)

func isEmailTaken(email string) bool {
	// Implement logic to check if the email already exists in the database
	// Example:
	collection := client.Database(dbName).Collection(partner_collection)
	count, err := collection.CountDocuments(context.Background(), bson.M{"email": email})
	if err != nil {
		fmt.Println("Error checking email:", err)
		return true // Assume email is taken in case of error
	}
	return count > 0
}

// func isEmailTaken(email string) bool {
// 	collection := client.Database(dbName).Collection(partner_collection)

// 	// Use a case insensitive regex for the email
// 	filter := bson.M{"email": bson.M{"$regex": "^" + email + "$", "$options": "i"}}

// 	count, err := collection.CountDocuments(context.Background(), filter)
// 	if err != nil {
// 		fmt.Println("Error checking email:", err)
// 		return true // Assume email is taken in case of error
// 	}
// 	return count > 0
// }

func isAdminEmailTaken(email string) bool {
	// Implement logic to check if the email already exists in the database
	// Example:
	collection := client.Database(dbName).Collection(admin_collection)
	count, err := collection.CountDocuments(context.Background(), bson.M{"email": email})
	if err != nil {
		fmt.Println("Error checking email:", err)
		return true // Assume email is taken in case of error
	}
	return count > 0
}

func isCustomerEmailTaken(email string) bool {
	// Implement logic to check if the email already exists in the database
	// Example:
	collection := client.Database(dbName).Collection(customer_collection)
	count, err := collection.CountDocuments(context.Background(), bson.M{"email": email})
	if err != nil {
		fmt.Println("Error checking email:", err)
		return true // Assume email is taken in case of error
	}
	return count > 0
}
