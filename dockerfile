

FROM golang:1.21.4

WORKDIR /app

COPY go.mod .
COPY go.sum .
COPY .env .


# Copy all source files
COPY *.go ./

# If you have additional resources, copy them as well
# COPY templates/ ./templates
# COPY config.json ./

RUN go mod tidy

RUN go build -o bin

ENTRYPOINT [ "/app/bin" ]
