package main

import (
	"fmt"
	"time"
)

func ConvertToNairobiTime() string {
	currentTime := time.Now()

	if currentTime.Location().String() != "Africa/Nairobi" {

		fmt.Println("Time is not Nairobi Time")
		nairobiLocation, err := time.LoadLocation("Africa/Nairobi")
		if err != nil {
			fmt.Println("Error loading Nairobi location:", err)
			return ""
		}

		// Convert the current time to Nairobi time zone
		currentTime = currentTime.In(nairobiLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")
		fmt.Println("Current Time (After):->", timeString)
		return timeString
	}
	return ""
}

// func ConvertToNairobiTime() time.Time {
// 	currentTime := time.Now()

// 	if currentTime.Location().String() != "Africa/Nairobi" {

// 		fmt.Println("Time is not Nairobi Time")
// 		nairobiLocation, err := time.LoadLocation("Africa/Nairobi")
// 		if err != nil {
// 			fmt.Println("Error loading Nairobi location:", err)
// 			return time.Time{}
// 		}

// 		// Convert the current time to Nairobi time zone
// 		currentTime = currentTime.In(nairobiLocation)
// 		timeString := currentTime.Format("2006-01-02 15:04:05")
// 		fmt.Println("Current Time (After):", timeString)

// 		return currentTime
// 	}
// 	return currentTime
// }
