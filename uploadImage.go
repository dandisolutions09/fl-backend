package main

import (
	//"bytes"
	"bytes"
	"image"
	"image/jpeg"

	// "image/jpeg"
	"image/png"

	//"context"
	"encoding/json"
	"fmt"
	"io"

	//	"io/ioutil"
	"log"

	//"log"
	"net/http"
	"strings"

	//"go.mongodb.org/mongo-driver/mongo"
	"github.com/gorilla/mux"
	//	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

// func uploadProfilePic(w http.ResponseWriter, r *http.Request) {
// 	// Parse the multipart form data
// 	err := r.ParseMultipartForm(10 << 20) // 10MB max size
// 	if err != nil {
// 		http.Error(w, "Failed to parse multipart form", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve the uploaded file
// 	file, fileHeader, err := r.FormFile("profile")
// 	if err != nil {
// 		http.Error(w, "Failed to retrieve uploaded file", http.StatusBadRequest)
// 		return
// 	}
// 	defer file.Close()

// 	// Read the contents of the uploaded file
// 	data, err := io.ReadAll(file)
// 	if err != nil {
// 		http.Error(w, "Failed to read file contents", http.StatusInternalServerError)
// 		return
// 	}

// 	// MongoDB connection URI
// 	//uri := "mongodb://localhost:27017"

// 	// Connect to MongoDB
// 	//client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(uri))

// 	// Open GridFS bucket
// 	database := client.Database("profile-bucket")
// 	bucket, err := gridfs.NewBucket(database)
// 	if err != nil {
// 		http.Error(w, "Failed to open GridFS bucket", http.StatusInternalServerError)
// 		return
// 	}

// 	// Upload the image to MongoDB GridFS
// 	uploadStream, err := bucket.OpenUploadStream(fileHeader.Filename)

// 	if err != nil {
// 		fmt.Println("ERRO", err)
// 		http.Error(w, "Failed to open GridFS upload stream", http.StatusInternalServerError)
// 		return
// 	}
// 	defer uploadStream.Close()

// 	_, err = uploadStream.Write(data)
// 	if err != nil {
// 		http.Error(w, "Failed to write file to GridFS", http.StatusInternalServerError)
// 		return
// 	}

// 	fileId, _ := json.Marshal(uploadStream.FileID)
// 	if err != nil {
// 		log.Fatal(err)
// 		//.JSON(http.StatusBadRequest, err.Error())
// 		return

// 	}

// 	//fmt.Println("Retrieved file", fileId)

// 	pic_id := strings.TrimSpace(string(fileId))

// 	fileIds := strings.Trim(pic_id, `"`)

// 	BASE_URL := "https://finelooks-backend-service.onrender.com"

// 	imageURL := BASE_URL + "/profile-view/" + fileIds

// 	// Respond with the image URL
// 	//w.WriteHeader(http.StatusOK)
// 	//w.Write([]byte(imageURL))

// 	responseBody := ImageUploadResponse{
// 		Message:  "Image Upload successfully",
// 		ImageURL: imageURL,
// 		Status:   "1",
// 	}

// 	// Respond with the image URL
// 	//w.WriteHeader(http.StatusOK)
// 	//w.Write([]byte(imageURL))
// 	//w.Write([]byte(responseBody))
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

// UploadLogoPic handles the image upload and resizing
func uploadProfilePic(w http.ResponseWriter, r *http.Request) {
	// Parse the multipart form data
	err := r.ParseMultipartForm(10 << 20) // 10MB max size
	if err != nil {
		http.Error(w, "Failed to parse multipart form", http.StatusBadRequest)
		return
	}

	// Retrieve the uploaded file
	file, fileHeader, err := r.FormFile("profile")
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to retrieve uploaded file",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
	defer file.Close()

	// Decode the image
	img, format, err := image.Decode(file)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to decode image",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Resize the image to width 800 while preserving aspect ratio
	resizedImg := ResizeImage(img, 800, 0)

	// Create a buffer to hold the resized image
	var buf bytes.Buffer

	// Encode the resized image into the buffer based on the original format
	switch format {
	case "jpeg":
		err = jpeg.Encode(&buf, resizedImg, nil)
	case "png":
		err = png.Encode(&buf, resizedImg)
	default:
		err = fmt.Errorf("unsupported image format: %s", format)
	}
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to encode resized image",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Connect to MongoDB
	//client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to connect to MongoDB",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Open GridFS bucket
	database := client.Database("profile-bucket")
	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to open GridFS bucket",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Upload the resized image to MongoDB GridFS
	uploadStream, err := bucket.OpenUploadStream(fileHeader.Filename)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to open GridFS upload stream",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
	defer uploadStream.Close()

	_, err = io.Copy(uploadStream, &buf)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to write resized image to GridFS",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	fileId, _ := json.Marshal(uploadStream.FileID)
	if err != nil {
		log.Fatal(err)
		responseBody := ErrorResponse{
			Message: "Failed to marshal fileID",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	picID := strings.TrimSpace(string(fileId))
	fileIDs := strings.Trim(picID, `"`)

	BASE_URL := "https://api.finelooks.co.uk"
	imageURL := BASE_URL + "/profile-view/" + fileIDs

	responseBody := ImageUploadResponse{
		Message:  "Image uploaded successfully",
		ImageURL: imageURL,
		Status:   "Success",
	}

	// Respond with the image URL
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// UploadLogoPic handles the image upload and resizing
func uploadLogoPic(w http.ResponseWriter, r *http.Request) {
	// Parse the multipart form data
	err := r.ParseMultipartForm(10 << 20) // 10MB max size
	if err != nil {
		http.Error(w, "Failed to parse multipart form", http.StatusBadRequest)
		return
	}

	// Retrieve the uploaded file
	file, fileHeader, err := r.FormFile("logo")
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to retrieve uploaded file",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
	defer file.Close()

	// Decode the image
	img, format, err := image.Decode(file)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to decode image",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Resize the image to width 800 while preserving aspect ratio
	resizedImg := ResizeImage(img, 800, 0)

	// Create a buffer to hold the resized image
	var buf bytes.Buffer

	// Encode the resized image into the buffer based on the original format
	switch format {
	case "jpeg":
		err = jpeg.Encode(&buf, resizedImg, nil)
	case "png":
		err = png.Encode(&buf, resizedImg)
	default:
		err = fmt.Errorf("unsupported image format: %s", format)
	}
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to encode resized image",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Connect to MongoDB
	//client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to connect to MongoDB",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Open GridFS bucket
	database := client.Database("logo-bucket")
	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to open GridFS bucket",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Upload the resized image to MongoDB GridFS
	uploadStream, err := bucket.OpenUploadStream(fileHeader.Filename)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to open GridFS upload stream",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
	defer uploadStream.Close()

	_, err = io.Copy(uploadStream, &buf)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Failed to write resized image to GridFS",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	fileId, _ := json.Marshal(uploadStream.FileID)
	if err != nil {
		log.Fatal(err)
		responseBody := ErrorResponse{
			Message: "Failed to marshal fileID",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	picID := strings.TrimSpace(string(fileId))
	fileIDs := strings.Trim(picID, `"`)

	BASE_URL := "https://api.finelooks.co.uk"
	imageURL := BASE_URL + "/logo-view/" + fileIDs

	responseBody := ImageUploadResponse{
		Message:  "Image uploaded successfully",
		ImageURL: imageURL,
		Status:   "Success",
	}

	// Respond with the image URL
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// func uploadLogoPic(w http.ResponseWriter, r *http.Request) {
// 	// Parse the multipart form data
// 	err := r.ParseMultipartForm(10 << 20) // 10MB max size
// 	if err != nil {
// 		http.Error(w, "Failed to parse multipart form", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve the uploaded file
// 	file, fileHeader, err := r.FormFile("logo")
// 	if err != nil {
// 		responseBody := ErrorResponse{
// 			Message: "Failed to retrieve uploaded file",
// 			Status:  "Error",
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}
// 	defer file.Close()

// 	// Decode the image
// 	img, _, err := image.Decode(file)
// 	if err != nil {
// 		fmt.Println("error", err)
// 		responseBody := ErrorResponse{
// 			Message: "Failed to decode image",
// 			Status:  "Error",
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Resize the image to width 800 while preserving aspect ratio
// 	resizedImg := ResizeImage(img, 800, 0)

// 	// Create a buffer to hold the resized image
// 	var buf io.Writer
// 	buf = &strings.Builder{}
// 	err = jpeg.Encode(buf, resizedImg, nil)
// 	if err != nil {
// 		responseBody := ErrorResponse{
// 			Message: "Failed to encode resized image",
// 			Status:  "Error",
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Connect to MongoDB
// 	//client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://localhost:27017"))
// 	if err != nil {
// 		responseBody := ErrorResponse{
// 			Message: "Failed to connect to MongoDB",
// 			Status:  "Error",
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Open GridFS bucket
// 	database := client.Database("logo-bucket")
// 	bucket, err := gridfs.NewBucket(database)
// 	if err != nil {
// 		responseBody := ErrorResponse{
// 			Message: "Failed to open GridFS bucket",
// 			Status:  "Error",
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Upload the resized image to MongoDB GridFS
// 	uploadStream, err := bucket.OpenUploadStream(fileHeader.Filename)
// 	if err != nil {
// 		responseBody := ErrorResponse{
// 			Message: "Failed to open GridFS upload stream",
// 			Status:  "Error",
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}
// 	defer uploadStream.Close()

// 	_, err = io.Copy(uploadStream, strings.NewReader(buf.(*strings.Builder).String()))
// 	if err != nil {
// 		responseBody := ErrorResponse{
// 			Message: "Failed to write resized image to GridFS",
// 			Status:  "Error",
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	fileId, _ := json.Marshal(uploadStream.FileID)
// 	if err != nil {
// 		log.Fatal(err)
// 		responseBody := ErrorResponse{
// 			Message: "Failed to marshal fileID",
// 			Status:  "Error",
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	picID := strings.TrimSpace(string(fileId))
// 	fileIDs := strings.Trim(picID, `"`)

// 	BASE_URL := "https://api.finelooks.co.uk"
// 	imageURL := BASE_URL + "/logo-view/" + fileIDs

// 	responseBody := ImageUploadResponse{
// 		Message:  "Image uploaded successfully",
// 		ImageURL: imageURL,
// 		Status:   "1",
// 	}

// 	// Respond with the image URL
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

// func main() {
// 	http.HandleFunc("/upload-logo", uploadLogoPic)
// 	fmt.Println("Server is listening on port 8080...")
// 	log.Fatal(http.ListenAndServe(":8080", nil))
// }

// func uploadLogoPic(w http.ResponseWriter, r *http.Request) {
// 	// Parse the multipart form data
// 	err := r.ParseMultipartForm(10 << 20) // 10MB max size
// 	if err != nil {
// 		http.Error(w, "Failed to parse multipart form", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve the uploaded file
// 	file, fileHeader, err := r.FormFile("logo")
// 	if err != nil {

// 		//http.Error(w, "Failed to retrieve uploaded file", http.StatusBadRequest)
// 		responseBody := ErrorResponse{
// 			Message: "Failed to retrieve uploaded file",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		//fmt.Println("Error updating record:", updatedDoc.Err())
// 		return
// 	}
// 	defer file.Close()

// 	// Read the contents of the uploaded file
// 	data, err := io.ReadAll(file)
// 	if err != nil {
// 		//http.Error(w, "Failed to read file contents", http.StatusInternalServerError)
// 		responseBody := ErrorResponse{
// 			Message: "Failed to read file contents",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// MongoDB connection URI
// 	//uri := "mongodb://localhost:27017"

// 	// Connect to MongoDB
// 	//client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(uri))

// 	// Open GridFS bucket
// 	database := client.Database("logo-bucket")
// 	bucket, err := gridfs.NewBucket(database)
// 	if err != nil {
// 		//http.Error(w, "Failed to open GridFS bucket", http.StatusInternalServerError)
// 		responseBody := ErrorResponse{
// 			Message: "Failed to open GridFS bucket",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Upload the image to MongoDB GridFS
// 	uploadStream, err := bucket.OpenUploadStream(fileHeader.Filename)
// 	if err != nil {
// 		fmt.Println("ERRO", err)
// 		//http.Error(w, "Failed to open GridFS upload stream", http.StatusInternalServerError)
// 		responseBody := ErrorResponse{
// 			Message: "Failed to open GridFS upload stream",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}
// 	defer uploadStream.Close()

// 	_, err = uploadStream.Write(data)
// 	if err != nil {
// 		//http.Error(w, "Failed to write file to GridFS", http.StatusInternalServerError)
// 		responseBody := ErrorResponse{
// 			Message: "Failed to write file to GridFS",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	fileId, _ := json.Marshal(uploadStream.FileID)
// 	if err != nil {
// 		log.Fatal(err)
// 		//.JSON(http.StatusBadRequest, err.Error())
// 		responseBody := ErrorResponse{
// 			Message: "Failed to marshal fileID",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return

// 	}

// 	//fmt.Println("Retrieved file", fileId)

// 	pic_id := strings.TrimSpace(string(fileId))

// 	fileIds := strings.Trim(pic_id, `"`)

// 	BASE_URL := "https://api.finelooks.co.uk"

// 	imageURL := BASE_URL + "/logo-view/" + fileIds

// 	responseBody := ImageUploadResponse{
// 		Message:  "Image Upload successfully",
// 		ImageURL: imageURL,
// 		Status:   "1",
// 	}

// 	// Respond with the image URL
// 	//w.WriteHeader(http.StatusOK)
// 	//w.Write([]byte(imageURL))
// 	//w.Write([]byte(responseBody))
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)

// }

func handleViewProfilePic(w http.ResponseWriter, r *http.Request) {
	// Extract the file ID from the request or use a default one for testing purposes.
	//id, _ := primitive.ObjectIDFromHex("6613ee0a363a5bbdf7ba60e3")

	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		//http.Error(w, "Invalid User", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Invalid User ID",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Get a handle to the MongoDB database.
	database := client.Database("profile-bucket")

	// Initialize the GridFS bucket.
	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		//http.Error(w, "Failed to open GridFS bucket", http.StatusInternalServerError)

		responseBody := ErrorResponse{
			Message: "Failed to open GridFS bucket",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Open a download stream for the file.
	downloadStream, err := bucket.OpenDownloadStream(objID)
	if err != nil {
		//http.Error(w, "Failed to open download stream", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to open download stream",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	//defer downloadStream.Close()

	// Read the file data.
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, downloadStream); err != nil {
		//http.Error(w, "Failed to read file", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to read file",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Set the appropriate content type based on the file type.
	contentType := http.DetectContentType(buf.Bytes())
	w.Header().Set("Content-Type", contentType)

	// Write the file data to the response writer.
	if _, err := io.Copy(w, &buf); err != nil {
		//http.Error(w, "Failed to write file to response", http.StatusInternalServerError)

		responseBody := ErrorResponse{
			Message: "Failed to write file to response",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
}

func handleViewLogo(w http.ResponseWriter, r *http.Request) {
	// Extract the file ID from the request or use a default one for testing purposes.
	//id, _ := primitive.ObjectIDFromHex("6613ee0a363a5bbdf7ba60e3")

	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		//http.Error(w, "Invalid User", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "invalid user id",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Get a handle to the MongoDB database.
	database := client.Database("logo-bucket")

	// Initialize the GridFS bucket.
	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		//http.Error(w, "Failed to open GridFS bucket", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to open GridFS bucket",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Open a download stream for the file.
	downloadStream, err := bucket.OpenDownloadStream(objID)

	if err != nil {
		fmt.Println("ERROR", err)
		//http.Error(w, "Failed to open download stream", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to open download stream",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	//	defer downloadStream.Close()

	// Read the file data.
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, downloadStream); err != nil {
		//http.Error(w, "Failed to read file", http.StatusInternalServerError)

		responseBody := ErrorResponse{
			Message: "Failed to read file",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Set the appropriate content type based on the file type.
	contentType := http.DetectContentType(buf.Bytes())
	w.Header().Set("Content-Type", contentType)

	// Write the file data to the response writer.
	if _, err := io.Copy(w, &buf); err != nil {
		//http.Error(w, "Failed to write file to response", http.StatusInternalServerError)

		responseBody := ErrorResponse{
			Message: "Failed to write file to response",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
}

func uploadPicCompress(w http.ResponseWriter, r *http.Request) {
	// Parse the multipart form data
	err := r.ParseMultipartForm(10 << 20) // 10MB max size
	if err != nil {
		http.Error(w, "Failed to parse multipart form", http.StatusBadRequest)
		return
	}

	// Retrieve the uploaded file
	file, fileHeader, err := r.FormFile("test-logo")
	if err != nil {

		//http.Error(w, "Failed to retrieve uploaded file", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "Failed to retrieve uploaded file",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		//fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}
	defer file.Close()

	//buffer := []byte("Your image data as a byte slice")
	// quality := 90       // Example quality value
	// dirname := "output" // Example output directory name

	// buffer1, _ := io.ReadAll(file)

	// //filename, err := imageProcessing(buffer1, quality, dirname)

	// if err != nil {
	// 	fmt.Println("FAILED TO COMPRESS", err)
	// 	responseBody := ErrorResponse{
	// 		Message: "FAILED TO COMPRESSs",
	// 		Status:  "Error",
	// 	}

	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)
	// 	return

	// }

	// fmt.Println("GENERATED FILE NAME", filename)

	// Read the contents of the uploaded file
	data, err := io.ReadAll(file)
	if err != nil {
		//http.Error(w, "Failed to read file contents", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to read file contents",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// MongoDB connection URI
	//uri := "mongodb://localhost:27017"

	// Connect to MongoDB
	//client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(uri))

	// Open GridFS bucket
	database := client.Database("test-bucket")
	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		//http.Error(w, "Failed to open GridFS bucket", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to open GridFS bucket",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Upload the image to MongoDB GridFS
	uploadStream, err := bucket.OpenUploadStream(fileHeader.Filename)
	if err != nil {
		fmt.Println("ERRO", err)
		//http.Error(w, "Failed to open GridFS upload stream", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to open GridFS upload stream",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
	defer uploadStream.Close()

	_, err = uploadStream.Write(data)
	if err != nil {
		//http.Error(w, "Failed to write file to GridFS", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to write file to GridFS",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	fileId, _ := json.Marshal(uploadStream.FileID)
	if err != nil {
		log.Fatal(err)
		//.JSON(http.StatusBadRequest, err.Error())
		responseBody := ErrorResponse{
			Message: "Failed to marshal fileID",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	//fmt.Println("Retrieved file", fileId)

	initializePartnersStatus()
	initializeCustomerStatus()

	pic_id := strings.TrimSpace(string(fileId))

	fileIds := strings.Trim(pic_id, `"`)

	BASE_URL := "https://finelooks-backend-service.onrender.com"

	imageURL := BASE_URL + "/test-bucket/" + fileIds

	responseBody := ImageUploadResponse{
		Message:  "Image Upload successfully",
		ImageURL: imageURL,
		Status:   "1",
	}

	// Respond with the image URL
	//w.WriteHeader(http.StatusOK)
	//w.Write([]byte(imageURL))
	//w.Write([]byte(responseBody))
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

}

func handleViewTestImage(w http.ResponseWriter, r *http.Request) {
	// Extract the file ID from the request or use a default one for testing purposes.
	//id, _ := primitive.ObjectIDFromHex("6613ee0a363a5bbdf7ba60e3")

	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		//http.Error(w, "Invalid User", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "invalid user id",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Get a handle to the MongoDB database.
	database := client.Database("test-bucket")

	// Initialize the GridFS bucket.
	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		//http.Error(w, "Failed to open GridFS bucket", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to open GridFS bucket",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Open a download stream for the file.
	downloadStream, err := bucket.OpenDownloadStream(objID)

	if err != nil {
		fmt.Println("ERROR", err)
		//http.Error(w, "Failed to open download stream", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to open download stream",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	//	defer downloadStream.Close()

	// Read the file data.
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, downloadStream); err != nil {
		//http.Error(w, "Failed to read file", http.StatusInternalServerError)

		responseBody := ErrorResponse{
			Message: "Failed to read file",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Set the appropriate content type based on the file type.
	contentType := http.DetectContentType(buf.Bytes())
	w.Header().Set("Content-Type", contentType)

	// Write the file data to the response writer.
	if _, err := io.Copy(w, &buf); err != nil {
		//http.Error(w, "Failed to write file to response", http.StatusInternalServerError)

		responseBody := ErrorResponse{
			Message: "Failed to write file to response",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
}
