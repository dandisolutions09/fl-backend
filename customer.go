package main

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Customer struct {
	ID              primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	UserID          string             `json:"user_id" bson:"user_id"`
	First_name      string             `json:"first_name" bson:"first_name"`
	Last_name       string             `json:"last_name" bson:"last_name"`
	Country         string             `json:"country" bson:"country"`
	Timezone        string             `json:"timezone" bson:"timezone"`
	PhoneNumber     string             `json:"phone_number" bson:"phone_number"`
	Email           string             `json:"email" bson:"email"`
	SocialMedia     string             `json:"social_media" bson:"social_media"`
	ProfilePic      string             `json:"profile_pic" bson:"profile_pic"`
	Password        string             `json:"password" bson:"password"`
	Salt            []byte             `json:"salt" bson:"salt"`
	Role            string             `json:"role" bson:"role"`
	Location        Coordinates        `json:"location" bson:"location"`
	Status          string             `json:"status" bson:"status"`
	Cart            []CartObject       `json:"cart" bson:"cart"`
	PendingBooking  BookingObject      `json:"pending_booking" bson:"pending_booking"`
	CreatedAt       string             `json:"created_at" bson:"created_at"`
	IsPasswordReset bool               `json:"isPasswordReset" bson:"isPasswordReset"`
}

type Customer2 struct {
	ID             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	UserID         string             `json:"user_id" bson:"user_id"`
	PrevID         string             `json:"prev_id" bson:"prev_id"`
	First_name     string             `json:"first_name" bson:"first_name"`
	Last_name      string             `json:"last_name" bson:"last_name"`
	Country        string             `json:"country" bson:"country"`
	Timezone       string             `json:"timezone" bson:"timezone"`
	PhoneNumber    string             `json:"phone_number" bson:"phone_number"`
	Email          string             `json:"email" bson:"email"`
	SocialMedia    string             `json:"social_media" bson:"social_media"`
	ProfilePic     string             `json:"profile_pic" bson:"profile_pic"`
	Password       string             `json:"password" bson:"password"`
	Salt           []byte             `json:"salt" bson:"salt"`
	Role           string             `json:"role" bson:"role"`
	Location       Coordinates        `json:"location" bson:"location"`
	Status         string             `json:"status" bson:"status"`
	Cart           []CartObject       `json:"cart" bson:"cart"`
	PendingBooking BookingObject      `json:"pending_booking" bson:"pending_booking"`
	CreatedAt      string             `json:"created_at" bson:"created_at"`
}

type CustomerRegistrationResponse struct {
	Message  string   `json:"message"`
	Token    string   `json:"token,omitempty"`
	Customer Customer `json:"customer,omitempty"`
	Status   string   `json:"status,omitempty"`
}

type CustomerRegistrationResponse2 struct {
	Message  string    `json:"message"`
	Token    string    `json:"token,omitempty"`
	Customer Customer2 `json:"customer,omitempty"`
	Status   string    `json:"status,omitempty"`
}

// type CustomerLoginResponse struct {
// 	Message    string `json:"message"`
// 	Token      string `json:"token,omitempty"`
// 	CustomerID string `json:"admin_id,omitempty"`
// 	Status     string `json:"status,omitempty"`
// 	Type       string `json:"type,omitempty"`
// 	Hash       string `json:"hash,omitempty"`
// }

type CustomerLoginResponse struct {
	Message  string   `json:"message"`
	Token    string   `json:"token,omitempty"`
	Customer Customer `json:"customer,omitempty"`

	Status string `json:"status,omitempty"`
}

func handleCustomerLogin(w http.ResponseWriter, r *http.Request) {

	type DataCustomerNeedsPasswordReset struct {
		Customer Customer `json:"partner"`
	}

	type CustomerNeedsPasswordResetResponse struct {
		Message string `json:"message"`

		Data DataCustomerNeedsPasswordReset `json:"data"`

		Status string `json:"status"`
	}

	type DataCustomerLogin struct {
		// Message    string  `json:"message"`
		Token       string   `json:"token"`
		Customer    Customer `json:"customer"`
		Customer_ID string   `json:"customer_id"`
		//PartnerID string  `json:"token,omitempty"`
		// Status string `json:"status,omitempty"`
	}

	type CustomerLoginResponse struct {
		Message string `json:"message"`
		Token   string `json:"token"`
		//Partner    Partner `json:"data,omitempty"
		Data DataCustomerLogin `json:"data"`
		//Customer_ID string            `json:"customer_id"`
		//PartnerID string  `json:"token,omitempty"`
		Status string `json:"status"`
	}

	// response := CustomerLoginResponse{
	// 	Message: "Authentication successful",
	// 	Status:  "Success",
	// 	Data:    data,

	// 	//Status:  storedUser.Status,
	// }

	w.Header().Set("Content-Type", "application/json")

	var u Customer
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		//http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "Failed to decode request JSON",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	fmt.Println("customer data provider", u)

	if u.Email == "" || u.Password == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: email, password",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// Make a GET request to retrieve the stored hash from another endpoint
	//url := "http://localhost:8080/get-username/" + u.Username
	//url := "http://localhost:8080/get-userid/" + u.ID.Hex()
	url := "http://localhost:8080/get-customer-by-email/" + u.Email

	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Error making GET request", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	storedHash, err := io.ReadAll(response.Body)
	if err != nil {
		http.Error(w, "Error reading response body", http.StatusInternalServerError)
		return
	}

	var storedUser Customer
	if err := json.Unmarshal(storedHash, &storedUser); err != nil {
		//http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Error parsing JSON response",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	//*************GET STORED USER***************
	customerID := storedUser.ID.Hex()
	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(customerID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}
	// Retrieve the specific admin from MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	var retrievedCustomer Customer
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&retrievedCustomer)
	if err != nil {
		fmt.Println("Customers", err)
		response := map[string]string{"message": "Customer not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}
	fmt.Println("Retrived customer on login", retrievedCustomer)
	//****************************************************

	if !retrievedCustomer.IsPasswordReset {

		data := DataCustomerNeedsPasswordReset{
			Customer: retrievedCustomer,
		}

		response := CustomerNeedsPasswordResetResponse{
			Message: "Customer Needs to Reset Password",
			Data:    data,
			Status:  "Error",
		}

		// response := map[string]string{"message": "Partner Needs to Reset Password", "status": "Error"}
		// w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return

	}

	passwordsMatch := comparePasswords(u.Password, storedUser.Password, storedUser.Salt)
	if passwordsMatch {
		token, _ := createToken(u.First_name + u.Last_name)

		// type DataCustomerLogin struct {
		// 	// Message    string  `json:"message"`
		// 	Token       string   `json:"token"`
		// 	Customer    Customer `json:"customer"`
		// 	Customer_ID string   `json:"customer_id"`
		// 	//PartnerID string  `json:"token,omitempty"`
		// 	// Status string `json:"status,omitempty"`
		// }

		data := DataCustomerLogin{

			Customer_ID: storedUser.ID.Hex(),
			Customer:    retrievedCustomer,
			Token:       token,
		}

		response := CustomerLoginResponse{
			Message: "Authentication successful",
			Token:   token,
			Data:    data,
			Status:  "Success",

			//Status:  storedUser.Status,
		}
		fmt.Println("first name:->", storedUser)

		logData := Logs_Struct{
			FirstName: storedUser.First_name + "  " + storedUser.Last_name,
			//LastName:  storedUser.Last_name,
			UserRole:  "customer",
			Email:     u.Email,
			Activity:  "Logged in Successfully",
			CreatedAt: ConvertToNairobiTime(),
		}

		insertLogs(logData)
		json.NewEncoder(w).Encode(response)
	} else {

		responseBody := ErrorResponse{
			Message: "Wrong Login Information",
			Status:  "Error",
		}

		logData := Logs_Struct{
			FirstName: storedUser.First_name + "  " + storedUser.Last_name,
			//LastName:  storedUser.Last_name,
			UserRole:  "customer",
			Email:     u.Email,
			Activity:  "Logged Failed",
			CreatedAt: ConvertToNairobiTime(),
		}

		insertLogs(logData)
		json.NewEncoder(w).Encode(responseBody)
	}
}

func RegisterCustomer(w http.ResponseWriter, r *http.Request) {

	//fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	var inc_data Customer
	err := decoder.Decode(&inc_data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("incoming data", inc_data.PhoneNumber)

	if inc_data.Email == "" || inc_data.Password == "" || inc_data.First_name == "" || inc_data.Last_name == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: email, password, first_name, or last_name",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	//inc_data.PendingBooking.CustomerLocation.Latitude = 0
	//inc_data.PendingBooking.CustomerLocation.Longitude = 0

	if isCustomerEmailTaken(inc_data.Email) {
		//http.Error(w, "Email-already-taken", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Email-already-taken",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		//fmt.Println("Error updating record:", updatedDoc.Err())

		return
	}

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(inc_data.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	inc_data.Password = hashedPassword
	inc_data.Salt = salt
	inc_data.Status = "INACTIVE"

	// fmt.Println("to be saved in db:->", inc_data)
	// fmt.Println("FULLNAME", inc_data.First_name+" "+inc_data.Last_name)
	fmt.Println("FULLNAME:", inc_data.First_name+" "+inc_data.Last_name)

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)
	inc_data.CreatedAt = nairobiTime
	inc_data.PendingBooking.Cart = []PartnerServiceObject{}

	if (inc_data.PendingBooking.Cart) == nil {
		fmt.Println(inc_data.PendingBooking.Cart)
	}

	// inc_data.Location.Latitude = 0
	// inc_data.Location.Longitude = 0

	collection := client.Database(dbName).Collection(customer_collection)
	_, err = collection.InsertOne(context.Background(), inc_data)
	fmt.Print(inc_data)
	if err != nil {
		http.Error(w, "Error inserting Customer", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	// fmt.Fprintf(w, "Nurse inserted Successfully")

	//response := map[string]string{"message": "User inserted successfully"}
	fmt.Println("User inserted successfully", inc_data.Email)

	url := "http://localhost:8080/get-customer-by-email/" + inc_data.Email

	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Error making GET request", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	storedUserData, err := io.ReadAll(response.Body)
	if err != nil {
		http.Error(w, "Error reading response body", http.StatusInternalServerError)
		return
	}

	var storedUser Customer
	if err := json.Unmarshal(storedUserData, &storedUser); err != nil {
		http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)
		return
	}

	fmt.Println("Passed user ID:", storedUser.ID)

	customerID := storedUser.ID.Hex()

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(customerID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific admin from MongoDB
	collection = client.Database(dbName).Collection(customer_collection)
	var retrievedCustomer Customer
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&retrievedCustomer)
	if err != nil {
		response := map[string]string{"message": "Customer not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	fmt.Println("Retrived customer", retrievedCustomer)

	token, _ := createToken(storedUser.First_name + storedUser.Last_name)

	data := DataCustomer{

		Customer_ID: storedUser.ID.Hex(),
		Customer:    retrievedCustomer,
		Token:       token,
	}

	responseBody := RegistrationResponseCustomer{
		Message: "Customer Registration Successful",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	// responseBody := CustomerRegistrationResponse{
	// 	Message:  "Customer Registration Successful",
	// 	Token:    token,
	// 	Customer: retrievedCustomer,
	// 	Status:   "1",
	// }

	//json.NewEncoder(w).Encode(response)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

}

func RegisterCustomer2(w http.ResponseWriter, r *http.Request) {

	//fmt.Println("incoming data:", r.Body)
	fmt.Println("Mock add customer endpoint")

	decoder := json.NewDecoder(r.Body)
	var inc_data Customer2
	err := decoder.Decode(&inc_data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("incoming data", inc_data.PhoneNumber)

	if inc_data.Email == "" || inc_data.Password == "" || inc_data.First_name == "" || inc_data.Last_name == "" || inc_data.PhoneNumber == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: email, password, first_name, or last_name",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	//inc_data.PendingBooking.CustomerLocation.Latitude = 0
	//inc_data.PendingBooking.CustomerLocation.Longitude = 0

	if isCustomerEmailTaken(inc_data.Email) {
		//http.Error(w, "Email-already-taken", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Email-already-taken",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		//fmt.Println("Error updating record:", updatedDoc.Err())

		return
	}

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(inc_data.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	inc_data.Password = hashedPassword
	inc_data.Salt = salt
	inc_data.Status = "INACTIVE"

	// fmt.Println("to be saved in db:->", inc_data)
	// fmt.Println("FULLNAME", inc_data.First_name+" "+inc_data.Last_name)
	fmt.Println("FULLNAME:", inc_data.First_name+" "+inc_data.Last_name)

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)
	inc_data.CreatedAt = nairobiTime
	inc_data.PendingBooking.Cart = []PartnerServiceObject{}

	if (inc_data.PendingBooking.Cart) == nil {
		fmt.Println(inc_data.PendingBooking.Cart)
	}

	// inc_data.Location.Latitude = 0
	// inc_data.Location.Longitude = 0

	collection := client.Database(dbName).Collection(customer_collection_mock)
	_, err = collection.InsertOne(context.Background(), inc_data)
	fmt.Print(inc_data)
	if err != nil {
		http.Error(w, "Error inserting Customer", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	// fmt.Fprintf(w, "Nurse inserted Successfully")

	//response := map[string]string{"message": "User inserted successfully"}
	fmt.Println("User inserted successfully", inc_data.Email)

	url := "http://localhost:8080/get-customer-by-email/" + inc_data.Email

	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Error making GET request", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	storedUserData, err := io.ReadAll(response.Body)
	if err != nil {
		http.Error(w, "Error reading response body", http.StatusInternalServerError)
		return
	}

	var storedUser Customer2
	if err := json.Unmarshal(storedUserData, &storedUser); err != nil {
		http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)
		return
	}

	fmt.Println("Passed user ID:", storedUser.ID)

	customerID := storedUser.ID.Hex()

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(customerID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific admin from MongoDB
	collection = client.Database(dbName).Collection(customer_collection_mock)
	var retrievedCustomer Customer2
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&retrievedCustomer)
	if err != nil {
		response := map[string]string{"message": "Customer not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	fmt.Println("Retrived customer", retrievedCustomer)

	token, _ := createToken(storedUser.First_name + storedUser.Last_name)

	responseBody := CustomerRegistrationResponse2{
		Message:  "Customers Registration Successful",
		Token:    token,
		Customer: retrievedCustomer,
		Status:   "1",
	}

	//json.NewEncoder(w).Encode(response)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

}

func handleGetCustomerByEmail(w http.ResponseWriter, r *http.Request) {

	fmt.Println("get driver by ID has been called....")
	// Get the driver ID from the request parameters
	params := mux.Vars(r)
	email := params["email"]

	// Convert the Driver ID to a MongoDB ObjectID

	// Retrieve the specific driver from MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	var usr User
	err := collection.FindOne(context.Background(), bson.M{"email": email}).Decode(&usr)
	if err != nil {
		//http.Error(w, "Driver not found", http.StatusNotFound)
		response := map[string]string{"message": "User not found", "status": "Error"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved driver in JSON format
	w.Header().Set("Content-Type", "application/json")

	//passwordVerification()

	json.NewEncoder(w).Encode(usr)
}

func handleGetCustomers(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(customer_collection)

	fmt.Println("Get all customers called")

	// Define a slice to store retrieved records
	var customers []Customer

	// Define a filter to retrieve only active records
	//filter := bson.M{"status": "active"}
	filter := bson.M{}

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the records slice
	for cursor.Next(context.Background()) {
		var record Customer
		if err := cursor.Decode(&record); err != nil {
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}

		if record.PendingBooking.Cart == nil {
			fmt.Println("record", record.PendingBooking.Cart)
			record.PendingBooking.Cart = []PartnerServiceObject{}
			//updatedCart = []PartnerServiceObject{}

		}
		customers = append(customers, record)
	}

	err_ := initializeCustomerStatus()
	if err_ != nil {
		fmt.Println("Error Initializing partner status:-", err_)
	}

	// Respond with the retrieved records in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(customers)
}

func uploadCustomerProfilePic(w http.ResponseWriter, r *http.Request) {
	// Parse the multipart form data
	err := r.ParseMultipartForm(10 << 20) // 10MB max size
	if err != nil {
		http.Error(w, "Failed to parse multipart form", http.StatusBadRequest)
		return
	}

	// Retrieve the uploaded file
	file, fileHeader, err := r.FormFile("profile")
	if err != nil {
		http.Error(w, "Failed to retrieve uploaded file", http.StatusBadRequest)
		return
	}
	defer file.Close()

	// Read the contents of the uploaded file
	data, err := io.ReadAll(file)
	if err != nil {
		http.Error(w, "Failed to read file contents", http.StatusInternalServerError)
		return
	}

	// MongoDB connection URI
	//uri := "mongodb://localhost:27017"

	// Connect to MongoDB
	//client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(uri))

	// Open GridFS bucket
	database := client.Database("profile-bucket")
	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		http.Error(w, "Failed to open GridFS bucket", http.StatusInternalServerError)
		return
	}

	// Upload the image to MongoDB GridFS
	uploadStream, err := bucket.OpenUploadStream(fileHeader.Filename)

	if err != nil {
		fmt.Println("ERRO", err)
		http.Error(w, "Failed to open GridFS upload stream", http.StatusInternalServerError)
		return
	}
	defer uploadStream.Close()

	_, err = uploadStream.Write(data)
	if err != nil {
		http.Error(w, "Failed to write file to GridFS", http.StatusInternalServerError)
		return
	}

	fileId, _ := json.Marshal(uploadStream.FileID)
	if err != nil {
		log.Fatal(err)
		//.JSON(http.StatusBadRequest, err.Error())
		return

	}

	//fmt.Println("Retrieved file", fileId)

	pic_id := strings.TrimSpace(string(fileId))

	fileIds := strings.Trim(pic_id, `"`)

	BASE_URL := "https://api.finelooks.co.uk"

	imageURL := BASE_URL + "/profile-view/" + fileIds

	// Respond with the image URL
	//w.WriteHeader(http.StatusOK)
	//w.Write([]byte(imageURL))

	responseBody := ImageUploadResponse{
		Message:  "Image Upload successfully",
		ImageURL: imageURL,
		Status:   "1",
	}

	// Respond with the image URL
	//w.WriteHeader(http.StatusOK)
	//w.Write([]byte(imageURL))
	//w.Write([]byte(responseBody))
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

func handleAddCustomerProfilePicURL(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	partnerID := params["id"]
	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", partnerID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Decode the JSON request body into a Person struct
	var updateData Customer
	//if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
	//http.Error(w, "Failed to decode request body", http.StatusBadRequest)

	if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
		//http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "Failed to decode request body",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	if updateData.ProfilePic == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: Profile Pic",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}
	//update := bson.M{"$set": bson.M{"profile_pic": updateData.ProfilePic}}
	update := bson.M{
		"$set": bson.M{
			"profile_pic":  updateData.ProfilePic,
			"phone_number": updateData.PhoneNumber,
		},
	}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		// http.Error(w, "Error updating record", http.StatusInternalServerError)
		// fmt.Println("errr", err)

		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("errr", err)
		return
	}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_customer Customer

	err = updatedDoc.Decode(&updated_customer)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	responseBody := CustomerRegistrationResponse{
		Message:  "Customer Profile Picture URL updated successfully",
		Customer: updated_customer,
		Status:   "1",
	}

	// Respond with a success message
	//response := map[string]string{"message": "Partner Profile Picture updated successfully", "status:": "1"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// UPDATE CUSTOMER INFO
func handleUpdateCustomer(w http.ResponseWriter, r *http.Request) {
	// Get the user ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Customer
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Exclude password and salt from the update
	updateData := bson.M{}
	var updateResult *mongo.UpdateResult

	if updatedData.Email != "" {
		updateData["email"] = updatedData.Email
	}
	if updatedData.First_name != "" {
		updateData["first_name"] = updatedData.First_name
	}
	if updatedData.Last_name != "" {
		updateData["last_name"] = updatedData.Last_name
	}

	if updatedData.PhoneNumber != "" {
		updateData["phone_number"] = updatedData.PhoneNumber
	}

	if updatedData.ProfilePic != "" {
		updateData["profile_pic"] = updatedData.ProfilePic
	}

	// Add other fields that you want to update...

	// Update the specific user in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updateData}

	updateResult, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating user", http.StatusInternalServerError)
		return
	}

	// Check if any document was modified
	if updateResult.ModifiedCount == 0 {
		// No document was modified
		responseBody := ErrorResponse{
			Message: "Nothing to be updated",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	logData := Logs_Struct{
		FirstName: updatedData.First_name + "  " + updatedData.Last_name,
		//LastName:  storedUser.Last_name,
		UserRole:  "partner",
		Email:     updatedData.Email,
		Activity:  "Updated User Registration Data",
		CreatedAt: ConvertToNairobiTime(),
	}

	insertLogs(logData)

	// Respond with a success message
	response := map[string]string{"message": "User updated successfully", "status": "Success"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleGetCustomerId(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get admin by ID has been called....")

	// Get the admin ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	fmt.Println("Passed user ID:", userID)

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific admin from MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	var admin Customer
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&admin)
	if err != nil {
		fmt.Println("ERR", err)
		response := map[string]string{"message": "Customer not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved admin in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(admin)
}

func handleGetActiveCustomers(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Welcome to protected area")

	collection := client.Database(dbName).Collection(customer_collection)

	fmt.Println("Get all partners called")

	// Define a slice to store retrieved records
	var partners []Customer

	// Define a filter to retrieve only active records
	filter := bson.M{}

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the records slice
	for cursor.Next(context.Background()) {
		var record Customer
		if err := cursor.Decode(&record); err != nil {
			fmt.Print("ERROR", err, record.PendingBooking)
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}
		// Check if either mainBusinessName or logo is empty
		if record.Location.Latitude == 0 ||
			record.Location.Longitude == 0 {
			record.Status = "INACTIVE"
		} else {
			record.Status = "ACTIVE"

			if record.PendingBooking.Cart == nil {
				fmt.Println("record", record.PendingBooking.Cart)
				record.PendingBooking.Cart = []PartnerServiceObject{}
				//updatedCart = []PartnerServiceObject{}

			}

			partners = append(partners, record)

		}
	}

	// Respond with the retrieved records in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(partners)
}

// Define a struct to hold only the Cart property
type CartResponse struct {
	Cart []PartnerServiceObject `json:"cart" bson:"cart"`
}

func handleGetCustomerCart(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get admin by ID has been called....")

	userID := r.URL.Query().Get("id")

	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	collection := client.Database(dbName).Collection(customer_collection)
	var admin Customer
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&admin)
	if err != nil {
		fmt.Println("ERR", err)
		response := map[string]string{"message": "Customer not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Create a response with only the Cart property
	cartResponse := CartResponse{Cart: admin.PendingBooking.Cart}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(cartResponse)
}

// //RESET CUSTOMER PASSWORD
func handleUpdateCustomerPassword(w http.ResponseWriter, r *http.Request) {

	// type DataCustomerPasswordReset struct {
	// 	Customer Customer `json:"customer"`
	// }

	// type CustomerPasswordResetResponse struct {
	// 	Message string `json:"message"`
	// 	Data DataCustomerPasswordReset `json:"data"`

	// 	Status string `json:"status"`
	// }
	// Get the user ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var incomingPassword Customer
	err = decoder.Decode(&incomingPassword)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("INCOMING DATA:->", incomingPassword, objID)

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// // Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(incomingPassword.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	fmt.Println("GENERATED HASH", hashedPassword)
	fmt.Println("GENERATED SALT", salt)

	updateData := bson.M{}
	var updateResult *mongo.UpdateResult

	if incomingPassword.Password != "" {
		updateData["password"] = hashedPassword
		fmt.Println("NEW HASH", updateData["password"])
		updateData["salt"] = salt
		fmt.Println("NEW SALT", updateData["salt"])
		updateData["isPasswordReset"] = true // Add this line to update isPasswordReset

		//Update the specific user in MongoDB
		collection := client.Database(dbName).Collection(customer_collection)
		filter := bson.M{"_id": objID}
		update := bson.M{"$set": updateData}

		updateResult, err = collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, "Error updating user", http.StatusInternalServerError)
			return
		}

		// Check if any document was modified
		if updateResult.ModifiedCount == 0 {

			http.Error(w, "Nothing to be updated", http.StatusBadRequest)
			return

		}

		// Respond with a success message
		//response := map[string]string{"message": "User updated successfully"}
		responseBody := ErrorResponse{
			Message: "Customer Password Reset successfully",
			Status:  "Success",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	} else {
		fmt.Println("password not provided")
		responseBody := ErrorResponse{
			Message: "Password not provided",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

}

// filter customers by status
func filterCustomersByStatus(w http.ResponseWriter, r *http.Request) {
	status := r.URL.Query().Get("status")

	fmt.Println("Status", status)

	// Check if subcategory is "all"
	if status == "ALL" {
		// Fetch all services from MongoDB
		var customers []Customer
		collection := client.Database(dbName).Collection(customer_collection)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var customer Customer
			err := cursor.Decode(&customer)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			customers = append(customers, customer)
		}

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(customers)
		return
	}

	// Fetch partners from MongoDB based on subcategory
	var customers []Customer
	collection := client.Database(dbName).Collection(customer_collection)
	cursor, err := collection.Find(context.Background(), bson.M{"status": status})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var customer Customer
		err := cursor.Decode(&customer)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		customers = append(customers, customer)
	}

	// Respond with the retrieved services
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(customers)
}

// Deactivate customer
func DeactivateCustomer(w http.ResponseWriter, r *http.Request) {
	// Get ID from URL params
	//vars := mux.Vars(r)
	partnerID := r.URL.Query().Get("id")

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Define filter to find document by ID

	// Define update to set status to INACTIVE
	update := bson.M{"$set": bson.M{"status": "INACTIVE"}}

	//fmt.Println("update Data:-->", updateData)

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_customer Customer

	err = updatedDoc.Decode(&updated_customer)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	responseBody := CustomerRegistrationResponse{
		Message:  "Customer Deactivated successfully",
		Customer: updated_customer,
		Status:   "1",
	}
	invalidatePartnerCache()

	// Encode the updated document as JSON and send in response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// Activate customer
func ActivateCustomer(w http.ResponseWriter, r *http.Request) {
	// Get ID from URL params
	//vars := mux.Vars(r)
	partnerID := r.URL.Query().Get("id")

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Define filter to find document by ID

	// Define update to set status to INACTIVE
	update := bson.M{"$set": bson.M{"status": "ACTIVE"}}

	//fmt.Println("update Data:-->", updateData)

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_customer Customer

	err = updatedDoc.Decode(&updated_customer)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	responseBody := CustomerRegistrationResponse{
		Message:  "Customer Activate successfully",
		Customer: updated_customer,
		Status:   "1",
	}
	invalidatePartnerCache()

	// Encode the updated document as JSON and send in response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// customer social login
func handleCustomerSocialLogin(w http.ResponseWriter, r *http.Request) {

	type CustomerData struct {
		// Message    string  `json:"message"`
		Token       string   `json:"token"`
		Customer    Customer `json:"customer"`
		Customer_ID string   `json:"customer_id"`
		//PartnerID string  `json:"token,omitempty"`
		// Status string `json:"status,omitempty"`
	}

	type CustomerRegistrationResponse struct {
		Message string       `json:"message"`
		Data    CustomerData `json:"data"`
		Status  string       `json:"status"`
	}

	// Decode the incoming partner data
	var customerSocialObject Customer
	err := json.NewDecoder(r.Body).Decode(&customerSocialObject)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Check if the partner already exists based on email
	existingPartner, err := getCustomerByEmail(customerSocialObject.Email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// If partner exists, return an appropriate message
	if existingPartner != nil {
		responseBody := CustomerRegistrationResponse{
			Message: "Customer already exists",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// If partner doesn't exist, proceed with storing
	// partnerSocialObject.Status = "INACTIVE"

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)
	customerSocialObject.CreatedAt = nairobiTime
	//partnerSocialObject.PartnerServices = []PartnerServiceObject{}

	customerSocialObject.Status = "INACTIVE"

	// fmt.Println("to be saved in db:->", inc_data)
	// fmt.Println("FULLNAME", inc_data.First_name+" "+inc_data.Last_name)
	fmt.Println("FULLNAME:", customerSocialObject.First_name+" "+customerSocialObject.Last_name)

	customerSocialObject.PendingBooking.Cart = []PartnerServiceObject{}

	// point := LocationPoint{
	// 	Type:        "Point",
	// 	Coordinates: []float64{0, 0},
	// }

	///customerSocialObject.Location = point

	collection := client.Database(dbName).Collection(customer_collection)
	customerSocialObject.CreatedAt = ConvertToNairobiTime()
	result, err := collection.InsertOne(context.Background(), customerSocialObject)

	if err != nil {
		fmt.Println("err", err)
		responseBody := RegistrationResponse{
			Message: "Error inserting Customer",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Retrieve the inserted partner record
	var insertedCustomer Customer
	filter := bson.M{"_id": result.InsertedID}
	err = collection.FindOne(context.Background(), filter).Decode(&insertedCustomer)
	if err != nil {
		fmt.Println("err", err)
		responseBody := RegistrationResponse{
			Message: "Error retrieving inserted partner",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Prepare response
	token, _ := createToken(insertedCustomer.First_name + insertedCustomer.Last_name)
	data := CustomerData{
		Customer:    insertedCustomer,
		Token:       token,
		Customer_ID: insertedCustomer.ID.Hex(),
	}

	responseBody := CustomerRegistrationResponse{
		Message: "Social Customer Added Successfully",
		Data:    data,
		Status:  "Success",
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// Function to get partner by email
func getCustomerByEmail(email string) (*Customer, error) {
	fmt.Println("check customer email....")
	var customer Customer
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"email": email}
	err := collection.FindOne(context.Background(), filter).Decode(&customer)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			// Partner doesn't exist
			return nil, nil
		}
		// Other error occurred
		return nil, err
	}
	// Partner exists
	return &customer, nil
}

func handleAddCustomerPhoneNumber(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters

	type DataCustomerPhoneNumberUpdate struct {
		Customer Customer `json:"customer"`
	}

	type CustomerPhoneNumberUpdateResponse struct {
		Message string `json:"message"`

		Data DataCustomerPhoneNumberUpdate `json:"data"`

		Status string `json:"status"`
	}

	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	customerID := params["id"]
	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", customerID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(customerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Decode the JSON request body into a Person struct
	var updateData Customer
	//if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
	//http.Error(w, "Failed to decode request body", http.StatusBadRequest)

	if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
		//http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "Failed to decode request body",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	if updateData.PhoneNumber == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: Phone Number",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": bson.M{"phone_number": updateData.PhoneNumber}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		// http.Error(w, "Error updating record", http.StatusInternalServerError)
		// fmt.Println("errr", err)

		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("errr", err)
		return
	}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_customer Customer

	err = updatedDoc.Decode(&updated_customer)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	// responseBody := RegistrationResponse{
	// 	Message: "Partner Profile Picture URL updated successfully",
	// 	Partner: updated_partner,
	// 	Status:  "Success",
	// }

	data := DataCustomerPhoneNumberUpdate{

		// Partner_ID: storedUser.ID.Hex(),
		Customer: updated_customer,
		// Token:      token,
	}

	responseBody := CustomerPhoneNumberUpdateResponse{
		Message: "Customer Phone Number updated successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	invalidatePartnerCache()

	// Respond with a success message
	//response := map[string]string{"message": "Partner Profile Picture updated successfully", "status:": "1"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}
