package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"sort"
	"strings"

	//"fmt"

	//"io"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
	//"github.com/gorilla/mux"
	// "go.mongodb.org/mongo-driver/bson"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/mongo/options"
)

type ServiceObject struct {
	ServiceID    string `json:"service_id" bson:"service_id"`
	Type         string `json:"type" bson:"type"`
	Status       string `json:"status" bson:"status"`
	Description  string `json:"description" bson:"description"`
	Localization string `json:"localization" bson:"localization"`
	Subcategory  string `json:"subcategory" bson:"subcategory"`
	CreatedAt    string `json:"created_at" bson:"created_at"`
}

type ServiceCategory struct {
	ID            string `json:"_id,omitempty" bson:"_id,omitempty"`
	Category_name string `json:"category_name" bson:"category_name"`
	Category_pic  string `json:"category_pic" bson:"category_pic"`
	CreatedAt     string `json:"created_at" bson:"created_at"`
}

func storeService(w http.ResponseWriter, r *http.Request) {
	var newService ServiceObject
	err := json.NewDecoder(r.Body).Decode(&newService)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)
	newService.CreatedAt = nairobiTime

	// Insert the new service into MongoDB
	collection := client.Database(dbName).Collection(services_collection)
	_, err = collection.InsertOne(context.Background(), newService)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with success message
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newService)
}

func storeService2(w http.ResponseWriter, r *http.Request) {
	var newService ServiceObject
	err := json.NewDecoder(r.Body).Decode(&newService)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)
	newService.CreatedAt = nairobiTime

	// Insert the new service into MongoDB
	collection := client.Database(dbName).Collection(services_2_collection)
	_, err = collection.InsertOne(context.Background(), newService)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with success message
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newService)
}

// Handler to retrieve services filtered by subcategory
func getServicesBySubcategory(w http.ResponseWriter, r *http.Request) {
	subcategory := r.URL.Query().Get("subcategory")

	// Check if subcategory is "all"
	if subcategory == "all" {
		// Fetch all services from MongoDB
		var services []ServiceObject
		collection := client.Database(dbName).Collection(services_collection)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var service ServiceObject
			err := cursor.Decode(&service)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			services = append(services, service)
		}

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(services)
		return
	}

	// Fetch services from MongoDB based on subcategory
	var services []ServiceObject
	collection := client.Database(dbName).Collection(services_collection)
	cursor, err := collection.Find(context.Background(), bson.M{"subcategory": subcategory})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var service ServiceObject
		err := cursor.Decode(&service)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		services = append(services, service)
	}

	// Respond with the retrieved services
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(services)
}

type CategorizedServices map[string][]ServiceObject

func getServicesBySubcategory2_Sorted(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Get sevices by category hit")

	type ServiceCategoryResponse struct {
		Category []ServiceObject `json:"category"`
		// TotalMoney            int               `json:"total_money"`
		// TotalUpcomingBookings int               `json:"total_upcoming_bookings"`
		Status string `json:"status"`
	}
	subcategory := r.URL.Query().Get("subcategory")

	// Fetch services from MongoDB based on subcategory
	var services []ServiceObject
	collection := client.Database(dbName).Collection(services_collection)
	filter := bson.M{}
	if subcategory != "all" {
		filter["subcategory"] = subcategory
	}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var service ServiceObject
		err := cursor.Decode(&service)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// fmt.Println("Dienstzeiten", service)
		services = append(services, service)

		// if len(services) == 0 {
		// 	fmt.Println("nicht gefunden")

		// 	responseBody := ServiceCategoryResponse{
		// 		Category: services,
		// 		Status:   "1",
		// 	}

		// 	// Respond with the retrieved services
		// 	w.Header().Set("Content-Type", "application/json")
		// 	json.NewEncoder(w).Encode(responseBody)

		// } else {

		// 	responseBody := ServiceCategoryResponse{
		// 		Category: services,
		// 		Status:   "1",
		// 	}

		// 	// Respond with the retrieved services
		// 	w.Header().Set("Content-Type", "application/json")
		// 	json.NewEncoder(w).Encode(responseBody)

		// }

	}

	// If subcategory is "all", categorize services by subcategory
	if subcategory == "all" {
		categorizedServices := make(CategorizedServices)
		for _, service := range services {
			categorizedServices[service.Subcategory] = append(categorizedServices[service.Subcategory], service)
		}

		// Sort services within each subcategory
		for _, services := range categorizedServices {
			sort.Slice(services, func(i, j int) bool {
				return strings.Compare(services[i].Type, services[j].Type) < 0
			})
		}

		// Respond with categorized services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(categorizedServices)
		return
	}

	// // Respond with the retrieved services
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(responseBody)
}

func getServicesBySubcategory2(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get services by category hit")

	type ServiceCategoryResponse struct {
		Category []ServiceObject `json:"child_services"`
		Status   string          `json:"status"`
	}

	subcategory := r.URL.Query().Get("subcategory")

	// Fetch services from MongoDB based on subcategory
	var services []ServiceObject
	collection := client.Database(dbName).Collection(services_collection)
	filter := bson.M{}
	if subcategory != "all" {
		filter["subcategory"] = subcategory
	}
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	for cursor.Next(context.Background()) {
		var service ServiceObject
		if err := cursor.Decode(&service); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		fmt.Println("Dienstzeiten", service)
		services = append(services, service)
	}

	// Check if services were found
	if len(services) == 0 {
		fmt.Println("Services not found")
		responseBody := ServiceCategoryResponse{
			Category: []ServiceObject{},
			Status:   "1", // Indicate no services found
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Respond with the retrieved services
	responseBody := ServiceCategoryResponse{
		Category: services,
		Status:   "1", // Indicate services found
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

type CategorizedServices2 map[string][]PartnerServiceObject

func getPartnerServices(w http.ResponseWriter, r *http.Request) {

	//services = append(services, service)

	//var Pservices []CategorizedServices2 // where all the partners services will be stored

	type ServiceCategoryResponse struct {
		PartnerServices CategorizedServices2 `json:"partner_services"`
		Status          string               `json:"status"`
	}

	fmt.Println("Get partner services called")
	// Get partner ID from request parameters
	partnerID := r.URL.Query().Get("partner_id")
	///partnerID := "661ac34012d273ffc769e27c"
	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific admin from MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	var partner Partner
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&partner)
	if err != nil {
		response := map[string]string{"message": "Service not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Categorize partner services
	categorizedServices := make(CategorizedServices2)
	for _, service := range partner.PartnerServices {
		categorizedServices[service.Subcategory] = append(categorizedServices[service.Subcategory], service)
	}

	// Sort services within each subcategory
	for _, services := range categorizedServices {
		sort.Slice(services, func(i, j int) bool {
			return strings.Compare(services[i].Type, services[j].Type) < 0
		})
	}

	if len(categorizedServices) == 0 {
		fmt.Println("Services not found")
		responseBody := ServiceCategoryResponse{
			PartnerServices: CategorizedServices2{},
			Status:          "1", // Indicate no services found
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// partnerServ = partnerServ.appen()

	//Pservices = append(Pservices, categorizedServices)

	// Respond with the retrieved services
	responseBody := ServiceCategoryResponse{
		PartnerServices: categorizedServices,
		Status:          "1", // Indicate services found
	}

	// // Respond with categorized services
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(categorizedServices)

	// Respond with categorized services
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// func getPartnerServices2(w http.ResponseWriter, r *http.Request) {

// 	//services = append(services, service)

// 	var Pservices []CategorizedServices2 // where all the partners services will be stored

// 	type ServiceCategoryResponse struct {
// 		PartnerServices []CategorizedServices2 `json:"partner_services"`
// 		Status          string               `json:"status"`
// 	}

// 	fmt.Println("Get partner services called")
// 	// Get partner ID from request parameters
// 	partnerID := r.URL.Query().Get("partner_id")
// 	///partnerID := "661ac34012d273ffc769e27c"
// 	// Convert the user ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(partnerID)
// 	if err != nil {
// 		http.Error(w, "Invalid User ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve the specific admin from MongoDB
// 	collection := client.Database(dbName).Collection(partner_collection)
// 	var partner Partner
// 	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&partner)
// 	if err != nil {
// 		response := map[string]string{"message": "Service not found", "status": "0"}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}

// 	// Categorize partner services
// 	categorizedServices := make(CategorizedServices2)
// 	for _, service := range partner.PartnerServices {
// 		categorizedServices[service.Subcategory] = append(categorizedServices[service.Subcategory], service)
// 	}

// 	// Sort services within each subcategory
// 	for _, services := range categorizedServices {
// 		sort.Slice(services, func(i, j int) bool {
// 			return strings.Compare(services[i].Type, services[j].Type) < 0
// 		})
// 	}

// 	if len(categorizedServices) == 0 {
// 		fmt.Println("Services not found")
// 		responseBody := ServiceCategoryResponse{
// 			PartnerServices: []CategorizedServices2{},
// 			Status:          "1", // Indicate no services found
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// partnerServ = partnerServ.appen()

// 	Pservices = append(Pservices, categorizedServices)

// 	// Respond with the retrieved services
// 	responseBody := ServiceCategoryResponse{
// 		PartnerServices: Pservices,
// 		Status:          "1", // Indicate services found
// 	}

// 	// // Respond with categorized services
// 	// w.Header().Set("Content-Type", "application/json")
// 	// json.NewEncoder(w).Encode(categorizedServices)

// 	// Respond with categorized services
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

// type CategorizedServices2 map[string][]PartnerServiceObject

// // type CategorizedServices2 map[string][]PartnerServiceObject
// type CategorizedServices2 map[string][]PartnerServiceObject

// func getPartnerServices2(w http.ResponseWriter, r *http.Request) {
// 	type ServiceCategoryResponse struct {
// 		PartnerServices [][]map[string][]PartnerServiceObject `json:"partner_services"`
// 		Status          string                                `json:"status"`
// 	}

// 	fmt.Println("Get partner services called")

// 	// Get partner ID from request parameters and convert to lowercase
// 	partnerID := strings.ToLower(r.URL.Query().Get("partner_id"))
// 	if partnerID == "" {
// 		http.Error(w, "Partner ID is required", http.StatusBadRequest)
// 		return
// 	}

// 	// Convert the partner ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(partnerID)
// 	if err != nil {
// 		http.Error(w, "Invalid Partner ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve the specific partner from MongoDB
// 	collection := client.Database(dbName).Collection(partner_collection)
// 	var partner Partner
// 	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&partner)
// 	if err != nil {
// 		response := map[string]string{"message": "Service not found", "status": "0"}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}

// 	// Categorize partner services
// 	categorizedServices := make(CategorizedServices2)
// 	for _, service := range partner.PartnerServices {
// 		categorizedServices[service.Subcategory] = append(categorizedServices[service.Subcategory], service)
// 	}

// 	// Sort services within each subcategory
// 	for _, services := range categorizedServices {
// 		sort.Slice(services, func(i, j int) bool {
// 			return strings.Compare(services[i].Type, services[j].Type) < 0
// 		})
// 	}

// 	// Create the partner services response
// 	var partnerServicesResponse [][]map[string][]PartnerServiceObject
// 	for category, services := range categorizedServices {
// 		categoryServices := map[string][]PartnerServiceObject{
// 			category: services,
// 		}
// 		partnerServicesResponse = append(partnerServicesResponse, []map[string][]PartnerServiceObject{categoryServices})
// 	}

// 	if len(partnerServicesResponse) == 0 {
// 		fmt.Println("Services not found")
// 		responseBody := ServiceCategoryResponse{
// 			PartnerServices: [][]map[string][]PartnerServiceObject{},
// 			Status:          "0", // Indicate no services found
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Respond with the retrieved services
// 	responseBody := ServiceCategoryResponse{
// 		PartnerServices: partnerServicesResponse,
// 		Status:          "1", // Indicate services found
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }
// type ServiceCategory struct {
// 	CategoryName string                `json:"category_name"`
// 	Services     []PartnerServiceObject `json:"services"`
// }

type Data_Services struct {
	Message string `json:"message"`
	//Token      string  `json:"token"`
	//Partner    Partner `json:"partner"`
	//Partner_ID string  `json:"partner_id"`
	//PartnerID string  `json:"token,omitempty"`
	// Status string `json:"status,omitempty"`

	PartnerServices []ServiceCategory2 `partner_services`
}

type ServiceCategoryResponse struct {
	Message         string             `json:"message"`
	Data            Data_Services      `json:"data"`
	PartnerServices []ServiceCategory2 `json:"partner_services"`
	Status          string             `json:"status"`
}

type ServiceCategory2 struct {
	CategoryName string                 `json:"category_name"`
	Services     []PartnerServiceObject `json:"services"`
}

func getPartnerServices2(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get partner services called")

	// Get partner ID from request parameters
	partnerID := r.URL.Query().Get("partner_id")
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific partner from MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	var partner Partner
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&partner)
	if err != nil {
		response := map[string]string{"message": "Service not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Categorize partner services
	categorizedServices := make(map[string][]PartnerServiceObject)
	for _, service := range partner.PartnerServices {
		categorizedServices[service.Subcategory] = append(categorizedServices[service.Subcategory], service)
	}

	// Sort services within each subcategory
	for _, services := range categorizedServices {
		sort.Slice(services, func(i, j int) bool {
			return strings.Compare(services[i].Type, services[j].Type) < 0
		})
	}

	// Transform categorized services into the desired response format
	var serviceCategories []ServiceCategory2
	for category, services := range categorizedServices {
		serviceCategories = append(serviceCategories, ServiceCategory2{
			CategoryName: category,
			Services:     services,
		})
	}

	// // Prepare response
	// responseBody := ServiceCategoryResponse{
	// 	PartnerServices: serviceCategories,
	// 	Status:          "1", // Indicate services found
	// }

	// type Data_Services struct {
	// 	Message string `json:"message"`
	// 	//Token      string  `json:"token"`
	// 	//Partner    Partner `json:"partner"`
	// 	//Partner_ID string  `json:"partner_id"`
	// 	//PartnerID string  `json:"token,omitempty"`
	// 	// Status string `json:"status,omitempty"`

	// 	PartnerServices []ServiceCategory2 `partner_services`
	// }

	data := Data_Services{

		// Partner_ID: storedUser.ID.Hex(),
		PartnerServices: serviceCategories,
		// Token:      token,
	}

	responseBody := ServiceCategoryResponse{
		Message: "",
		Data:    data,
		Status:  "Success",
	}

	if len(serviceCategories) == 0 {
		fmt.Println("Services not found")
		responseBody.Status = "0" // Indicate no services found
	}

	// Respond with categorized services
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// func getPartnerServices2(w http.ResponseWriter, r *http.Request) {
// 	var Pservices [][]PartnerServiceObject // Array of arrays to store categorized services

// 	type ServiceCategoryResponse struct {
// 		PartnerServices [][]PartnerServiceObject `json:"partner_services"`
// 		Status          string                    `json:"status"`
// 	}

// 	fmt.Println("Get partner services called")

// 	// Get partner ID from request parameters and convert to lowercase
// 	partnerID := strings.ToLower(r.URL.Query().Get("partner_id"))
// 	if partnerID == "" {
// 		http.Error(w, "Partner ID is required", http.StatusBadRequest)
// 		return
// 	}

// 	// Convert the partner ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(partnerID)
// 	if err != nil {
// 		http.Error(w, "Invalid Partner ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve the specific partner from MongoDB
// 	collection := client.Database(dbName).Collection(partner_collection)
// 	var partner Partner
// 	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&partner)
// 	if err != nil {
// 		response := map[string]string{"message": "Service not found", "status": "0"}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}

// 	// Categorize partner services
// 	categorizedServices := make(CategorizedServices2)
// 	for _, service := range partner.PartnerServices {
// 		categorizedServices[service.Subcategory] = append(categorizedServices[service.Subcategory], service)
// 	}

// 	// Sort services within each subcategory
// 	for _, services := range categorizedServices {
// 		sort.Slice(services, func(i, j int) bool {
// 			return strings.Compare(services[i].Type, services[j].Type) < 0
// 		})
// 	}

// 	// Append categorized services to Pservices
// 	for _, services := range categorizedServices {
// 		Pservices = append(Pservices, services)
// 	}

// 	if len(Pservices) == 0 {
// 		fmt.Println("Services not found")
// 		responseBody := ServiceCategoryResponse{
// 			PartnerServices: [][]PartnerServiceObject{},
// 			Status:          "0", // Indicate no services found
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Respond with the retrieved services
// 	responseBody := ServiceCategoryResponse{
// 		PartnerServices: Pservices,
// 		Status:          "1", // Indicate services found
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

// type CategorizedServices2 map[string][]PartnerServiceObject

// func getPartnerServices(w http.ResponseWriter, r *http.Request) {
// 	type ServiceCategory struct {
// 		Category string                `json:"category"`
// 		Services []PartnerServiceObject `json:"services"`
// 	}

// 	type ServiceCategoryResponse struct {
// 		PartnerServices []ServiceCategory `json:"partner_services"`
// 		Status          string            `json:"status"`
// 	}

// 	fmt.Println("Get partner services called")
// 	// Get partner ID from request parameters
// 	partnerID := r.URL.Query().Get("partner_id")
// 	if partnerID == "" {
// 		http.Error(w, "Partner ID is required", http.StatusBadRequest)
// 		return
// 	}

// 	// Convert the partner ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(partnerID)
// 	if err != nil {
// 		http.Error(w, "Invalid Partner ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve the specific partner from MongoDB
// 	collection := client.Database(dbName).Collection(partner_collection)
// 	var partner Partner
// 	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&partner)
// 	if err != nil {
// 		response := map[string]string{"message": "Service not found", "status": "0"}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}

// 	// Categorize partner services
// 	categorizedServices := make(CategorizedServices2)
// 	for _, service := range partner.PartnerServices {
// 		categorizedServices[service.Subcategory] = append(categorizedServices[service.Subcategory], service)
// 	}

// 	// Sort services within each subcategory
// 	for _, services := range categorizedServices {
// 		sort.Slice(services, func(i, j int) bool {
// 			return strings.Compare(services[i].Type, services[j].Type) < 0
// 		})
// 	}

// 	// Convert map to slice of ServiceCategory
// 	var serviceCategories []ServiceCategory
// 	for category, services := range categorizedServices {
// 		serviceCategories = append(serviceCategories, ServiceCategory{
// 			Category: category,
// 			Services: services,
// 		})
// 	}

// 	if len(serviceCategories) == 0 {
// 		fmt.Println("Services not found")
// 		responseBody := ServiceCategoryResponse{
// 			PartnerServices: []ServiceCategory{},
// 			Status:          "0", // Indicate no services found
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Respond with the retrieved services
// 	responseBody := ServiceCategoryResponse{
// 		PartnerServices: serviceCategories,
// 		Status:          "1", // Indicate services found
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }
//handleGetServicesCategory

func handleGetServices(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get partner services called")

	type Category struct {
		Name    string `json:"category_name"`
		Picture string `json:"category_picture"`
	}

	filter := bson.M{}
	var all_services []ServiceObject

	collection := client.Database(dbName).Collection(services_collection)

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the records slice
	for cursor.Next(context.Background()) {
		var service ServiceObject
		if err := cursor.Decode(&service); err != nil {
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}
		all_services = append(all_services, service)
	}

	//var services []ServiceObject
	// if err := json.Unmarshal(all_services, &services); err != nil {
	//     fmt.Println("Error:", err)
	//     return
	// }

	var subcategories []Category
	for _, service := range all_services {

		responseBody := Category{
			Name: service.Subcategory,
		}

		subcategories = append(subcategories, responseBody)
	}

	fmt.Println(subcategories)

	// Respond with categorized services
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(subcategories)
}

//Add service category

func uploadServiceCategoryPic(w http.ResponseWriter, r *http.Request) {
	// Parse the multipart form data
	err := r.ParseMultipartForm(10 << 20) // 10MB max size
	if err != nil {
		http.Error(w, "Failed to parse multipart form", http.StatusBadRequest)
		return
	}

	// Retrieve the uploaded file
	file, fileHeader, err := r.FormFile("category-photo")
	if err != nil {
		http.Error(w, "Failed to retrieve uploaded file", http.StatusBadRequest)
		return
	}
	defer file.Close()

	// Read the contents of the uploaded file
	data, err := io.ReadAll(file)
	if err != nil {
		http.Error(w, "Failed to read file contents", http.StatusInternalServerError)
		return
	}

	// MongoDB connection URI
	//uri := "mongodb://localhost:27017"

	// Connect to MongoDB
	//client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(uri))

	// Open GridFS bucket
	database := client.Database("category-bucket")
	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		http.Error(w, "Failed to open GridFS bucket", http.StatusInternalServerError)
		return
	}

	// Upload the image to MongoDB GridFS
	uploadStream, err := bucket.OpenUploadStream(fileHeader.Filename)

	if err != nil {
		fmt.Println("ERRO", err)
		http.Error(w, "Failed to open GridFS upload stream", http.StatusInternalServerError)
		return
	}
	defer uploadStream.Close()

	_, err = uploadStream.Write(data)
	if err != nil {
		http.Error(w, "Failed to write file to GridFS", http.StatusInternalServerError)
		return
	}

	fileId, err := json.Marshal(uploadStream.FileID)
	if err != nil {
		log.Fatal(err)
		//.JSON(http.StatusBadRequest, err.Error())
		return

	}

	//fmt.Println("Retrieved file", fileId)

	pic_id := strings.TrimSpace(string(fileId))

	fileIds := strings.Trim(pic_id, `"`)

	fmt.Print()

	BASE_URL := "https://api.finelooks.co.uk"

	imageURL := BASE_URL + "/category-view/" + fileIds

	// Respond with the image URL
	//w.WriteHeader(http.StatusOK)
	//w.Write([]byte(imageURL))

	responseBody := ImageUploadResponse{
		Message:  "Image Upload successfully",
		ImageURL: imageURL,
		Status:   "1",
	}

	// Respond with the image URL
	//w.WriteHeader(http.StatusOK)
	//w.Write([]byte(imageURL))
	//w.Write([]byte(responseBody))
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// func storeServiceCategory(w http.ResponseWriter, r *http.Request) {
// 	var serviceCategory ServiceCategory
// 	err := json.NewDecoder(r.Body).Decode(&serviceCategory)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusBadRequest)
// 		return
// 	}

// 	nairobiTime := ConvertToNairobiTime()
// 	fmt.Println("Nairobi Time:", nairobiTime)
// 	serviceCategory.CreatedAt = nairobiTime

// 	// Insert the new service into MongoDB
// 	collection := client.Database(dbName).Collection(categories_collection)
// 	_, err = collection.InsertOne(context.Background(), serviceCategory)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusInternalServerError)
// 		return
// 	}

// 	// Respond with success message
// 	w.WriteHeader(http.StatusCreated)
// 	json.NewEncoder(w).Encode(serviceCategory)
// }

func storeServiceCategory(w http.ResponseWriter, r *http.Request) {
	var serviceCategory ServiceCategory
	err := json.NewDecoder(r.Body).Decode(&serviceCategory)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)
	serviceCategory.CreatedAt = nairobiTime

	// Insert the new service into MongoDB
	collection := client.Database(dbName).Collection(categories_collection)
	_, err = collection.InsertOne(context.Background(), serviceCategory)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Retrieve the updated list of service categories
	cursor, err := collection.Find(context.Background(), bson.M{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	var serviceCategories []ServiceCategory
	if err = cursor.All(context.Background(), &serviceCategories); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with the updated service category list
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(serviceCategories)
}

func handleViewServiceCategoryPic(w http.ResponseWriter, r *http.Request) {
	// Extract the file ID from the request or use a default one for testing purposes.
	//id, _ := primitive.ObjectIDFromHex("6613ee0a363a5bbdf7ba60e3")

	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		//http.Error(w, "Invalid User", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Invalid User ID",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Get a handle to the MongoDB database.
	database := client.Database("category-bucket")

	// Initialize the GridFS bucket.
	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		//http.Error(w, "Failed to open GridFS bucket", http.StatusInternalServerError)

		responseBody := ErrorResponse{
			Message: "Failed to open GridFS bucket",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Open a download stream for the file.
	downloadStream, err := bucket.OpenDownloadStream(objID)
	if err != nil {
		//http.Error(w, "Failed to open download stream", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to open download stream",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	//defer downloadStream.Close()

	// Read the file data.
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, downloadStream); err != nil {
		//http.Error(w, "Failed to read file", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Failed to read file",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Set the appropriate content type based on the file type.
	contentType := http.DetectContentType(buf.Bytes())
	w.Header().Set("Content-Type", contentType)

	// Write the file data to the response writer.
	if _, err := io.Copy(w, &buf); err != nil {
		//http.Error(w, "Failed to write file to response", http.StatusInternalServerError)

		responseBody := ErrorResponse{
			Message: "Failed to write file to response",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
}

func handleGetServiceCategories(w http.ResponseWriter, r *http.Request) {

	type DataServiceCategoriesUpdate struct {
		//Customer Customer `json:"customer"`
		Categories []ServiceCategory `json:"categories"`
	}

	type CustomerPhoneNumberUpdateResponse struct {
		Message string `json:"message"`

		Data DataServiceCategoriesUpdate `json:"data"`

		Status string `json:"status"`
	}

	type ServiceCategoriesResponse struct {
		Categories []ServiceCategory `json:"categories"`
		// TotalMoney            int               `json:"total_money"`
		// TotalUpcomingBookings int               `json:"total_upcoming_bookings"`
		Status string `json:"status"`
	}
	collection := client.Database(dbName).Collection(categories_collection)

	fmt.Println("Get all customers called")

	// Define a slice to store retrieved records
	var categories []ServiceCategory

	// Define a filter to retrieve only active records
	//filter := bson.M{"status": "active"}
	filter := bson.M{}

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the records slice
	for cursor.Next(context.Background()) {
		var record ServiceCategory
		if err := cursor.Decode(&record); err != nil {
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}

		fmt.Println("record service_category", record)
		categories = append(categories, record)
	}

	fmt.Println("number of services", len(categories))

	// responseBody := ServiceCategoriesResponse{
	// 	Categories: categories,
	// 	Status:     "1",
	// }

	data := DataServiceCategoriesUpdate{

		// Partner_ID: storedUser.ID.Hex(),
		Categories: categories,
		// Token:      token,
	}

	responseBody := CustomerPhoneNumberUpdateResponse{
		Message: "Customer Service Categories Retrieved Successfully",
		Data:    data,
		Status:  "Success",
	}

	//fmt.Println("response body", responseBody)

	// Respond with the retrieved bookings or message in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

	// // Respond with the retrieved records in JSON format
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(categories)
}

// ///
func handleGetChildrenServices(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Welcome to protected area")

	type ChildService struct {
		ServiceID string `json:"service_id"`
		Type      string `json:"type"`
		//Status          string `json:"status"`
		Description  string `json:"description"`
		Localization string `json:"localization"`
		Subcategory  string `json:"subcategory"`
		//Price           string `json:"-"`
		//ServiceDuration string `json:"service_duration"`
		CreatedAt string `json:"created_at"`
	}

	type Response struct {
		//Partners        []Partner              `json:"partners"`
		ChildServices []ChildService `json:"child_services"`
		Status        string         `json:"status"`
	}

	// Parse query parameters
	queryValues := r.URL.Query()
	subcategory := queryValues.Get("subcategory")

	collection := client.Database(dbName).Collection(partner_collection)
	fmt.Println("Get services for subcategory:", subcategory)

	//var partners []Partner
	var allPartnerServices []PartnerServiceObject

	cursor, err := collection.Find(context.Background(), bson.M{})
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	for cursor.Next(context.Background()) {
		var record Partner
		if err := cursor.Decode(&record); err != nil {
			fmt.Println("ERROR", err)
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}
		//partners = append(partners, record)
		allPartnerServices = append(allPartnerServices, record.PartnerServices...)
	}

	if err := cursor.Err(); err != nil {
		http.Error(w, "Cursor error", http.StatusInternalServerError)
		return
	}

	// Filter services based on subcategory
	// var filteredServices []PartnerServiceObject
	// for _, service := range allPartnerServices {
	// 	if service.Subcategory == subcategory {
	// 		filteredServices = append(filteredServices, service)
	// 	}
	// }

	var filteredServices []ChildService
	addedSubcategories := make(map[string]bool)

	for _, service := range allPartnerServices {
		if service.Subcategory == subcategory && service.ServiceID != "" && !addedSubcategories[service.Type] {

			service := ChildService{
				ServiceID: service.ServiceID,
				Type:      service.Type,
				//Status:          "active",
				Description:  service.Description,
				Localization: service.Localization,
				Subcategory:  service.Subcategory,
				CreatedAt:    "2024-05-17 09:38:38",
			}

			filteredServices = append(filteredServices, service)
			addedSubcategories[service.Subcategory] = true
		}
	}

	// service := ChildService{
	// 	ServiceID:       "9ELWFAONWP",
	// 	Type:            "Acrylics",
	// 	Status:          "active",
	// 	Description:     "",
	// 	Localization:    "Kenya",
	// 	Subcategory:     "Nails",
	// 	Price:           "4000",
	// 	ServiceDuration: "4",
	// 	CreatedAt:       "2024-05-17 09:38:38",
	// }

	initializePartnersStatus()
	initializeCustomerStatus()

	if len(filteredServices) == 0 {

		response := Response{
			//Partners:       partners,
			ChildServices: []ChildService{},
			Status:        "1",
		}

		// Convert filtered services slice to JSON
		jsonData, err := json.Marshal(response)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		// Respond with the filtered services in JSON format
		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonData)
		return

	}

	response := Response{
		//Partners:       partners,
		ChildServices: filteredServices,
		Status:        "1",
	}

	// Convert filtered services slice to JSON
	jsonData, err := json.Marshal(response)
	if err != nil {
		http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
		return
	}

	// Respond with the filtered services in JSON format
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}
