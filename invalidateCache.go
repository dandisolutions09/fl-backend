package main

import "fmt"

func invalidatePartnerCache() {
	// Invalidate cache for general partner data
	partnersCache.Delete("partners")

	// Invalidate cache for active partner data
	partnersCache.Delete("active-partners")

	fmt.Println("successfully invalidated Partner Cache")
}
