package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func handleUpdateCreatedAtDate(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record createdAt!")
	params := mux.Vars(r)
	partnerID := params["id"]
	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", partnerID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Decode the JSON request body into a Person struct
	var updateData Partner
	//if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
	//http.Error(w, "Failed to decode request body", http.StatusBadRequest)

	if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
		//http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "Failed to decode request body",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	if updateData.CreatedAt == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: Current Date",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": bson.M{"created_at": updateData.CreatedAt}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		// http.Error(w, "Error updating record", http.StatusInternalServerError)
		// fmt.Println("errr", err)

		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("errr", err)
		return
	}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_partner Partner

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	// responseBody := RegistrationResponse{
	// 	Message: "Partner Profile Picture URL updated successfully",
	// 	Partner: updated_partner,
	// 	Status:  "Success",
	// }

	data := Data{

		// Partner_ID: storedUser.ID.Hex(),
		Partner: updated_partner,
		// Token:      token,
	}

	responseBody := RegistrationResponse{
		Message: "Partner Logo URL updated successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	invalidatePartnerCache()

	// Respond with a success message
	//response := map[string]string{"message": "Partner Profile Picture updated successfully", "status:": "1"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}
