#!/bin/bash

# Function to print messages
print_message() {
  echo "**** $1"
}

print_message "Pulling latest changes..."
git pull

if [ $? -eq 0 ]; then
  print_message "Changes pulled successfully."
else
  print_message "Failed to pull changes."
  exit 1
fi

# Stop and remove existing backend-container if it exists
print_message "Stopping and removing the container if it exists..."
docker rm -f backend-container >/dev/null 2>&1

if [ $? -eq 0 ]; then
  print_message "Container stopped and removed successfully."
else
  print_message "No existing container found, or failed to remove."
fi

# Build the backend-image
print_message "Building the Docker image..."
docker build -t backend-image .

if [ $? -eq 0 ]; then
  print_message "Successfully built the image."
else
  print_message "Failed to build the image."
  exit 1
fi

# Run the container in detached mode and show logs
print_message "Running the container..."
docker run -d --network host --name backend-container backend-image

if [ $? -eq 0 ]; then
  print_message "Container is running."
else
  print_message "Failed to start the container."
  exit 1
fi

print_message "Following the container logs..."
docker logs -f backend-container
