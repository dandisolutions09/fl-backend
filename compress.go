package main

import (
	//"fmt"
	"image"
	//"image/jpeg"
	//"io"
	//"log"
	//"net/http"

	"github.com/nfnt/resize"
)

// ResizeImage resizes the given image to the specified width and height, preserving the aspect ratio if height is 0.
func ResizeImage(img image.Image, width uint, height uint) image.Image {
	return resize.Resize(width, height, img, resize.Lanczos3)
}
