package main

import (
	//"context"
	//"encoding/json"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	//"go.mongodb.org/mongo-driver/bson"
)

// GoogleAPIKey is your Google Maps API key
const GoogleAPIKey = "YOUR_API_KEY"

// UserLocation represents the user's location
type UserLocation struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

// NearbyLocation represents a nearby location
type NearbyLocation struct {
	Name      string  `json:"name"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func googletest(location string) ([]byte, error) {
	// URL for the Google Places Autocomplete API
	url := fmt.Sprintf("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyDolZ9dXT3cqWqqHt6XTyojZlzt1EmtZoo&input=%s&types=geocode&components=country:KE", location)

	// Send GET request
	response, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("error making GET request: %v", err)
	}
	defer response.Body.Close()

	// Read response body
	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response body: %v", err)
	}

	return body, nil
}

func handleGetSuggestions(w http.ResponseWriter, r *http.Request) {
	// Get the location query parameter
	location := r.URL.Query().Get("location")

	// Call the googletest function to get suggestions
	suggestions, err := googletest(location)
	if err != nil {
		// Handle the error
		http.Error(w, "Error retrieving suggestions", http.StatusInternalServerError)
		return
	}

	// Set the response content type to JSON
	w.Header().Set("Content-Type", "application/json")

	// Write the suggestions as JSON response
	w.Write(suggestions)
}

//GET  NEARBY LOCATIONS

func handleNearbyLocations(w http.ResponseWriter, r *http.Request) {
	// Parse user's location from request body
	var userLocation UserLocation
	if err := json.NewDecoder(r.Body).Decode(&userLocation); err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	// Query Google Places API to find nearby locations
	nearbyLocations, err := getNearbyLocations(userLocation)
	if err != nil {
		http.Error(w, "Failed to fetch nearby locations", http.StatusInternalServerError)
		return
	}

	// Encode and send nearby locations as JSON response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(nearbyLocations)
}

func getNearbyLocations(userLocation UserLocation) ([]NearbyLocation, error) {
	// Query Google Places API to find nearby locations
	// You can customize the radius, type of places, etc., according to your requirements
	// Here, we're querying for 'restaurant' places within a 10 km radius of the user's location
	url := fmt.Sprintf("https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=%s&location=%f,%f&radius=10000&type=restaurant",
		GoogleAPIKey, userLocation.Latitude, userLocation.Longitude)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var nearbyLocations struct {
		Results []struct {
			Name     string `json:"name"`
			Geometry struct {
				Location struct {
					Lat float64 `json:"lat"`
					Lng float64 `json:"lng"`
				} `json:"location"`
			} `json:"geometry"`
		} `json:"results"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&nearbyLocations); err != nil {
		return nil, err
	}

	// Convert Google Places API response to NearbyLocation struct
	var nearbyLocationsList []NearbyLocation
	for _, result := range nearbyLocations.Results {
		nearbyLocationsList = append(nearbyLocationsList, NearbyLocation{
			Name:      result.Name,
			Latitude:  result.Geometry.Location.Lat,
			Longitude: result.Geometry.Location.Lng,
		})
	}

	return nearbyLocationsList, nil
}
