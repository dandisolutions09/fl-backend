package main

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	// "time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	//"github.com/gorilla/mux"
	//"go.mongodb.org/mongo-driver/bson"
	//"go.mongodb.org/mongo-driver/bson/primitive"
)

type AdminLoginResponse struct {
	Message  string `json:"message"`
	Token    string `json:"token,omitempty"`
	Admin_ID string `json:"admin_id,omitempty"`
	Status   string `json:"status,omitempty"`
	Type     string `json:"type,omitempty"`
	Hash     string `json:"hash,omitempty"`
}

func RegisterAdmin(w http.ResponseWriter, r *http.Request) {

	fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	var inc_data Admin
	err := decoder.Decode(&inc_data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("incoming data", inc_data)

	// // Check if the username or email is taken
	// if isUsernameTaken(inc_data.Username) {
	// 	http.Error(w, "Username-already-taken", http.StatusBadRequest)
	// 	return
	// }

	if isAdminEmailTaken(inc_data.Email) {
		http.Error(w, "Email-already-taken", http.StatusBadRequest)
		return
	}

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(inc_data.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	inc_data.Password = hashedPassword
	inc_data.Salt = salt

	// fmt.Println("to be saved in db:->", inc_data)
	// fmt.Println("FULLNAME", inc_data.First_name+" "+inc_data.Last_name)
	fmt.Println("FULLNAME:", inc_data.First_name+" "+inc_data.Last_name)

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)
	inc_data.CreatedAt = nairobiTime

	collection := client.Database(dbName).Collection(admin_collection)
	_, err = collection.InsertOne(context.Background(), inc_data)
	fmt.Print(inc_data)
	if err != nil {
		http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	// fmt.Fprintf(w, "Nurse inserted Successfully")

	//response := map[string]string{"message": "User inserted successfully"}
	fmt.Println("User inserted successfully", inc_data.Email)

	url := "http://localhost:8080/get-admin-by-email/" + inc_data.Email

	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Error making GET request", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	storedUserData, err := io.ReadAll(response.Body)
	if err != nil {
		http.Error(w, "Error reading response body", http.StatusInternalServerError)
		return
	}

	var storedUser Admin
	if err := json.Unmarshal(storedUserData, &storedUser); err != nil {
		http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)
		return
	}

	responseBody := AdminRegistrationResponse{
		Message: "Registration Successful",
		//Token:   token,
		Admin_ID: storedUser.ID.Hex(),
		//Status:     storedUser.Status,
	}
	//json.NewEncoder(w).Encode(response)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

}

func handleAdminLogin(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var u Admin
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Make a GET request to retrieve the stored hash from another endpoint
	//url := "http://localhost:8080/get-username/" + u.Username
	//url := "http://localhost:8080/get-userid/" + u.ID.Hex()
	url := "http://localhost:8080/get-admin-by-email/" + u.Email

	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Error making GET request", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	storedHash, err := io.ReadAll(response.Body)
	if err != nil {
		http.Error(w, "Error reading response body", http.StatusInternalServerError)
		return
	}

	var storedUser Admin
	if err := json.Unmarshal(storedHash, &storedUser); err != nil {
		http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)
		return
	}

	fmt.Println("stored user", storedUser)

	passwordsMatch := comparePasswords(u.Password, storedUser.Password, storedUser.Salt)
	if passwordsMatch {
		token, _ := createToken(u.First_name + u.Last_name)

		response := AdminLoginResponse{
			Message:  "Authentication successful",
			Token:    token,
			Admin_ID: storedUser.ID.Hex(),
			Type:     storedUser.Type,
			Hash:     storedUser.Password,
			//Status:  storedUser.Status,
		}
		json.NewEncoder(w).Encode(response)
	} else {
		//http.Error(w, "Authentication failed", http.StatusUnauthorized)
		response := LoginResponse{
			Message: "AWrong Login Information",
			//Token:   "your_generated_token",
			//User:    storedUser,
			//Status:  storedUser.Status,
		}
		json.NewEncoder(w).Encode(response)
	}
}

func handleGetAdmins(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(admin_collection)

	fmt.Println("Get all partners called")

	// Define a slice to store retrieved records
	var admins []Admin

	// Define a filter to retrieve only active records
	//filter := bson.M{"status": "active"}
	filter := bson.M{}

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the records slice
	for cursor.Next(context.Background()) {
		var record Admin
		if err := cursor.Decode(&record); err != nil {
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}
		admins = append(admins, record)
	}

	// Respond with the retrieved records in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(admins)
}

func handleGetAdminById(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get admin by ID has been called....")

	// Get the admin ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	fmt.Println("Passed user ID:", userID)

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific admin from MongoDB
	collection := client.Database(dbName).Collection(admin_collection)
	var admin Admin
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&admin)
	if err != nil {
		response := map[string]string{"message": "Admin not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved admin in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(admin)
}

func handleUpdateAdmin(w http.ResponseWriter, r *http.Request) {
	// Get the user ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Admin
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// if isAdminEmailTaken(updatedData.Email) {
	// 	http.Error(w, "Email-already-taken", http.StatusBadRequest)
	// 	return
	// }

	// Exclude password and salt from the update
	updateData := bson.M{}
	var updateResult *mongo.UpdateResult

	if updatedData.Email != "" {

		updateData["email"] = updatedData.Email

	}
	if updatedData.First_name != "" {
		updateData["first_name"] = updatedData.First_name
	}
	if updatedData.Last_name != "" {
		updateData["last_name"] = updatedData.Last_name
	}

	if updatedData.Type != "" {
		updateData["type"] = updatedData.Type
	}
	// if updatedData.Status != "" {
	// 	updateData["status"] = updatedData.Status
	// }

	// if updatedData.Role != "" {
	// 	updateData["role"] = updatedData.Role
	// }
	// Add other fields that you want to update...

	// Update the specific user in MongoDB
	collection := client.Database(dbName).Collection(admin_collection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updateData}

	updateResult, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating user", http.StatusInternalServerError)
		return
	}

	// Check if any document was modified
	if updateResult.ModifiedCount == 0 {
		// No document was modified
		responseBody := ErrorResponse{
			Message: "User Not Found",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "User updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleUpdateAdminPassword(w http.ResponseWriter, r *http.Request) {
	// Get the user ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var incomingPassword Admin
	err = decoder.Decode(&incomingPassword)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("INCOMING DATA:->", incomingPassword, objID)

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// // Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(incomingPassword.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	fmt.Println("GENERATED HASH", hashedPassword)
	fmt.Println("GENERATED SALT", salt)

	updateData := bson.M{}
	var updateResult *mongo.UpdateResult

	if incomingPassword.Password != "" {
		updateData["password"] = hashedPassword
		fmt.Println("NEW HASH", updateData["password"])
		updateData["salt"] = salt
		fmt.Println("NEW SALT", updateData["salt"])

		//Update the specific user in MongoDB
		collection := client.Database(dbName).Collection(admin_collection)
		filter := bson.M{"_id": objID}
		update := bson.M{"$set": updateData}

		updateResult, err = collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, "Error updating user", http.StatusInternalServerError)
			return
		}

		// Check if any document was modified
		if updateResult.ModifiedCount == 0 {
			// No document was modified
			// responseBody := ErrorResponse{
			// 	Message: "User Not Found",
			// 	Status:  "Error",
			// }

			// w.Header().Set("Content-Type", "application/json")
			// json.NewEncoder(w).Encode(responseBody)

			http.Error(w, "User not found", http.StatusBadRequest)
			return

		}

		// Respond with a success message
		response := map[string]string{"message": "User updated successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {
		fmt.Println("password not provided")
		responseBody := ErrorResponse{
			Message: "Password not provided",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

}
