package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func handleAddAvailabilityDash(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Availability!")
	params := mux.Vars(r)
	partnerID := params["id"]
	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", partnerID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Decode the JSON request body into a Person struct
	var updateData Availability
	if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
		//http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "Failed to decode request body",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	fmt.Println("partner availability-->", updateData)

	if updateData.Monday.OpeningTime == "" ||
		updateData.Tuesday.OpeningTime == "" ||
		updateData.Wednesday.OpeningTime == "" ||
		updateData.Thursday.OpeningTime == "" ||
		updateData.Friday.OpeningTime == "" ||
		updateData.Saturday.OpeningTime == "" ||
		updateData.Sunday.OpeningTime == "" ||

		updateData.Monday.ClosingTime == "" ||
		updateData.Tuesday.ClosingTime == "" ||
		updateData.Wednesday.ClosingTime == "" ||
		updateData.Thursday.ClosingTime == "" ||
		updateData.Friday.ClosingTime == "" ||
		updateData.Saturday.ClosingTime == "" ||
		updateData.Sunday.ClosingTime == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing one or more required fields:",
			Status:  "Error",
		}

		fmt.Printf("Missing or or more required fields")

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	fmt.Println("update Data:-->", updateData)

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}

	// Define the update operation to update multiple fields
	update := bson.M{
		"$set": bson.M{
			//"service_type": updateData.ServiceType,
			//"payment_type": updateData.PaymentOption,
			"isAvailabilitySet":     "YES",
			"partner_availability":  updateData,
			"provider_availability": GetAvailabilityResponse(updateData).PartnerAvailability,
		},
	}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_partner Partner

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}


	initializePartnersStatus()
	initializeCustomerStatus()

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	// responseBody := RegistrationResponse{
	// 	Message: "Partner Availability updated successfully",
	// 	Partner: updated_partner,
	// 	Status:  "Success",
	// }

	data := Data{

		// Partner_ID: storedUser.ID.Hex(),
		Partner: updated_partner,
		// Token:      token,
	}

	responseBody := RegistrationResponse{
		Message: "Partner Availability updated successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	invalidatePartnerCache()

	// Respond with a success message
	//response := map[string]string{"message": "Partner Availability updated successfully", "status:": "1"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

func handleAddAvailability(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Availability!")
	params := mux.Vars(r)
	partnerID := params["id"]
	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", partnerID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// type AvailabilityDay struct {
	// 	Day         string `json:"day"`
	// 	OpeningTime string `json:"opening_time"`
	// 	ClosingTime string `json:"closing_time"`
	// }

	var incomingData PartnerAvailabilityResponse

	if err := json.NewDecoder(r.Body).Decode(&incomingData); err != nil {
		//http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "Failed to decode request body",
			Status:  "Error",
		}

		var updateData Availability
		if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
			//http.Error(w, "Failed to decode request body", http.StatusBadRequest)
			responseBody := ErrorResponse{
				Message: "Failed to decode request body",
				Status:  "Error",
			}

			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(responseBody)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	fmt.Println("incoming availability", incomingData.PartnerAvailability[0].Day)

	// Decode the JSON request body into a Person struct
	var updateData Availability
	// if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
	// 	//http.Error(w, "Failed to decode request body", http.StatusBadRequest)
	// 	responseBody := ErrorResponse{
	// 		Message: "Failed to decode request body",
	// 		Status:  "Error",
	// 	}

	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)
	// 	return
	// }

	updateData.Monday.OpeningTime = incomingData.PartnerAvailability[0].OpeningTime
	updateData.Monday.ClosingTime = incomingData.PartnerAvailability[0].ClosingTime

	updateData.Tuesday.OpeningTime = incomingData.PartnerAvailability[1].OpeningTime
	updateData.Tuesday.ClosingTime = incomingData.PartnerAvailability[1].ClosingTime

	updateData.Wednesday.OpeningTime = incomingData.PartnerAvailability[2].OpeningTime
	updateData.Wednesday.ClosingTime = incomingData.PartnerAvailability[2].ClosingTime

	updateData.Thursday.OpeningTime = incomingData.PartnerAvailability[3].OpeningTime
	updateData.Thursday.ClosingTime = incomingData.PartnerAvailability[3].ClosingTime

	updateData.Friday.OpeningTime = incomingData.PartnerAvailability[4].OpeningTime
	updateData.Friday.ClosingTime = incomingData.PartnerAvailability[4].ClosingTime

	updateData.Saturday.OpeningTime = incomingData.PartnerAvailability[5].OpeningTime
	updateData.Saturday.ClosingTime = incomingData.PartnerAvailability[5].ClosingTime

	updateData.Sunday.OpeningTime = incomingData.PartnerAvailability[6].OpeningTime
	updateData.Sunday.ClosingTime = incomingData.PartnerAvailability[6].ClosingTime

	fmt.Println("partner availability-->", updateData)

	if updateData.Monday.OpeningTime == "" ||
		updateData.Tuesday.OpeningTime == "" ||
		updateData.Wednesday.OpeningTime == "" ||
		updateData.Thursday.OpeningTime == "" ||
		updateData.Friday.OpeningTime == "" ||
		updateData.Saturday.OpeningTime == "" ||
		updateData.Sunday.OpeningTime == "" ||

		updateData.Monday.ClosingTime == "" ||
		updateData.Tuesday.ClosingTime == "" ||
		updateData.Wednesday.ClosingTime == "" ||
		updateData.Thursday.ClosingTime == "" ||
		updateData.Friday.ClosingTime == "" ||
		updateData.Saturday.ClosingTime == "" ||
		updateData.Sunday.ClosingTime == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing one or more required fields:",
			Status:  "Error",
		}

		fmt.Printf("Missing or or more required fields")

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	fmt.Println("update Data:-->", updateData)

	//inc_data.ProviderAvailability = GetAvailabilityResponse(updateData).PartnerAvailability

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}

	// Define the update operation to update multiple fields
	update := bson.M{
		"$set": bson.M{
			//"service_type": updateData.ServiceType,
			//"payment_type": updateData.PaymentOption,
			"isAvailabilitySet":     "YES",
			"partner_availability":  updateData,
			"provider_availability": GetAvailabilityResponse(updateData).PartnerAvailability,
		},
	}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_partner Partner

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	// responseBody := RegistrationResponse{
	// 	Message: "Partner Availability updated successfully",
	// 	Partner: updated_partner,
	// 	Status:  "Success",
	// }

	data := Data{

		// Partner_ID: storedUser.ID.Hex(),
		Partner: updated_partner,
		// Token:      token,
	}

	responseBody := RegistrationResponse{
		Message: "Partner Availability updated successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	invalidatePartnerCache()

	// Respond with a success message
	//response := map[string]string{"message": "Partner Availability updated successfully", "status:": "1"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}
