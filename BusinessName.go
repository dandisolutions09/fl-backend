package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func handleAddBusinessName(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	partnerID := params["id"]
	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", partnerID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Decode the JSON request body into a Person struct
	var updateData Partner
	if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
		// http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		// return

		responseBody := ErrorResponse{
			Message: "Failed to decode request body",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	if updateData.MainBusinessName == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: Business Name",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	} else if updateData.PhoneNumber == "" {
		responseBody := ErrorResponse{
			Message: "Missing required fields: Phone Number",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// nairobiTime := ConvertToNairobiTime()
	// fmt.Println("Nairobi Time:", nairobiTime)
	// inc_data.CreatedAt = nairobiTime

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}
	//update := bson.M{"$set": bson.M{"main_businessName": updateData.MainBusinessName}}

	update := bson.M{
		"$set": bson.M{
			"main_businessName": updateData.MainBusinessName,
			"bio":               updateData.Bio,
			"phone_number":      updateData.PhoneNumber,
		},
	}

	fmt.Println("update:-->", update)

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_partner Partner

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	// responseBody := RegistrationResponse{
	// 	Message: "Partner Business Name updated successfully",
	// 	Partner: updated_partner,
	// 	Status:  "Success",
	// }

	data := Data{

		// Partner_ID: storedUser.ID.Hex(),
		Partner: updated_partner,
		// Token:      token,
	}

	responseBody := RegistrationResponse{
		Message: "Partner Business Name updated successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	// _, err = collection.UpdateOne(context.Background(), filter, update)
	// if err != nil {
	// 	//http.Error(w, "Error updating record", http.StatusInternalServerError)
	// 	responseBody := ErrorResponse{
	// 		Message: "Error updating record",
	// 		Status:  "Error",
	// 	}

	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)
	// 	fmt.Println("errr", err)

	// 	return
	// }

	// Respond with a success message
	//response := map[string]string{"message": "Partner Business Name updated successfully", "status:": "1"}

	invalidatePartnerCache()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}
