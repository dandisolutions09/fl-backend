package main

type NewPartnerAvailability struct {
	//Status              string            `json:"status"`
	PartnerAvailability []AvailabilityDay `json:"partner_availability"`
}

func GetAvailabilityResponse(availability Availability) NewPartnerAvailability {
	availabilityDays := []AvailabilityDay{
		{"Monday", availability.Monday.OpeningTime, availability.Monday.ClosingTime},
		{"Tuesday", availability.Tuesday.OpeningTime, availability.Tuesday.ClosingTime},
		{"Wednesday", availability.Wednesday.OpeningTime, availability.Wednesday.ClosingTime},
		{"Thursday", availability.Thursday.OpeningTime, availability.Thursday.ClosingTime},
		{"Friday", availability.Friday.OpeningTime, availability.Friday.ClosingTime},
		{"Saturday", availability.Saturday.OpeningTime, availability.Saturday.ClosingTime},
		{"Sunday", availability.Sunday.OpeningTime, availability.Sunday.ClosingTime},
	}

	availabilityResponse := NewPartnerAvailability{

		PartnerAvailability: availabilityDays,
	}

	return availabilityResponse
}
