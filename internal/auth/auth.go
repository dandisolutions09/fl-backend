package auth

import (
	"fmt"
	"log"
	"os"

	"github.com/gorilla/sessions"
	"github.com/joho/godotenv"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/google"
)

const (
	key    = "randomString"
	MaxAge = 86400 * 30
	IsProd = false
)

func NewAuth() {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal(("Error loading .env file"))
		fmt.Print(err)
	}

	googleClientId := os.Getenv("GOOGLE_CLIENT_ID")
	googleClientSecret := os.Getenv("GOOGLE_CLIENT_SECRET")

	//googleClientId := "416299411529-tnn656kd00sacg05hpl1ic6hcsfqbarg.apps.googleusercontent.com"
	//googleClientSecret := "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"

	fmt.Println(googleClientId)
	fmt.Println(googleClientSecret)

	store := sessions.NewCookieStore([]byte(key))
	store.MaxAge(MaxAge)

	store.Options.Path = "/"
	store.Options.HttpOnly = true
	store.Options.Secure = IsProd

	gothic.Store = store
	fmt.Println("store", store)
	goth.UseProviders(
		google.New(googleClientId, googleClientSecret, "http://localhost:3000/auth/google/callback"),
	)

}
