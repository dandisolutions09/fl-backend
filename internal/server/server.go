package server

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	//	"strconv"
	"time"

	"FL-backend/internal/database"

	"github.com/joho/godotenv"
	/////"github.com/joho/godotenv/autoload"
)

type Server struct {
	port int
	db   database.Service
}

func NewServer() *http.Server {

	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal(("Error loading .env file"))
		fmt.Print(err)
	}

	//port := os.Getenv("PORT")
	port, _ := strconv.Atoi(os.Getenv("PORT"))
	fmt.Println("port:->", port)
	NewServer := &Server{
		port: port,
		db:   database.New(),
	}

	// Declare Server config
	server := &http.Server{
		Addr:         fmt.Sprintf(":%d", NewServer.port),
		Handler:      NewServer.RegisterRoutes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	return server
}
