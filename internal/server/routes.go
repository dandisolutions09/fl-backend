package server

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/markbates/goth/gothic"
)

func (s *Server) RegisterRoutes() http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/", s.HelloWorldHandler)
	r.HandleFunc("/health", s.healthHandler)
	r.HandleFunc("/auth/{provider}/callback", s.getAuthCallbackFunction)

	return r
}

func (s *Server) HelloWorldHandler(w http.ResponseWriter, r *http.Request) {
	resp := make(map[string]string)
	resp["message"] = "Hello World"

	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("error handling JSON marshal. Err: %v", err)
	}

	_, _ = w.Write(jsonResp)
}

func (s *Server) healthHandler(w http.ResponseWriter, r *http.Request) {
	jsonResp, err := json.Marshal(s.db.Health())

	if err != nil {
		log.Fatalf("error handling JSON marshal. Err: %v", err)
	}

	_, _ = w.Write(jsonResp)
}

func (s *Server) getAuthCallbackFunction(w http.ResponseWriter, r *http.Request) {
	//provider :=chi.URLParam(r, "provider")
	vars := mux.Vars(r)
	provider := vars["provider"]

	fmt.Println("provider", provider)

	r = r.WithContext(context.WithValue(context.Background(), "provider", provider))
	fmt.Println("r", r)

	user, err := gothic.CompleteUserAuth(w, r)
	fmt.Println("error", err)
	if err != nil {
		fmt.Fprintln(w, r)
		return
	}

	fmt.Println(user)

	//when you get a callback, you want to re-direct the user
	http.Redirect(w, r, "http://localhost:5173", http.StatusFound)

}
