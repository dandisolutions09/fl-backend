package database

import (
	"context"
	"fmt"
	"log"
	//"os"
	"time"

	//"github.com/joho/godotenv/autoload"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Service interface {
	Health() map[string]string
}

type service struct {
	db *mongo.Client
}

var (
//host      = os.Getenv("DB_HOST")
//port      = os.Getenv("DB_PORT")
//mongo_uri = os.Getenv("MONGO_URI")

// database = os.Getenv("DB_DATABASE")
)

func New() Service {
	//fmt.Println("mongo uri:->", mongo_uri)

	//client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s", host, port)))
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb+srv://dandisolutions09:GAFmLYi25DojAuM1@tms-cluster.cp6qfmq.mongodb.net/"))
	//clientOptions := options.Client().ApplyURI(mongoURI)

	if err != nil {
		log.Fatal(err)

	}
	fmt.Println("Connected to MongoDB")
	return &service{
		db: client,
	}

}

func (s *service) Health() map[string]string {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	err := s.db.Ping(ctx, nil)
	if err != nil {
		log.Fatalf(fmt.Sprintf("db down: %v", err))
	}

	return map[string]string{
		"message": "It's healthy",
	}
}
