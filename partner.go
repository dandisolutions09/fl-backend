package main

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	//"sort"
	"strconv"

	//"os/exec"
	//	"strconv"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/gorilla/mux"
	"github.com/patrickmn/go-cache"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	// "github.com/dgrijalva/jwt-go"
	//"go.mongodb.org/mongo-driver/mongo/options"
	//"go.mongodb.org/mongo-driver/mongo/options"
	//"golang.org/x/oauth2/jwt"
	//"go.mongodb.org/mongo-driver/mongo"
)

// var client *mongo.Client
// var dbName = "rems_db"
// var partner_collection = "partner_collection"

var (
	SECRET_KEY    = []byte("secret_key")
	mongoClient   *mongo.Client
	blacklistColl *mongo.Collection
	ctx           = context.TODO()
)

type Claims struct {
	UserID string `json:"userId"`
	jwt.RegisteredClaims
}

type TokenBlacklist struct {
	Token  string    `bson:"token"`
	Expiry time.Time `bson:"expiry"`
}

type Client struct {
	ID          string    `json:"_id,omitempty" bson:"_id,omitempty"`
	Status      string    `json:"status" bson:"status"`
	First_name  string    `json:"first_name" bson:"first_name"`
	Last_name   string    `json:"last_name" bson:"last_name"`
	Email       string    `json:"email" bson:"email"`
	Reviews     []Reviews `json:"reviews" bson:"reviews"`
	PhoneNumber string    `json:"phone_number" bson:"phone_number"`
	CreatedAt   string    `json:"created_at" bson:"created_at"`
}

type Coordinates struct {
	Latitude  float32 `json:"latitude" bson:"latitude"`
	Longitude float32 `json:"longitude" bson:"longitude"`
	Address   string  `json:"address" bson:"address"`
}

type Job struct {
	ID              string  `json:"_id,omitempty" bson:"_id,omitempty"`
	Status          string  `json:"status" bson:"status"`
	Client          Client  `json:"client" bson:"client"`
	Job_description string  `json:"job_description" bson:"job_description"`
	Email           string  `json:"email" bson:"email"`
	Review          Reviews `json:"reviews" bson:"reviews"`
	PhoneNumber     string  `json:"phone_number" bson:"phone_number"`
	CreatedAt       string  `json:"created_at" bson:"created_at"`
}
type Service struct {
	Service_name string `json:"service_name" bson:"service_name"`
	Amount       string `json:"amount" bson:"amount"`
	Duration     string `json:"duration" bson:"duration"`
}

type Reviews struct {
	Client Client `json:"client" bson:"client"`
	Review string `json:"review" bson:"review"`
	Star   string `json:"stars" bson:"stars"`
}

type Gallery struct {
	PictureName string `json:"picture_name" bson:"picture_name"`
	Picture     string `json:"picture" bson:"picture"`
}

type Time struct {
	OpeningTime string `json:"opening_time" bson:"opening_time"`
	ClosingTime string `json:"closing_time" bson:"closing_time"`
}

type Availability struct {
	Monday    Time `json:"monday" bson:"monday"`
	Tuesday   Time `json:"tuesday" bson:"tuesday"`
	Wednesday Time `json:"wednesday" bson:"wednesday"`
	Thursday  Time `json:"thursday" bson:"thursday"`
	Friday    Time `json:"friday" bson:"friday"`
	Saturday  Time `json:"saturday" bson:"saturday"`
	Sunday    Time `json:"sunday" bson:"sunday"`
}
type Business struct {
	ID            string      `json:"_id,omitempty" bson:"_id,omitempty"`
	Status        string      `json:"status" bson:"status"`
	Business_name string      `json:"business_name" bson:"business_name"`
	Location      Coordinates `json:"location" bson:"location"`
	Email         string      `json:"email" bson:"email"`
	Services      []Service   `json:"services" bson:"services"`
	Gallery       []Gallery   `json:"gallery" bson:"gallery"`
	PhoneNumber   string      `json:"phone_number" bson:"phone_number"`
	CreatedAt     string      `json:"created_at" bson:"created_at"`
}

type Partner_Service_object struct {
	ID           string `json:"id" bson:"id"`
	Type         string `json:"type" bson:"type"`
	Status       string `json:"status" bson:"status"`
	Description  string `json:"description" bson:"description"`
	Localization string `json:"localization" bson:"localization"`
	Price        string `json:"price" bson:"price"`
}

type PartnerBeautyServices struct {
	//	Service_ID     string           `json:"service_id" bson:"service_id"`
	MensHair       []Partner_Service_object `json:"mens_hair" bson:"mens_hair"`
	WomenHair      []Partner_Service_object `json:"women_hair" bson:"women_hair"`
	FaceServices   []Partner_Service_object `json:"face_services" bson:"face_services"`
	Nails_Services []Partner_Service_object `json:"nail_services" bson:"nail_services"`
	Henna_Services []Partner_Service_object `json:"henna_services" bson:"henna_services"`
}

type LocationPoint struct {
	Type        string    `json:"type" bson:"type"`
	Coordinates []float64 `json:"coordinates" bson:"coordinates"`
}

type Partner struct {
	ID                   primitive.ObjectID     `json:"_id,omitempty" bson:"_id,omitempty"`
	UserID               string                 `json:"user_id" bson:"user_id"`
	PrevID               string                 `json:"prev_id" bson:"prev_id"`
	First_name           string                 `json:"first_name" bson:"first_name"`
	Last_name            string                 `json:"last_name" bson:"last_name"`
	Businesses           []Business             `json:"businesses" bson:"businesses"`
	Country              string                 `json:"country" bson:"country"`
	Timezone             string                 `json:"timezone" bson:"timezone"`
	MainBusinessName     string                 `json:"main_businessName" bson:"main_businessName"`
	PhoneNumber          string                 `json:"phone_number" bson:"phone_number"`
	Bio                  string                 `json:"bio" bson:"bio"`
	Logo                 string                 `json:"logo" bson:"logo"`
	ServiceType          string                 `json:"service_type" bson:"service_type"`
	PaymentOption        string                 `json:"payment_option" bson:"payment_option"`
	PartnerAvailability  Availability           `json:"partner_availability" bson:"partner_availability"`
	Email                string                 `json:"email" bson:"email"`
	SocialMedia          string                 `json:"social_media" bson:"social_media"`
	ProfilePic           string                 `json:"profile_pic" bson:"profile_pic"`
	Password             string                 `json:"password" bson:"password"`
	Salt                 []byte                 `json:"salt" bson:"salt"`
	Role                 string                 `json:"role" bson:"role"`
	Location             Coordinates            `json:"location" bson:"location"`
	LocationPoint        LocationPoint          `json:"location_point" bson:"location_point"`
	Jobs                 []Job                  `json:"jobs" bson:"jobs"`
	Status               string                 `json:"status" bson:"status"`
	CreatedAt            string                 `json:"created_at" bson:"created_at"`
	PartnerServices      []PartnerServiceObject `json:"partner_services" bson:"partner_services"`
	ProviderAvailability []AvailabilityDay      `json:"provider_availability" bson:"provider_availability"`
	Address              string                 `json:"address" bson:"address"`
	IsFeatured           string                 `json:"isFeatured" bson:"isFeatured"`
	IsAvailabilitySet    string                 `json:"isAvailabilitySet" bson:"isAvailabilitySet"`
	Distance             float64                `json:"distance" bson:"distance"`
	SocialProvider       string                 `json:"social_provider" bson:"social_provider"`
	IsPasswordReset      bool                   `json:"isPasswordReset" bson:"isPasswordReset"`
	TotalRatings         string                 `json:"total_ratings" bson:"total_ratings"`
	AverageRatings       string                 `json:"average_ratings" bson:"average_ratings"`
	PartnerReviews       []PartnerReviewObject  `json:"partner_reviews" bson:"partner_reviews"`
}

type SocialLoginStruct struct {
	ID                   primitive.ObjectID     `json:"_id,omitempty" bson:"_id,omitempty"`
	UserID               string                 `json:"user_id" bson:"user_id"`
	PrevID               string                 `json:"prev_id" bson:"prev_id"`
	First_name           string                 `json:"first_name" bson:"first_name"`
	Last_name            string                 `json:"last_name" bson:"last_name"`
	Businesses           []Business             `json:"businesses" bson:"businesses"`
	Country              string                 `json:"country" bson:"country"`
	Timezone             string                 `json:"timezone" bson:"timezone"`
	MainBusinessName     string                 `json:"main_businessName" bson:"main_businessName"`
	PhoneNumber          string                 `json:"phone_number" bson:"phone_number"`
	Bio                  string                 `json:"bio" bson:"bio"`
	Logo                 string                 `json:"logo" bson:"logo"`
	ServiceType          string                 `json:"service_type" bson:"service_type"`
	PaymentOption        string                 `json:"payment_option" bson:"payment_option"`
	PartnerAvailability  Availability           `json:"partner_availability" bson:"partner_availability"`
	Email                string                 `json:"email" bson:"email"`
	SocialMedia          string                 `json:"social_media" bson:"social_media"`
	ProfilePic           string                 `json:"profile_pic" bson:"profile_pic"`
	Password             string                 `json:"password" bson:"password"`
	Salt                 []byte                 `json:"salt" bson:"salt"`
	Role                 string                 `json:"role" bson:"role"`
	Location             Coordinates            `json:"location" bson:"location"`
	LocationPoint        LocationPoint          `json:"location_point" bson:"location_point"`
	Jobs                 []Job                  `json:"jobs" bson:"jobs"`
	Status               string                 `json:"status" bson:"status"`
	CreatedAt            string                 `json:"created_at" bson:"created_at"`
	PartnerServices      []PartnerServiceObject `json:"partner_services" bson:"partner_services"`
	ProviderAvailability []AvailabilityDay      `json:"provider_availability" bson:"provider_availability"`
	Address              string                 `json:"address" bson:"address"`
	IsFeatured           string                 `json:"isFeatured" bson:"isFeatured"`
	IsAvailabilitySet    string                 `json:"isAvailabilitySet" bson:"isAvailabilitySet"`
	Distance             float64                `json:"distance" bson:"distance"`
	SocialProvider       string                 `json:"social_provider" bson:"social_provider"`
}

type Admin struct {
	ID         primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	First_name string             `json:"first_name" bson:"first_name"`
	Last_name  string             `json:"last_name" bson:"last_name"`
	Email      string             `json:"email" bson:"email"`
	Password   string             `json:"password" bson:"password"`
	Salt       []byte             `json:"salt" bson:"salt"`
	Status     string             `json:"status" bson:"status"`
	Type       string             `json:"type" bson:"type"`
	CreatedAt  string             `json:"created_at" bson:"created_at"`
}

type LoginResponse struct {
	Message string `json:"message"`
	Token   string `json:"token"`
	//Partner    Partner `json:"data,omitempty"
	Data       Data   `json:"data"`
	Partner_ID string `json:"partner_id"`
	//PartnerID string  `json:"token,omitempty"`
	Status string `json:"status"`
}

type Data struct {
	// Message    string  `json:"message"`
	Token      string  `json:"token"`
	Partner    Partner `json:"partner"`
	Partner_ID string  `json:"partner_id"`
	//PartnerID string  `json:"token,omitempty"`
	// Status string `json:"status,omitempty"`
}

type DataCustomer struct {
	// Message    string  `json:"message"`
	Token       string   `json:"token"`
	Customer    Customer `json:"customer"`
	Customer_ID string   `json:"customer_id"`
	//PartnerID string  `json:"token,omitempty"`
	// Status string `json:"status,omitempty"`
}

type RegistrationResponse struct {
	Message string `json:"message"`
	// Token      string  `json:"token"`
	// Partner    Partner `json:"partner"`
	// Partner_ID string  `json:"partner_id"`
	Data Data `json:"data"`

	Status string `json:"status"`
}

type RegistrationResponseCustomer struct {
	Message string `json:"message"`
	// Token      string  `json:"token"`
	// Partner    Partner `json:"partner"`
	// Partner_ID string  `json:"partner_id"`
	Data DataCustomer `json:"data"`

	Status string `json:"status"`
}

type ImageUploadResponse struct {
	Message string `json:"message"`
	//Token      string  `json:"token,omitempty"`
	//Partner    Partner `json:"partner,omitempty"`
	//Partner_ID string  `json:"partner_id,omitempty"`
	ImageURL string `json:"image_url"`

	Status string `json:"status"`
}

type ErrorResponse struct {
	Message string `json:"message"`
	Status  string `json:"status"`
}

type AdminRegistrationResponse struct {
	Message string `json:"message"`
	Token   string `json:"token"`
	//Partner   Partner `json:"partner,omitempty"`
	Admin_ID string `json:"admin_id"`
	Status   string `json:"status"`
}

type EmailResponse struct {
	Message string `json:"message"`
	Token   string `json:"token,omitempty"`
	//Partner   Partner `json:"partner,omitempty"`
	Partner_ID string `json:"partner_id,omitempty"`
	Status     string `json:"status,omitempty"`
}

type PartnerServiceObject struct {
	ServiceID       string `json:"service_id" bson:"service_id"`
	Type            string `json:"type" bson:"type"`
	Status          string `json:"status" bson:"status"`
	Description     string `json:"description" bson:"description"`
	Localization    string `json:"localization" bson:"localization"`
	Subcategory     string `json:"subcategory" bson:"subcategory"`
	Number          string `json:"number" bson:"number"`
	Price           string `json:"price" bson:"price"`
	ServiceDuration string `json:"service_duration" bson:"service_duration"`
	CreatedAt       string `json:"created_at" bson:"created_at"`
}

type PartnerReviewObject struct {
	ReviewID   string `json:"review_id" bson:"review_id"`
	CustomerID string `json:"customer_id" bson:"customer_id"`
	Rating     string `json:"rating" bson:"rating"`
	Message    string `json:"message" bson:"message"`
	CreatedAt  string `json:"created_at" bson:"created_at"`
}

type CustomerReviewObject struct {
	ReviewID  string `json:"review_id" bson:"review_id"`
	PartnerID string `json:"customer_id" bson:"customer_id"`
	Rating    string `json:"rating" bson:"rating"`
	Message   string `json:"message" bson:"message"`
	CreatedAt string `json:"created_at" bson:"created_at"`
}

type AvailabilityDay struct {
	Day         string `json:"day"`
	OpeningTime string `json:"opening_time"`
	ClosingTime string `json:"closing_time"`
}
type PartnerAvailabilityResponse struct {
	Status              string            `json:"status"`
	PartnerAvailability []AvailabilityDay `json:"partner_availability"`
}

var secretKey = []byte("secret-key")

var partnersCache *cache.Cache

func RegisterPartner(w http.ResponseWriter, r *http.Request) {

	fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	var inc_data Partner

	err := decoder.Decode(&inc_data)
	if err != nil {
		//http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "Failed to decode request JSON",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Check if all required fields are present
	if inc_data.Email == "" || inc_data.Password == "" || inc_data.First_name == "" || inc_data.Last_name == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: email, password, first_name, or last_name",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	fmt.Println("incoming data", inc_data)

	if isEmailTaken(inc_data.Email) {
		//http.Error(w, "Email-already-taken", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Email-already-taken",
			Status:  "Error",
		}
		//json.NewEncoder(w).Encode(response)

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
		//return
	}

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(inc_data.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	inc_data.Password = hashedPassword
	inc_data.Salt = salt
	inc_data.IsPasswordReset = true

	// fmt.Println("to be saved in db:->", inc_data)
	// fmt.Println("FULLNAME", inc_data.First_name+" "+inc_data.Last_name)
	fmt.Println("FULLNAME:", inc_data.First_name+" "+inc_data.Last_name)

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)
	inc_data.CreatedAt = nairobiTime
	inc_data.PartnerServices = []PartnerServiceObject{}
	inc_data.PartnerReviews = []PartnerReviewObject{}
	inc_data.Status = "INACTIVE"

	//Time.OpeningTime = "07:00AM"

	inc_data.PartnerAvailability.Monday.OpeningTime = "07:00 AM"
	inc_data.PartnerAvailability.Monday.ClosingTime = "06:00 PM"

	inc_data.PartnerAvailability.Tuesday.OpeningTime = "07:00 AM"
	inc_data.PartnerAvailability.Tuesday.ClosingTime = "06:00 PM"

	inc_data.PartnerAvailability.Wednesday.OpeningTime = "07:00 AM"
	inc_data.PartnerAvailability.Wednesday.ClosingTime = "06:00 PM"

	inc_data.PartnerAvailability.Thursday.OpeningTime = "07:00 AM"
	inc_data.PartnerAvailability.Thursday.ClosingTime = "06:00 PM"

	inc_data.PartnerAvailability.Friday.OpeningTime = "07:00 AM"
	inc_data.PartnerAvailability.Friday.ClosingTime = "06:00 PM"

	inc_data.PartnerAvailability.Saturday.OpeningTime = "Closed"
	inc_data.PartnerAvailability.Saturday.ClosingTime = "Closed"

	inc_data.PartnerAvailability.Sunday.OpeningTime = "Closed"
	inc_data.PartnerAvailability.Sunday.ClosingTime = "Closed"

	inc_data.ProviderAvailability = GetAvailabilityResponse(inc_data.PartnerAvailability).PartnerAvailability

	//setting whether featured or not
	inc_data.IsFeatured = "NO"
	inc_data.IsAvailabilitySet = "NO"

	point := LocationPoint{
		Type:        "Point",
		Coordinates: []float64{0, 0},
	}

	inc_data.LocationPoint = point

	collection := client.Database(dbName).Collection(partner_collection)
	_, err = collection.InsertOne(context.Background(), inc_data)
	//fmt.Print(inc_data)
	// inc_data.LocationPoint.Type="Point"
	// inc_data.LocationPoint.Coordinates[0]=0
	// inc_data.LocationPoint.Coordinates[1]=0

	if err != nil {

		fmt.Println("err", err)
		responseBody := ErrorResponse{
			Message: "Error inserting Partner",
			Status:  "Error",
		}
		//json.NewEncoder(w).Encode(response)

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Respond with a success message
	// fmt.Fprintf(w, "Nurse inserted Successfully")

	//response := map[string]string{"message": "User inserted successfully"}
	fmt.Println("User inserted successfully", inc_data.Email)

	url := "http://localhost:8080/get-user-by-email/" + inc_data.Email

	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Error making GET request", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	storedUserData, err := io.ReadAll(response.Body)
	if err != nil {
		//http.Error(w, "Error reading response body", http.StatusInternalServerError)
		//ErrorResponse
		responseBody := ErrorResponse{
			Message: "Error reading response body",
			Status:  "Error",
		}
		//json.NewEncoder(w).Encode(response)

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	var storedUser Partner
	if err := json.Unmarshal(storedUserData, &storedUser); err != nil {
		http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)

		//ErrorResponse
		responseBody := ErrorResponse{
			Message: "Error parsing JSON response",
			Status:  "Error",
		}
		//json.NewEncoder(w).Encode(response)

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	fmt.Println("Stored user:-->", storedUser)

	token, _ := createToken(storedUser.First_name + storedUser.Last_name)

	//*************GET STORED USER***************
	customerID := storedUser.ID.Hex()
	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(customerID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}
	// Retrieve the specific admin from MongoDB
	collection = client.Database(dbName).Collection(partner_collection)
	var retrievedPartner Partner
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&retrievedPartner)
	if err != nil {
		response := map[string]string{"message": "Partner not found", "status": "0"}
		fmt.Println("error", err)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}
	fmt.Println("Retrived customer on login", retrievedPartner)

	data := Data{

		Partner_ID: storedUser.ID.Hex(),
		Partner:    retrievedPartner,
		Token:      token,
	}

	responseBody := RegistrationResponse{
		Message: "Registration Successful",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	// response := LoginResponse{
	// 	Message: "Authentication successful",
	// 	Status:  "Success",
	// 	Data:    data,

	// 	//Status:  storedUser.Status,
	// }
	//****************************************************

	// responseBody := RegistrationResponse{
	// 	Message:    "Registration Successful",
	// 	Partner_ID: storedUser.ID.Hex(),
	// 	Partner:    retrievedPartner,
	// 	Token:      token,
	// 	Status:     "1",
	// }
	//json.NewEncoder(w).Encode(response)

	// logData := Logs_Struct{
	// 	FirstName: storedUser.First_name + "  " + storedUser.Last_name,
	// 	//LastName:  storedUser.Last_name,
	// 	UserRole:  "partner",
	// 	Email:     storedUser.Email,
	// 	Activity:  "Registered New Partner",
	// 	CreatedAt: ConvertToNairobiTime(),
	// }

	// insertLogs(logData)

	// partnersCache.Delete("partners")
	// partnersCache.Delete("active-partners")
	invalidatePartnerCache()

	fmt.Println("active partner caches invalidated")

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

}

// mock customer registration
// func RegisterPartner2(w http.ResponseWriter, r *http.Request) {

// 	//fmt.Println("incoming data:", r.Body)

// 	decoder := json.NewDecoder(r.Body)
// 	var inc_data Partner

// 	err := decoder.Decode(&inc_data)
// 	if err != nil {
// 		//http.Error(w, "Error decoding JSON", http.StatusBadRequest)
// 		fmt.Println("err decoding", err)
// 		responseBody := ErrorResponse{
// 			Message: "Failed to decode request JSON",
// 			Status:  "Error",
// 		}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Check if all required fields are present
// 	if inc_data.Email == "" || inc_data.Password == "" || inc_data.First_name == "" || inc_data.Last_name == "" {
// 		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

// 		responseBody := ErrorResponse{
// 			Message: "Missing required fields: email, password, first_name, or last_name",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return

// 	}

// 	fmt.Println("incoming data", inc_data)

// 	if isEmailTaken(inc_data.Email) {
// 		//http.Error(w, "Email-already-taken", http.StatusBadRequest)

// 		responseBody := ErrorResponse{
// 			Message: "Email-already-taken",
// 			Status:  "Error",
// 		}
// 		//json.NewEncoder(w).Encode(response)

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 		//return
// 	}

// 	salt := make([]byte, 16)
// 	rsp, err := rand.Read(salt)
// 	if err != nil {
// 		fmt.Println("Error generating salt:", err)
// 		return
// 	}

// 	fmt.Println("salting resp:", rsp)

// 	// Hash the password with the generated salt
// 	hashedPassword, salt, err := hashPassword(inc_data.Password, salt)
// 	if err != nil {
// 		fmt.Println("Error hashing password:", err)
// 		return
// 	}

// 	inc_data.Password = hashedPassword
// 	inc_data.Salt = salt

// 	// fmt.Println("to be saved in db:->", inc_data)
// 	// fmt.Println("FULLNAME", inc_data.First_name+" "+inc_data.Last_name)
// 	fmt.Println("FULLNAME:", inc_data.First_name+" "+inc_data.Last_name)

// 	nairobiTime := ConvertToNairobiTime()
// 	fmt.Println("Nairobi Time:", nairobiTime)
// 	inc_data.CreatedAt = nairobiTime
// 	inc_data.PartnerServices = []PartnerServiceObject{}
// 	inc_data.Status = "INACTIVE"

// 	//Time.OpeningTime = "07:00AM"

// 	inc_data.PartnerAvailability.Monday.OpeningTime = "07:00 AM"
// 	inc_data.PartnerAvailability.Monday.ClosingTime = "06:00 PM"

// 	inc_data.PartnerAvailability.Tuesday.OpeningTime = "07:00 AM"
// 	inc_data.PartnerAvailability.Tuesday.ClosingTime = "06:00 PM"

// 	inc_data.PartnerAvailability.Wednesday.OpeningTime = "07:00 AM"
// 	inc_data.PartnerAvailability.Wednesday.ClosingTime = "06:00 PM"

// 	inc_data.PartnerAvailability.Thursday.OpeningTime = "07:00 AM"
// 	inc_data.PartnerAvailability.Thursday.ClosingTime = "06:00 PM"

// 	inc_data.PartnerAvailability.Friday.OpeningTime = "07:00 AM"
// 	inc_data.PartnerAvailability.Friday.ClosingTime = "06:00 PM"

// 	inc_data.PartnerAvailability.Saturday.OpeningTime = "Closed"
// 	inc_data.PartnerAvailability.Saturday.ClosingTime = "Closed"

// 	inc_data.PartnerAvailability.Sunday.OpeningTime = "Closed"
// 	inc_data.PartnerAvailability.Sunday.ClosingTime = "Closed"

// 	inc_data.ProviderAvailability = GetAvailabilityResponse(inc_data.PartnerAvailability).PartnerAvailability

// 	//setting whether featured or not
// 	inc_data.IsFeatured = "NO"
// 	inc_data.IsAvailabilitySet = "NO"

// 	point := LocationPoint{
// 		Type:        "Point",
// 		Coordinates: []float64{0, 0},
// 	}

// 	inc_data.LocationPoint = point

// 	collection := client.Database(dbName).Collection(partner_collection2)
// 	_, err = collection.InsertOne(context.Background(), inc_data)
// 	//fmt.Print(inc_data)
// 	// inc_data.LocationPoint.Type="Point"
// 	// inc_data.LocationPoint.Coordinates[0]=0
// 	// inc_data.LocationPoint.Coordinates[1]=0

// 	if err != nil {

// 		fmt.Println("err", err)
// 		responseBody := ErrorResponse{
// 			Message: "Error inserting Partner",
// 			Status:  "Error",
// 		}
// 		//json.NewEncoder(w).Encode(response)

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Respond with a success message
// 	// fmt.Fprintf(w, "Nurse inserted Successfully")

// 	//response := map[string]string{"message": "User inserted successfully"}
// 	fmt.Println("User inserted successfully", inc_data.Email)

// 	url := "http://localhost:8080/get-user-by-email/" + inc_data.Email

// 	response, err := http.Get(url)
// 	if err != nil {
// 		http.Error(w, "Error making GET request", http.StatusInternalServerError)
// 		return
// 	}
// 	defer response.Body.Close()

// 	storedUserData, err := io.ReadAll(response.Body)
// 	if err != nil {
// 		//http.Error(w, "Error reading response body", http.StatusInternalServerError)
// 		//ErrorResponse
// 		responseBody := ErrorResponse{
// 			Message: "Error reading response body",
// 			Status:  "Error",
// 		}
// 		//json.NewEncoder(w).Encode(response)

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return

// 	}

// 	var storedUser Partner
// 	if err := json.Unmarshal(storedUserData, &storedUser); err != nil {
// 		http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)

// 		//ErrorResponse
// 		responseBody := ErrorResponse{
// 			Message: "Error parsing JSON response",
// 			Status:  "Error",
// 		}
// 		//json.NewEncoder(w).Encode(response)

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	fmt.Println("Stored user:-->", storedUser)

// 	token, _ := createToken(storedUser.First_name + storedUser.Last_name)

// 	//*************GET STORED USER***************
// 	customerID := storedUser.ID.Hex()
// 	// Convert the user ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(customerID)
// 	if err != nil {
// 		http.Error(w, "Invalid User ID", http.StatusBadRequest)
// 		return
// 	}
// 	// Retrieve the specific admin from MongoDB
// 	collection = client.Database(dbName).Collection(partner_collection)
// 	var retrievedPartner Partner
// 	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&retrievedPartner)
// 	if err != nil {
// 		response := map[string]string{"message": "Partner not found", "status": "0"}
// 		fmt.Println("error", err)
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}
// 	fmt.Println("Retrived customer on login", retrievedPartner)
// 	//****************************************************

// 	responseBody := RegistrationResponse{
// 		Message:    "Registration Successful",

// 		// Partner_ID: storedUser.ID.Hex(),
// 		// Partner:    retrievedPartner,
// 		// Token:      token,
// 		Status:     "1",
// 	}
// 	//json.NewEncoder(w).Encode(response)

// 	// logData := Logs_Struct{
// 	// 	FirstName: storedUser.First_name + "  " + storedUser.Last_name,
// 	// 	//LastName:  storedUser.Last_name,
// 	// 	UserRole:  "partner",
// 	// 	Email:     storedUser.Email,
// 	// 	Activity:  "Registered New Partner",
// 	// 	CreatedAt: ConvertToNairobiTime(),
// 	// }

// 	// insertLogs(logData)

// 	// partnersCache.Delete("partners")
// 	// partnersCache.Delete("active-partners")
// 	invalidatePartnerCache()

// 	fmt.Println("active partner caches invalidated")

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)

// }

func handleGetUserByUserId(w http.ResponseWriter, r *http.Request) {

	fmt.Println("get partnet by ID has been called....")
	// Get the driver ID from the request parameters
	params := mux.Vars(r)
	userId := params["userId"]

	fmt.Println("passed user id", userId)

	// Convert the Driver ID to a MongoDB ObjectID

	// Retrieve the specific driver from MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	var usr Partner
	err := collection.FindOne(context.Background(), bson.M{"user_id": userId}).Decode(&usr)
	if err != nil {
		//http.Error(w, "Driver not found", http.StatusNotFound)
		response := map[string]string{"message": "Partner not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved driver in JSON format
	w.Header().Set("Content-Type", "application/json")

	//passwordVerification()

	json.NewEncoder(w).Encode(usr)
}

func handleLogin(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	type DataPartnerNeedsPasswordReset struct {
		Partner Partner `json:"partner"`
	}

	type PartnerNeedsPasswordResetResponse struct {
		Message string `json:"message"`

		Data DataPartnerNeedsPasswordReset `json:"data"`

		Status string `json:"status"`
	}

	var u Partner
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		//http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		responseBody := ErrorResponse{
			Message: "Failed to decode request JSON",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	if u.Email == "" || u.Password == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: email, password",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// Make a GET request to retrieve the stored hash from another endpoint
	//url := "http://localhost:8080/get-username/" + u.Username
	//url := "http://localhost:8080/get-userid/" + u.ID.Hex()
	url := "http://localhost:8080/get-user-by-email/" + u.Email

	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Error making GET request", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	storedHash, err := io.ReadAll(response.Body)
	if err != nil {
		http.Error(w, "Error reading response body", http.StatusInternalServerError)
		return
	}

	var storedUser Partner
	if err := json.Unmarshal(storedHash, &storedUser); err != nil {
		//http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)
		responseBody := ErrorResponse{
			Message: "Error parsing JSON response",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	//*************GET STORED USER***************
	customerID := storedUser.ID.Hex()
	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(customerID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}
	fmt.Println("Object ID", objID)
	// Retrieve the specific partner from MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	var retrievedPartner Partner
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&retrievedPartner)
	if err != nil {
		fmt.Println("error", err)
		response := map[string]string{"message": "Partner not found", "status": "Error"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}
	fmt.Println("Retrived customer on login", retrievedPartner)
	//****************************************************

	//***********CHECK IF PASSWORD HAS BEEN RESET***********************//

	if !retrievedPartner.IsPasswordReset {

		data := DataPartnerNeedsPasswordReset{
			Partner: retrievedPartner,
		}

		response := PartnerNeedsPasswordResetResponse{
			Message: "Partner Needs to Reset Password",
			Data:    data,
			Status:  "Error",
		}

		// response := map[string]string{"message": "Partner Needs to Reset Password", "status": "Error"}
		// w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return

	}

	passwordsMatch := comparePasswords(u.Password, storedUser.Password, storedUser.Salt)
	if passwordsMatch {
		token, _ := createToken(u.First_name + u.Last_name)

		data := Data{

			Partner_ID: storedUser.ID.Hex(),
			Partner:    retrievedPartner,
			Token:      token,
		}

		response := LoginResponse{
			Message: "Authentication successful",
			Status:  "Success",
			Data:    data,

			//Status:  storedUser.Status,
		}
		fmt.Println("first name:->", storedUser)

		logData := Logs_Struct{
			FirstName: storedUser.First_name + "  " + storedUser.Last_name,
			//LastName:  storedUser.Last_name,
			UserRole:  "partner",
			Email:     u.Email,
			Activity:  "Logged in Successfully",
			CreatedAt: ConvertToNairobiTime(),
		}

		insertLogs(logData)
		json.NewEncoder(w).Encode(response)
	} else {
		//http.Error(w, "Authentication failed", http.StatusUnauthorized)
		// response := LoginResponse{
		// 	Message: "Authentication Not Successful",
		// 	Status:  "Error",
		// 	//Token:   "your_generated_token",
		// 	//User:    storedUser,
		// 	//Status:  storedUser.Status,
		// }

		response := LoginResponse{
			Message: "Wrong Login Information",
			Status:  "Error",
			// Data:    data,

			//Status:  storedUser.Status,
		}
		logData := Logs_Struct{
			FirstName: storedUser.First_name + "  " + storedUser.Last_name,
			//LastName:  storedUser.Last_name,
			UserRole:  "partner",
			Email:     u.Email,
			Activity:  "Logged Failed",
			CreatedAt: ConvertToNairobiTime(),
		}

		insertLogs(logData)
		json.NewEncoder(w).Encode(response)
	}
}

// func handleGetUserByEmail(w http.ResponseWriter, r *http.Request) {

// 	fmt.Println("get driver by ID has been called....")
// 	// Get the driver ID from the request parameters
// 	params := mux.Vars(r)
// 	email := params["email"]

// 	// Convert the Driver ID to a MongoDB ObjectID

// 	// Retrieve the specific driver from MongoDB
// 	collection := client.Database(dbName).Collection(partner_collection)
// 	var usr User
// 	err := collection.FindOne(context.Background(), bson.M{"email": email}).Decode(&usr)
// 	if err != nil {
// 		//http.Error(w, "Driver not found", http.StatusNotFound)

// 		response := map[string]string{"message": "Partner not found", "status": "0"}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}

// 	// Respond with the retrieved driver in JSON format
// 	w.Header().Set("Content-Type", "application/json")

// 	//passwordVerification()

// 	json.NewEncoder(w).Encode(usr)
// }

func handleGetUserByEmail(w http.ResponseWriter, r *http.Request) {
	fmt.Println("get driver by ID has been called....")
	// Get the email from the request parameters
	params := mux.Vars(r)
	email := params["email"]

	// Create a case insensitive regex filter for the email
	filter := bson.M{"email": bson.M{"$regex": "^" + email + "$", "$options": "i"}}

	// Retrieve the specific user from MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	var usr User
	err := collection.FindOne(context.Background(), filter).Decode(&usr)
	if err != nil {
		//http.Error(w, "Partner not found", http.StatusNotFound)

		response := map[string]string{"message": "Partner not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved user in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(usr)
}

func handleGetAdminByEmail(w http.ResponseWriter, r *http.Request) {

	fmt.Println("get driver by ID has been called....")
	// Get the driver ID from the request parameters
	params := mux.Vars(r)
	email := params["email"]

	// Convert the Driver ID to a MongoDB ObjectID

	// Retrieve the specific driver from MongoDB
	collection := client.Database(dbName).Collection(admin_collection)
	var usr User
	err := collection.FindOne(context.Background(), bson.M{"email": email}).Decode(&usr)
	if err != nil {
		//http.Error(w, "Driver not found", http.StatusNotFound)
		response := map[string]string{"message": "Partner not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved driver in JSON format
	w.Header().Set("Content-Type", "application/json")

	//passwordVerification()

	json.NewEncoder(w).Encode(usr)
}

func handleGetPartnerById(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get admin by ID has been called....")

	// Get the admin ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	fmt.Println("Passed user ID:", userID)

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific admin from MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	var admin Partner
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&admin)
	if err != nil {
		response := map[string]string{"message": "Partner not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	admin.ProviderAvailability = GetAvailabilityResponse(admin.PartnerAvailability).PartnerAvailability

	fmt.Println()

	// Respond with the retrieved admin in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(admin)
}

func handleUpdateProvider(w http.ResponseWriter, r *http.Request) {
	// Get the user ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Partner
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// if isAdminEmailTaken(updatedData.Email) {
	// 	http.Error(w, "Email-already-taken", http.StatusBadRequest)
	// 	return
	// }

	// Exclude password and salt from the update
	updateData := bson.M{}
	var updateResult *mongo.UpdateResult

	if updatedData.Email != "" {
		updateData["email"] = updatedData.Email
	}
	if updatedData.First_name != "" {
		updateData["first_name"] = updatedData.First_name
	}
	if updatedData.Last_name != "" {
		updateData["last_name"] = updatedData.Last_name
	}

	if updatedData.PhoneNumber != "" {
		updateData["phone_number"] = updatedData.PhoneNumber
	}
	if updatedData.MainBusinessName != "" {
		updateData["main_businessName"] = updatedData.MainBusinessName
	}

	if updatedData.Bio != "" {
		updateData["bio"] = updatedData.Bio
	}

	if updatedData.ProfilePic != "" {
		updateData["profile_pic"] = updatedData.ProfilePic
	}

	if updatedData.Logo != "" {
		updateData["logo"] = updatedData.Logo
	}

	// Add other fields that you want to update...

	// Update the specific user in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updateData}

	updateResult, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating user", http.StatusInternalServerError)
		return
	}

	// Check if any document was modified
	if updateResult.ModifiedCount == 0 {
		// No document was modified
		responseBody := ErrorResponse{
			Message: "User Not Found",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	logData := Logs_Struct{
		FirstName: updatedData.First_name + "  " + updatedData.Last_name,
		//LastName:  storedUser.Last_name,
		UserRole:  "partner",
		Email:     updatedData.Email,
		Activity:  "Updated User Registration Data",
		CreatedAt: ConvertToNairobiTime(),
	}

	insertLogs(logData)

	// partnersCache.Delete("partners")
	// partnersCache.Delete("active-partners")
	invalidatePartnerCache()

	// Respond with a success message
	response := map[string]string{"message": "User updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleDeletePartner(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid facility ID", http.StatusBadRequest)
		return
	}

	// Delete the specific facility in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}

	res, err := collection.DeleteOne(context.Background(), filter)

	//result, err = collection.DeleteOne(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error deleting driver", http.StatusInternalServerError)
		return
	}

	// Check if the document was found and deleted
	if res.DeletedCount == 0 {
		// If no documents were deleted, it might mean the document with the provided ID doesn't exist
		//http.NotFound(w, r)
		response := map[string]string{"message": "Partner not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with a JSON success message
	response := map[string]string{"message": "Facility deleted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// func handleGetActivePartners(w http.ResponseWriter, r *http.Request) {

// 	fmt.Println("Welcome to protected area")

// 	// Check if data exists in the cache
// 	if cachedData, found := partnersCache.Get("active-partners"); found {
// 		// Data found in cache, return it
// 		fmt.Println("Data found in cache!")
// 		w.Header().Set("Content-Type", "application/json")
// 		w.Write(cachedData.([]byte))
// 		return
// 	}

// 	collection := client.Database(dbName).Collection(partner_collection)

// 	fmt.Println("Get active partners called")

// 	// Define a slice to store retrieved records
// 	var partners []Partner

// 	// Define a filter to retrieve only active records
// 	filter := bson.M{}

// 	// Retrieve documents from the collection that match the filter
// 	cursor, err := collection.Find(context.Background(), filter)
// 	if err != nil {
// 		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
// 		return
// 	}
// 	defer cursor.Close(context.Background())

// 	// Iterate through the cursor and decode documents into the records slice
// 	for cursor.Next(context.Background()) {
// 		var record Partner
// 		if err := cursor.Decode(&record); err != nil {
// 			fmt.Print("ERROR", err)
// 			http.Error(w, "Error decoding record", http.StatusInternalServerError)
// 			return
// 		}
// 		// Check if either mainBusinessName or logo is empty
// 		if record.MainBusinessName == "" ||
// 			record.Logo == "" ||
// 			record.PartnerAvailability.Monday.OpeningTime == "" ||
// 			record.Location.Latitude == 0 ||
// 			record.Location.Longitude == 0 ||
// 			len(record.PartnerServices) <= 0 ||
// 			record.ServiceType == "" {
// 			record.Status = "INACTIVE"

// 			fmt.Print("in-active", record)
// 		} else {
// 			record.Status = "ACTIVE"
// 			partners = append(partners, record)
// 			fmt.Print("active", record)

// 		}
// 	}

// 	// Convert partners slice to JSON
// 	jsonData, err := json.Marshal(partners)
// 	if err != nil {
// 		http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
// 		return
// 	}

// 	partnersCache.Set("active-partners", jsonData, cache.DefaultExpiration)

// 	// Respond with the retrieved records in JSON format
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(partners)
// }

func handleGetActivePartners(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Welcome to protected area")

	collection := client.Database(dbName).Collection(partner_collection)

	fmt.Println("Get active partners called")

	// Define a slice to store retrieved records
	var partners []Partner

	// Define a filter to retrieve only active records
	filter := bson.M{"status": "ACTIVE"}

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the records slice
	for cursor.Next(context.Background()) {
		var record Partner
		if err := cursor.Decode(&record); err != nil {
			fmt.Print("ERROR", err)
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}
		partners = append(partners, record)
	}

	// Convert partners slice to JSON
	jsonData, err := json.Marshal(partners)
	if err != nil {
		http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
		return
	}

	// Respond with the retrieved records in JSON format
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func handleGetPartnersCached(w http.ResponseWriter, r *http.Request) {

	//initializePartnersStatus()
	fmt.Println("Welcome to protected area")

	// Check if data exists in the cache
	// if cachedData, found := partnersCache.Get("partners"); found {
	// 	// Data found in cache, return it
	// 	fmt.Println("Data found in cache!")
	// 	w.Header().Set("Content-Type", "application/json")
	// 	w.Write(cachedData.([]byte))
	// 	return
	// }

	collection := client.Database(dbName).Collection(partner_collection)
	fmt.Println("Get all partners called")

	var partners []Partner

	filter := bson.M{}

	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	for cursor.Next(context.Background()) {
		var record Partner
		if err := cursor.Decode(&record); err != nil {
			fmt.Print("ERROR", err)
			fmt.Println("erroneous partner", record)
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}
		partners = append(partners, record)
	}

	err_ := initializePartnersStatus()
	if err_ != nil {
		fmt.Println("Error Initializing partner status:-", err_)
	}

	// Convert partners slice to JSON
	jsonData, err := json.Marshal(partners)
	if err != nil {
		http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
		return
	}

	fmt.Println("number of partners all", len(partners))

	// Store data in cache
	// partnersCache.Set("partners", jsonData, cache.DefaultExpiration)

	// Respond with the retrieved records in JSON format
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func storePartnerService(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData PartnerServiceObject
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Update the specific records in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}
	///update := bson.M{"$set": updatedData}

	// update := bson.M{
	// 	"$set": bson.M{

	// 		"partner_services": updatedData,
	// 		// Include all other fields you want to update, excluding 'created_at'
	// 	},
	// }

	fmt.Println("incoming data-->", updatedData)
	updatedData.CreatedAt = ConvertToNairobiTime()

	updatedData.ServiceID = generateAlphanumeric()

	//push partener services
	update := bson.M{
		"$push": bson.M{
			"partner_services": updatedData,
		},
	}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return

	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_partner Partner

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	data := Data{

		// Partner_ID: storedUser.ID.Hex(),
		Partner: updated_partner,
		// Token:      token,
	}

	responseBody := RegistrationResponse{
		Message: "Partner Service updated successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	invalidatePartnerCache()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

}

// sample
func handleGetPartnerAvailability(w http.ResponseWriter, r *http.Request) {

	fmt.Println("get partner by ID has been called....")

	params := mux.Vars(r)
	userID := params["userId"]

	fmt.Println("passed user id", userID)

	collection := client.Database(dbName).Collection(partner_collection)
	var usr Partner
	err := collection.FindOne(context.Background(), bson.M{"user_id": userID}).Decode(&usr)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "User not found",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	availabilityDays := []AvailabilityDay{
		{"Monday", usr.PartnerAvailability.Monday.OpeningTime, usr.PartnerAvailability.Monday.ClosingTime},
		{"Tuesday", usr.PartnerAvailability.Tuesday.OpeningTime, usr.PartnerAvailability.Tuesday.ClosingTime},
		{"Wednesday", usr.PartnerAvailability.Wednesday.OpeningTime, usr.PartnerAvailability.Wednesday.ClosingTime},
		{"Thursday", usr.PartnerAvailability.Thursday.OpeningTime, usr.PartnerAvailability.Thursday.ClosingTime},
		{"Friday", usr.PartnerAvailability.Friday.OpeningTime, usr.PartnerAvailability.Friday.ClosingTime},
		{"Saturday", usr.PartnerAvailability.Saturday.OpeningTime, usr.PartnerAvailability.Saturday.ClosingTime},
		{"Sunday", usr.PartnerAvailability.Sunday.OpeningTime, usr.PartnerAvailability.Sunday.ClosingTime},
	}

	availabilityResponse := PartnerAvailabilityResponse{
		Status:              "1",
		PartnerAvailability: availabilityDays,
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(availabilityResponse)
}

//DEACTIVATE PARTNER

// Function to update document status to INACTIVE
func DeactivatePartner(w http.ResponseWriter, r *http.Request) {
	// Get ID from URL params
	//vars := mux.Vars(r)
	//partnerID := vars["id"]

	partnerID := r.URL.Query().Get("id")

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Define filter to find document by ID

	// Define update to set status to INACTIVE
	update := bson.M{"$set": bson.M{"status": "INACTIVE"}}

	//fmt.Println("update Data:-->", updateData)

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_partner Partner

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	// responseBody := RegistrationResponse{
	// 	Message: "Partner Deactivated successfully",
	// 	Partner: updated_partner,
	// 	Status:  "Success",
	// }

	data := Data{

		// Partner_ID: storedUser.ID.Hex(),
		Partner: updated_partner,
		// Token:      token,
	}

	responseBody := RegistrationResponse{
		Message: "Partner Deactivated successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}
	invalidatePartnerCache()

	// Encode the updated document as JSON and send in response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

//

func ActivatePartner(w http.ResponseWriter, r *http.Request) {
	// Get ID from URL params
	//vars := mux.Vars(r)
	partnerID := r.URL.Query().Get("id")

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Define filter to find document by ID

	// Define update to set status to INACTIVE
	update := bson.M{"$set": bson.M{"status": "ACTIVE"}}

	//fmt.Println("update Data:-->", updateData)

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_partner Partner

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	data := Data{

		// Partner_ID: storedUser.ID.Hex(),
		Partner: updated_partner,
		// Token:      token,
	}

	responseBody := RegistrationResponse{
		Message: "Partner Activated successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}
	invalidatePartnerCache()

	// Encode the updated document as JSON and send in response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

//SORT PARTNERS BASED ON THEIR STATUS

// Handler to retrieve services filtered by subcategory
func getPartnersByStatus(w http.ResponseWriter, r *http.Request) {
	status := r.URL.Query().Get("status")

	fmt.Println("Status", status)

	// Check if subcategory is "all"
	if status == "ALL" {
		// Fetch all services from MongoDB
		var partners []Partner
		collection := client.Database(dbName).Collection(partner_collection)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var partner Partner
			err := cursor.Decode(&partner)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			partners = append(partners, partner)
		}

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(partners)
		return
	}

	// Fetch partners from MongoDB based on subcategory
	var partners []Partner
	collection := client.Database(dbName).Collection(partner_collection)
	cursor, err := collection.Find(context.Background(), bson.M{"status": status})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var partner Partner
		err := cursor.Decode(&partner)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		partners = append(partners, partner)
	}

	// Respond with the retrieved services
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(partners)
}

// GET FEATURED PARTNERS
func handleGetFeaturedPartners(w http.ResponseWriter, r *http.Request) {
	// Define the response structure
	type FeaturedPartnersResponse struct {
		FeaturedPartners []Partner `json:"featured_partners,omitempty"`
		Status           string    `json:"status,omitempty"`
	}

	type FeaturedPartnersResponseEmpty struct {
		FeaturedPartners string `json:"featured_partners,omitempty"`
		Status           string `json:"status,omitempty"`
	}

	fmt.Println("Get admin by ID has been called....")

	// Extract the user ID from the query parameters
	userID := r.URL.Query().Get("id")

	fmt.Println("Passed user ID:", userID)

	// Access the bookings collection
	collection := client.Database(dbName).Collection(partner_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	// filter := bson.M{
	// 	//"provider_id": userID,
	// 	"isFeatured": bson.M{
	// 		"$in": []string{"NO"},
	// 	},
	// }

	filter := bson.M{"isFeatured": "YES"}

	// Define options to sort by timestamp
	//options := options.Find().SetSort(bson.D{{"timestamp", 1}})

	// Find documents
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())

	// Slice to store the found bookings
	var partners []Partner

	// Iterate over the cursor and decode each document into a BookingObject
	for cursor.Next(context.Background()) {
		var result Partner
		if err := cursor.Decode(&result); err != nil {
			log.Fatal(err)
		}
		partners = append(partners, result)
	}
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	// Prepare response

	// If there are no bookings, set the bookings array to an empty array
	if len(partners) == 0 {
		// responseBody := FeaturedPartnersResponseEmpty{
		// 	FeaturedPartners: "empty",
		// 	Status:           "1",
		// }

		// responseBody := map[string]interface{}{
		// 	"message": "Retrieved Featured Parnter Successfuly",
		// 	"status":  "Success",
		// 	"data":    []Partner{}, // Include the whole object in the response
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Featured Partner Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"featured_partners": []Partner{}, // Include the whole object in the response
			},
		}

		//fmt.Println("response body", responseBody)

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

		//new changes

	} else {

		// responseBody := FeaturedPartnersResponse{
		// 	FeaturedPartners: partners,
		// 	Status:           "1",
		// }

		// responseBody := map[string]interface{}{
		// 	"message": "Retrieved Featured Parnter Successfuly",
		// 	"status":  "Success",
		// 	"data":    partners, // Include the whole object in the response
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Featured Partner Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"featured_partners": partners, // Include the whole object in the response
			},
		}

		//fmt.Println("response body", responseBody)

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

}

// //FEATURE PARTNER
func handleFeaturePartner(w http.ResponseWriter, r *http.Request) {
	// Get ID from URL params
	//vars := mux.Vars(r)

	type FeaturedPartnerResponse struct {
		Message string `json:"message"`
		//Token   string  `json:"token,omitempty"`
		Partner Partner `json:"partner,omitempty"`
		Status  string  `json:"status,omitempty"`
	}

	partnerID := r.URL.Query().Get("id")

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Define filter to find document by ID

	// Define update to set status to YES
	update := bson.M{"$set": bson.M{"isFeatured": "YES"}}

	//fmt.Println("update Data:-->", updateData)

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_partner Partner

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	responseBody := FeaturedPartnerResponse{
		Message: "Partner Featured Successfully",
		Partner: updated_partner,
		Status:  "Success",
	}
	invalidatePartnerCache()

	// Encode the updated document as JSON and send in response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("log out handler")
	authHeader := r.Header.Get("Authorization")
	tokenString := strings.TrimPrefix(authHeader, "Bearer ")

	fmt.Println("auth header", authHeader)
	fmt.Println("tokenString", tokenString)

	if tokenString == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
	// 	return token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
	// 		return secretKey, nil
	// 	}), nil
	// })

	// Decode the token to get its expiry time
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return secretKey, nil
	})
	if err != nil || !token.Valid {
		fmt.Println("error", err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	fmt.Println("claims", claims.ExpiresAt.Time)

	result := TokenBlacklist{
		Token: tokenString,

		Expiry: claims.ExpiresAt.Time,
	}

	// blacklistColl.InsertOne(ctx, TokenBlacklist{
	//     Token:  tokenString,
	//     Expiry: claims.ExpiresAt.Time,
	// })

	collection := client.Database(dbName).Collection(blacklist_collection)
	_, err = collection.InsertOne(context.Background(), result)
	fmt.Print(result)
	if err != nil {
		http.Error(w, "Error inserting Customer", http.StatusInternalServerError)
		return
	}

	response := LogoutResponse{
		Message: "User logged out successfully",
		//Token:   "your_generated_token",
		//User:    storedUser,
		Status: "Success",
	}

	json.NewEncoder(w).Encode(response)
}

type LocationRequest struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

//

func getNearestPartners(w http.ResponseWriter, r *http.Request) {

	type DataNearestPartnerResponse struct {
		Partner []Partner `json:"partner"`
	}

	type NearestPartnerResponse struct {
		Message string `json:"message"`

		Data DataNearestPartnerResponse `json:"data"`

		Status string `json:"status"`
	}

	// type NearestPartnersResponse struct {
	// 	NearestPartners []Partner `json:"nearest_partners"`
	// 	Status          string    `json:"status"`
	// }

	latStr := r.URL.Query().Get("lat")
	longStr := r.URL.Query().Get("long")

	subcategory := r.URL.Query().Get("category")
	if latStr == "" || longStr == "" {
		http.Error(w, "Latitude and longitude are required parameters", http.StatusBadRequest)
		return
	}

	// Convert latitude and longitude to float64
	lat, err := strconv.ParseFloat(latStr, 64)
	if err != nil {
		http.Error(w, "Invalid latitude", http.StatusBadRequest)
		return
	}
	long, err := strconv.ParseFloat(longStr, 64)
	if err != nil {
		http.Error(w, "Invalid longitude", http.StatusBadRequest)
		return
	}

	points, err := GetPointsByDistance(NewPoint(long, lat), 5000)

	for _, partner := range points {
		fmt.Printf("PartnerFirst Name: %s, %s, Distance: %.2f km\n", partner.First_name, partner.Last_name, partner.Distance)
		fmt.Println(partner.PartnerServices)
	}

	if err != nil {
		fmt.Println(err)
		return
	}

	//fmt.Println(points)

	filteredPartners := filterPartnersBySubcategory(points, subcategory)

	fmt.Println("filtered", filteredPartners)

	if len(filteredPartners) == 0 {

		// resp := NearestPartnersResponse{
		// 	NearestPartners: []Partner{},
		// 	Status:          "1",
		// }

		data := DataNearestPartnerResponse{

			// Partner_ID: storedUser.ID.Hex(),
			Partner: []Partner{},
			// Token:      token,
		}

		responseBody := NearestPartnerResponse{
			Message: "Nearest Partner Retrieved successfully",
			// Partner_ID: storedUser.ID.Hex(),
			// Partner:    retrievedPartner,
			// Token:      token,
			Data:   data,
			Status: "Success",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	} else if len(filteredPartners) != 0 {

		data := DataNearestPartnerResponse{

			// Partner_ID: storedUser.ID.Hex(),
			Partner: filteredPartners,
			// Token:      token,
		}

		responseBody := NearestPartnerResponse{
			Message: "Nearest Partner Retrieved successfully",
			// Partner_ID: storedUser.ID.Hex(),
			// Partner:    retrievedPartner,
			// Token:      token,
			Data:   data,
			Status: "Success",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

	// } else if len(filteredPartners) != 0 {

	// 	resp := NearestPartnersResponse{
	// 		NearestPartners: filteredPartners,
	// 		Status:          "1",
	// 	}

	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(resp)
	// }

}

type Location struct {
	Type        string    `json:"type" bson:"type"`
	Coordinates []float64 `json:"coordinates" bson:"coordinates"`
}
type Point struct {
	ID       primitive.ObjectID `json:"id" bson:"_id"`
	Title    string             `json:"title"`
	Location Location           `json:"location"`
	Distance float64            `json:"distance"`
}

var results []Partner

func GetPointsByDistance(location Location, distance int) ([]Partner, error) {
	coll := client.Database(dbName).Collection(partner_collection)

	ctx := context.TODO()

	// Define the geoNear stage with distance calculation
	geoNearStage := bson.D{{
		"$geoNear", bson.D{
			{"near", location},
			{"distanceField", "distance"},
			{"maxDistance", distance},
			{"spherical", true},
		},
	}}

	// Create the pipeline with the geoNear stage
	pipeline := mongo.Pipeline{geoNearStage}

	// Run the aggregation
	cur, err := coll.Aggregate(ctx, pipeline)
	if err != nil {
		return []Partner{}, err
	}
	defer cur.Close(ctx)

	var results []Partner

	for cur.Next(ctx) {
		var p Partner
		err := cur.Decode(&p)
		if err != nil {
			fmt.Println("Could not decode Point")
			return []Partner{}, err
		}

		p.Distance = p.Distance / 1000 // Convert distance to kilometers
		results = append(results, p)

		//print(results)
	}

	// for _, partner := range results {
	// 	fmt.Printf("Partner: %s, Distance: %.2f km\n", partner.First_name, partner.Distance)
	// }

	if err := cur.Err(); err != nil {
		return []Partner{}, err
	}

	return results, nil
}

// NEW POINT
func NewPoint(long, lat float64) Location {
	return Location{
		"Point",
		[]float64{long, lat},
	}
}

// Function to filter partners by service subcategory
func filterPartnersBySubcategory(partners []Partner, subcategory string) []Partner {
	var filteredPartners []Partner

	for _, partner := range partners {
		for _, service := range partner.PartnerServices {
			if service.Subcategory == subcategory {
				filteredPartners = append(filteredPartners, partner)
				break
			}
		}
	}

	return filteredPartners
}

func handleSocialLogin(w http.ResponseWriter, r *http.Request) {

	type RegistrationResponse struct {
		Message string `json:"message"`
		Data    Data   `json:"data"`
		Status  string `json:"status"`
	}

	// Decode the incoming partner data
	var partnerSocialObject Partner
	err := json.NewDecoder(r.Body).Decode(&partnerSocialObject)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Check if the partner already exists based on email
	existingPartner, err := getPartnerByEmail(partnerSocialObject.Email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// If partner exists, return an appropriate message
	if existingPartner != nil {
		// responseBody := RegistrationResponse{
		// 	Message: "Partner already exists",
		// 	Status:  "Error",
		// }

		token, _ := createToken(existingPartner.First_name + existingPartner.Last_name)

		data := Data{
			Partner:    *existingPartner,
			Token:      token,
			Partner_ID: existingPartner.ID.Hex(),
		}

		responseBody := RegistrationResponse{
			Message: "Partner already exists",
			Data:    data,
			Status:  "Success",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// If partner doesn't exist, proceed with storing
	// partnerSocialObject.Status = "INACTIVE"

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)
	partnerSocialObject.CreatedAt = nairobiTime
	partnerSocialObject.PartnerServices = []PartnerServiceObject{}
	//partnerSocialObject.MainBusinessName=
	partnerSocialObject.Status = "INACTIVE"

	//Time.OpeningTime = "07:00AM"

	partnerSocialObject.PartnerAvailability.Monday.OpeningTime = "07:00 AM"
	partnerSocialObject.PartnerAvailability.Monday.ClosingTime = "06:00 PM"

	partnerSocialObject.PartnerAvailability.Tuesday.OpeningTime = "07:00 AM"
	partnerSocialObject.PartnerAvailability.Tuesday.ClosingTime = "06:00 PM"

	partnerSocialObject.PartnerAvailability.Wednesday.OpeningTime = "07:00 AM"
	partnerSocialObject.PartnerAvailability.Wednesday.ClosingTime = "06:00 PM"

	partnerSocialObject.PartnerAvailability.Thursday.OpeningTime = "07:00 AM"
	partnerSocialObject.PartnerAvailability.Thursday.ClosingTime = "06:00 PM"

	partnerSocialObject.PartnerAvailability.Friday.OpeningTime = "07:00 AM"
	partnerSocialObject.PartnerAvailability.Friday.ClosingTime = "06:00 PM"

	partnerSocialObject.PartnerAvailability.Saturday.OpeningTime = "Closed"
	partnerSocialObject.PartnerAvailability.Saturday.ClosingTime = "Closed"

	partnerSocialObject.PartnerAvailability.Sunday.OpeningTime = "Closed"
	partnerSocialObject.PartnerAvailability.Sunday.ClosingTime = "Closed"

	partnerSocialObject.ProviderAvailability = GetAvailabilityResponse(partnerSocialObject.PartnerAvailability).PartnerAvailability

	//setting whether featured or not
	partnerSocialObject.IsFeatured = "NO"
	partnerSocialObject.IsAvailabilitySet = "NO"

	point := LocationPoint{
		Type:        "Point",
		Coordinates: []float64{0, 0},
	}

	partnerSocialObject.LocationPoint = point

	collection := client.Database(dbName).Collection(partner_collection)
	partnerSocialObject.CreatedAt = ConvertToNairobiTime()
	result, err := collection.InsertOne(context.Background(), partnerSocialObject)

	if err != nil {
		fmt.Println("err", err)
		responseBody := RegistrationResponse{
			Message: "Error inserting partner",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Retrieve the inserted partner record
	var insertedPartner Partner
	filter := bson.M{"_id": result.InsertedID}
	err = collection.FindOne(context.Background(), filter).Decode(&insertedPartner)
	if err != nil {
		fmt.Println("err", err)
		responseBody := RegistrationResponse{
			Message: "Error retrieving inserted partner",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	token, _ := createToken(insertedPartner.First_name + insertedPartner.Last_name)

	// Prepare response
	data := Data{
		Partner:    insertedPartner,
		Token:      token,
		Partner_ID: insertedPartner.ID.Hex(),
	}

	responseBody := RegistrationResponse{
		Message: "Social Partner Added Successfully",
		Data:    data,
		Status:  "Success",
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// Function to get partner by email
func getPartnerByEmail(email string) (*Partner, error) {
	var partner Partner
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"email": email}
	err := collection.FindOne(context.Background(), filter).Decode(&partner)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			// Partner doesn't exist
			return nil, nil
		}
		// Other error occurred
		return nil, err
	}
	// Partner exists
	return &partner, nil
}

func handleUpdatePartnerPassword(w http.ResponseWriter, r *http.Request) {
	// Get the user ID from the request parameters
	params := mux.Vars(r)
	userID := params["id"]

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var incomingPassword Partner
	err = decoder.Decode(&incomingPassword)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("INCOMING DATA:->", incomingPassword, objID)

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(incomingPassword.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	fmt.Println("GENERATED HASH", hashedPassword)
	fmt.Println("GENERATED SALT", salt)

	updateData := bson.M{}
	var updateResult *mongo.UpdateResult

	if incomingPassword.Password != "" {
		updateData["password"] = hashedPassword
		fmt.Println("NEW HASH", updateData["password"])
		updateData["salt"] = salt
		fmt.Println("NEW SALT", updateData["salt"])
		updateData["isPasswordReset"] = true // Add this line to update isPasswordReset

		// Update the specific user in MongoDB
		collection := client.Database(dbName).Collection(partner_collection)
		filter := bson.M{"_id": objID}
		update := bson.M{"$set": updateData}

		updateResult, err = collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, "Error updating user", http.StatusInternalServerError)
			return
		}

		// Check if any document was modified
		if updateResult.ModifiedCount == 0 {
			http.Error(w, "Nothing to be updated", http.StatusBadRequest)
			return
		}

		// Respond with a success message
		responseBody := ErrorResponse{
			Message: "Partner Password Reset successfully",
			Status:  "Success",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
	} else {
		fmt.Println("password not provided")
		responseBody := ErrorResponse{
			Message: "Password not provided",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
}
