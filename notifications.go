package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

// User represents a user in the database
// type partner struct {
//     ID   string `bson:"_id"`
//     Name string `bson:"name"`
// }

// ActiveConnections holds active SSE connections
var ActiveConnections = make(map[string]chan string)

// MongoDB client
//var client *mongo.Client

// connectToMongoDB initializes the MongoDB connection
// func connectToMongoDB() {
// 	var err error
// 	client, err = mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	// Check the connection
// 	err = client.Ping(context.TODO(), nil)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Connected to MongoDB!")
// }

// fetchUser fetches a user from MongoDB by ID
func fetchUser(userID string) (*User, error) {
	collection := client.Database(dbName).Collection(partner_collection)
	var user User
	err := collection.FindOne(context.TODO(), bson.M{"_id": userID}).Decode(&user)
	// collection := client.Database(dbName).Collection(booking_collection)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

// SSEHandler handles the Server-Sent Events connection for a specific user
func SSEHandler(w http.ResponseWriter, r *http.Request) {
	// Get user ID from query parameters
	userID := r.URL.Query().Get("user_id")
	if userID == "" {
		http.Error(w, "user_id is required", http.StatusBadRequest)
		return
	}

	// Fetch user from MongoDB
	user, err := fetchUser(userID)
	if err != nil {
		http.Error(w, "User not found", http.StatusNotFound)
		return
	}

	// Set headers to indicate this is an SSE connection
	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")

	// Create a channel for this user's events
	eventChan := make(chan string)
	ActiveConnections[userID] = eventChan
	defer func() {
		close(eventChan)
		delete(ActiveConnections, userID)
	}()

	// Goroutine to send events to the client
	go func() {
		for {
			// Send a message every 2 seconds
			time.Sleep(2 * time.Second)
			eventChan <- fmt.Sprintf("data: Hello %s, the time is %s\n\n", user.First_name, time.Now().String())
		}
	}()

	// Listen for events and write them to the ResponseWriter
	for {
		select {
		case event := <-eventChan:
			if _, err := fmt.Fprintf(w, event); err != nil {
				log.Println("Error writing to ResponseWriter:", err)
				return
			}
			// Flush the response buffer
			if f, ok := w.(http.Flusher); ok {
				f.Flush()
			}
		case <-r.Context().Done():
			// Client closed the connection
			log.Println("Client closed connection")
			return
		}
	}
}

// func main() {
//     // Connect to MongoDB
//     connectToMongoDB()

//     // Set up the SSE endpoint
//     http.HandleFunc("/events", SSEHandler)

//     // Start the server
//     log.Println("Starting server on :8080")
//     if err := http.ListenAndServe(":8080", nil); err != nil {
//         log.Fatal("ListenAndServe:", err)
//     }
// }
