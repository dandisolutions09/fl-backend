package main

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetPartnerByID(client *mongo.Client, dbName, partnerCollection string, objID primitive.ObjectID) (Partner, error) {
	collection := client.Database(dbName).Collection(partnerCollection)
	var retrievedPartner Partner
	err := collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&retrievedPartner)
	if err != nil {
		return Partner{}, fmt.Errorf("partner not found: %w", err)
	}
	return retrievedPartner, nil

}

func RetrievePartnerByID(userID string, client *mongo.Client, dbName, partnerCollection string) (Partner, error) {
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return Partner{}, fmt.Errorf("invalid record ID: %w", err)
	}

	partner, err := GetPartnerByID(client, dbName, partnerCollection, objID)
	if err != nil {
		return Partner{}, err
	}

	//fmt.Println("Retrieved Partner", partner)
	return partner, nil
}

func GetCustomerByID(client *mongo.Client, dbName, customerCollection string, objID primitive.ObjectID) (Customer, error) {
	collection := client.Database(dbName).Collection(customerCollection)
	var retrievedCustomer Customer
	err := collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&retrievedCustomer)
	if err != nil {
		return Customer{}, fmt.Errorf("partner not found: %w", err)
	}
	return retrievedCustomer, nil

}

func RetrieveCustomerByID(userID string, client *mongo.Client, dbName, customerCollection string) (Customer, error) {
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return Customer{}, fmt.Errorf("invalid record ID: %w", err)
	}

	customer, err := GetCustomerByID(client, dbName, customerCollection, objID)
	if err != nil {
		return Customer{}, err
	}
	return customer, nil
}
