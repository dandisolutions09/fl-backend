package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	//"strings"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var updateResult *mongo.UpdateResult

type ServiceObjectt struct {
	Type         string `json:"type" bson:"type"`
	Status       string `json:"status" bson:"status"`
	Description  string `json:"description" bson:"description"`
	Localization string `json:"localization" bson:"localization"`
	Subcategory  string `json:"subcategory" bson:"subcategory"`
	CreatedAt    string `json:"created_at" bson:"created_at"`
}

type CartObject struct {
	ItemID                string      `json:"item_id,omitempty" bson:"item_id,omitempty"`
	ProviderName          string      `json:"provider_name" bson:"provider_name"`
	ProviderID            string      `json:"provider_id" bson:"provider_id"`
	ProviderPhoneNumber   string      `json:"provider_phone_number" bson:"provider_phone_number"`
	ProviderEmail         string      `json:"provider_email" bson:"provider_email"`
	ProviderLocation      Coordinates `json:"provider_location" bson:"provider_location"`
	ProviderPaymentOption string      `json:"provider_payment_option" bson:"provider_payment_option"`

	CustomerID          string      `json:"customer_id" bson:"customer_id"`
	CustomerName        string      `json:"customer_name" bson:"customer_name"`
	CustomerPhoneNumber string      `json:"customer_phone_number" bson:"customer_phone_number"`
	CustomerEmail       string      `json:"customer_email" bson:"customer_email"`
	CustomerLocation    Coordinates `json:"customer_location" bson:"customer_location"`

	Services  []PartnerServiceObject `json:"services" bson:"services"`
	CreatedAt string                 `json:"created_at" bson:"created_at"`

	// ServiceDate string `json:"service_date" bson:"service_date"`
	// ServiceTime string `json:"service_time" bson:"service_time"`
	// ServiceType string `json:"service_type" bson:"service_type"`
	// ServiceCost string `json:"service_cost" bson:"service_cost"`
	// CreatedAt   string `json:"created_at" bson:"created_at"`
}

func generateItemID() string {
	// You can generate ItemID using any logic you prefer, for example:
	uuid := uuid.New()
	return uuid.String()
}

func storeItemToCart(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData CartObject
	err = decoder.Decode(&updatedData)
	if err != nil {
		fmt.Println("ERROR", err)
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("updated Data", updatedData)

	// Check for empty fields
	var emptyFields []string
	if updatedData.CustomerEmail == "" {
		emptyFields = append(emptyFields, "CustomerEmail")
	} else if updatedData.CustomerID == "" {
		emptyFields = append(emptyFields, "CustomerID")
	} else if updatedData.CustomerLocation.Latitude == 0 {
		emptyFields = append(emptyFields, "CustomerLocation's Latitude")
	} else if updatedData.CustomerLocation.Longitude == 0 {
		emptyFields = append(emptyFields, "CustomerLocation's Longitude")
	} else if updatedData.CustomerName == "" {
		emptyFields = append(emptyFields, "CustomerName")
	} else if updatedData.CustomerPhoneNumber == "" {
		emptyFields = append(emptyFields, "CustomerPhoneNumber")

	} else if updatedData.ProviderName == "" {
		emptyFields = append(emptyFields, "ProviderName")
	} else if updatedData.ProviderID == "" {
		emptyFields = append(emptyFields, "ProviderID")
	} else if updatedData.ProviderPhoneNumber == "" {
		emptyFields = append(emptyFields, "ProviderPhoneNumber")
	} else if updatedData.ProviderLocation.Latitude == 0 {
		emptyFields = append(emptyFields, "ProviderLocation's Latitude")
	} else if updatedData.ProviderLocation.Longitude == 0 {
		emptyFields = append(emptyFields, "ProviderLocation's Longitude")
	} else if updatedData.ProviderPaymentOption == "" {
		emptyFields = append(emptyFields, "ProviderPaymentOption")
	} else if updatedData.ProviderEmail == "" {
		emptyFields = append(emptyFields, "ProviderPaymentOption")

	}

	if len(emptyFields) > 0 {
		// Prepare response for empty fields
		responseBody := ErrorResponse{
			Message: fmt.Sprintf("Missing required fields: %s", strings.Join(emptyFields, ", ")),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Update the specific records in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}

	itemID := generateItemID()

	// Assign ItemID to updatedData
	updatedData.ItemID = itemID
	updatedData.CreatedAt = ConvertToNairobiTime()
	//updatedData.Services=[]
	updatedData.Services = []PartnerServiceObject{}

	fmt.Println("updated services", updatedData.Services)

	// Push updated data to the cart array
	update := bson.M{
		"$push": bson.M{
			"cart": updatedData,
		},
	}

	// Perform update operation
	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating Customer", http.StatusInternalServerError)
		return
	}

	// Retrieve the updated customer document
	var updatedCustomer Customer
	err = collection.FindOne(context.Background(), filter).Decode(&updatedCustomer)
	if err != nil {
		http.Error(w, "Error retrieving updated Customer", http.StatusInternalServerError)
		return
	}

	//latestService := updatedCustomer.Cart[len(updatedCustomer.Cart)-1].Services[len(updatedCustomer.Cart[len(updatedCustomer.Cart)-1].Services)-1]
	latestCartItem := updatedCustomer.Cart[len(updatedCustomer.Cart)-1]

	// Respond with the updated customer model
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(latestCartItem)
}

func storeCustomerDetailsToCart(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData CartObject
	err = decoder.Decode(&updatedData)
	if err != nil {
		fmt.Println("ERROR", err)
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("updated Data", updatedData)

	// Check for empty fields
	var emptyFields []string
	if updatedData.CustomerEmail == "" {
		emptyFields = append(emptyFields, "CustomerEmail")
	} else if updatedData.CustomerID == "" {
		emptyFields = append(emptyFields, "CustomerID")
	} else if updatedData.CustomerLocation.Latitude == 0 {
		emptyFields = append(emptyFields, "CustomerLocation's Latitude")
	} else if updatedData.CustomerLocation.Longitude == 0 {
		emptyFields = append(emptyFields, "CustomerLocation's Longitude")
	} else if updatedData.CustomerName == "" {
		emptyFields = append(emptyFields, "CustomerName")
	} else if updatedData.CustomerPhoneNumber == "" {
		emptyFields = append(emptyFields, "CustomerPhoneNumber")

		// } else if updatedData.ProviderName == "" {
		// 	emptyFields = append(emptyFields, "ProviderName")
		// } else if updatedData.ProviderID == "" {
		// 	emptyFields = append(emptyFields, "ProviderID")
		// } else if updatedData.ProviderPhoneNumber == "" {
		// 	emptyFields = append(emptyFields, "ProviderPhoneNumber")
		// } else if updatedData.ProviderLocation.Latitude == "" {
		// 	emptyFields = append(emptyFields, "ProviderLocation's Latitude")
		// } else if updatedData.ProviderLocation.Longitude == "" {
		// 	emptyFields = append(emptyFields, "ProviderLocation's Longitude")
		// } else if updatedData.ProviderPaymentOption == "" {
		// 	emptyFields = append(emptyFields, "ProviderPaymentOption")
		// } else if updatedData.ProviderEmail == "" {
		// 	emptyFields = append(emptyFields, "ProviderPaymentOption")

	}

	if len(emptyFields) > 0 {
		// Prepare response for empty fields
		responseBody := ErrorResponse{
			Message: fmt.Sprintf("Missing required fields: %s", strings.Join(emptyFields, ", ")),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Update the specific records in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}

	itemID := generateItemID()

	// Assign ItemID to updatedData
	updatedData.ItemID = itemID
	updatedData.CreatedAt = ConvertToNairobiTime()

	// Push updated data to the cart array
	update := bson.M{
		"$push": bson.M{
			"cart": updatedData,
		},
	}

	// Perform update operation
	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating Customer", http.StatusInternalServerError)
		return
	}

	// Retrieve the updated customer document
	var updatedCustomer Customer
	err = collection.FindOne(context.Background(), filter).Decode(&updatedCustomer)
	if err != nil {
		http.Error(w, "Error retrieving updated Customer", http.StatusInternalServerError)
		return
	}

	// Respond with the updated customer model
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(updatedCustomer)
}
func storeProviderDetailsToCart(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData CartObject
	err = decoder.Decode(&updatedData)
	if err != nil {
		fmt.Println("ERROR", err)
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("updated Data", updatedData)

	// Check for empty fields
	var emptyFields []string
	// if updatedData.CustomerEmail == "" {
	// 	emptyFields = append(emptyFields, "CustomerEmail")
	// } else if updatedData.CustomerID == "" {
	// 	emptyFields = append(emptyFields, "CustomerID")
	// } else if updatedData.CustomerLocation.Latitude == "" {
	// 	emptyFields = append(emptyFields, "CustomerLocation's Latitude")
	// } else if updatedData.CustomerLocation.Longitude == "" {
	// 	emptyFields = append(emptyFields, "CustomerLocation's Longitude")
	// } else if updatedData.CustomerName == "" {
	// 	emptyFields = append(emptyFields, "CustomerName")
	// } else if updatedData.CustomerPhoneNumber == "" {
	// 	emptyFields = append(emptyFields, "CustomerPhoneNumber")

	if updatedData.ProviderName == "" {
		emptyFields = append(emptyFields, "ProviderName")
	} else if updatedData.ProviderID == "" {
		emptyFields = append(emptyFields, "ProviderID")
	} else if updatedData.ProviderPhoneNumber == "" {
		emptyFields = append(emptyFields, "ProviderPhoneNumber")
	} else if updatedData.ProviderLocation.Latitude == 0 {
		emptyFields = append(emptyFields, "ProviderLocation's Latitude")
	} else if updatedData.ProviderLocation.Longitude == 0 {
		emptyFields = append(emptyFields, "ProviderLocation's Longitude")
	} else if updatedData.ProviderPaymentOption == "" {
		emptyFields = append(emptyFields, "ProviderPaymentOption")
	} else if updatedData.ProviderEmail == "" {
		emptyFields = append(emptyFields, "ProviderPaymentOption")

	}

	if len(emptyFields) > 0 {
		// Prepare response for empty fields
		responseBody := ErrorResponse{
			Message: fmt.Sprintf("Missing required fields: %s", strings.Join(emptyFields, ", ")),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Update the specific records in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}

	itemID := generateItemID()

	// Assign ItemID to updatedData
	updatedData.ItemID = itemID
	updatedData.CreatedAt = ConvertToNairobiTime()

	// Push updated data to the cart array
	update := bson.M{
		"$push": bson.M{
			"cart": updatedData,
		},
	}

	// Perform update operation
	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating Customer", http.StatusInternalServerError)
		return
	}

	// Retrieve the updated customer document
	var updatedCustomer Customer
	err = collection.FindOne(context.Background(), filter).Decode(&updatedCustomer)
	if err != nil {
		http.Error(w, "Error retrieving updated Customer", http.StatusInternalServerError)
		return
	}

	// Respond with the updated customer model
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(updatedCustomer)
}

// func storeItemToCart(w http.ResponseWriter, r *http.Request) {
// 	// Get the facility ID from the request parameters
// 	fmt.Println("Handle update is called")
// 	params := mux.Vars(r)
// 	facilityID := params["id"]

// 	// Convert the facility ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(facilityID)
// 	if err != nil {
// 		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Parse JSON data from the request
// 	decoder := json.NewDecoder(r.Body)
// 	var updatedData CartObject
// 	err = decoder.Decode(&updatedData)
// 	if err != nil {
// 		fmt.Println("ERROR", err)
// 		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
// 		return
// 	}

// 	fmt.Println("provider ProviderName", updatedData.ProviderName)
// 	fmt.Println("provider ProviderID", updatedData.ProviderID)

// 	fmt.Println("provider ProviderPhoneNumber", updatedData.ProviderPhoneNumber)

// 	fmt.Println("provider ProviderEmail", updatedData.ProviderEmail)

// 	fmt.Println("provider Latitude", updatedData.ProviderLocation.Latitude)

// 	fmt.Println("provider Longitude", updatedData.ProviderLocation.Longitude)

// 	fmt.Println("provider ProviderPaymentOption", updatedData.ProviderPaymentOption)

// 	fmt.Println("customer ID", updatedData.CustomerID)
// 	fmt.Println("customer  name", updatedData.CustomerName)
// 	fmt.Println("customer phone number", updatedData.CustomerPhoneNumber)
// 	fmt.Println("customer email", updatedData.CustomerEmail)

// 	// if updatedData.ProviderName == "" ||
// 	// 	updatedData.ProviderID == "" ||
// 	// 	updatedData.ProviderPhoneNumber == "" ||
// 	// 	updatedData.ProviderEmail == "" ||
// 	// 	updatedData.ProviderLocation.Latitude == "" ||
// 	// 	updatedData.ProviderLocation.Longitude == "" ||
// 	// 	updatedData.ProviderPaymentOption == "" ||

// 	// 	updatedData.CustomerID == "" ||
// 	// 	updatedData.CustomerName == "" ||
// 	// 	updatedData.CustomerPhoneNumber == "" ||
// 	// 	updatedData.CustomerEmail == "" ||

// 	// 	updatedData.CustomerLocation.Latitude == "" ||
// 	// 	updatedData.CustomerLocation.Longitude == "" {

// 	// 	//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

// 	// 	responseBody := ErrorResponse{
// 	// 		Message: "Missing one or more Required fields",
// 	// 		Status:  "Error",
// 	// 	}

// 	// 	w.Header().Set("Content-Type", "application/json")
// 	// 	json.NewEncoder(w).Encode(responseBody)
// 	// 	return

// 	// }

// 	var emptyFields []string

// 	if updatedData.ProviderName == "" {
// 		emptyFields = append(emptyFields, "ProviderName")
// 	}
// 	if updatedData.ProviderID == "" {
// 		emptyFields = append(emptyFields, "ProviderID")
// 	}
// 	if updatedData.ProviderPhoneNumber == "" {
// 		emptyFields = append(emptyFields, "ProviderPhoneNumber")
// 	}
// 	if updatedData.ProviderEmail == "" {
// 		emptyFields = append(emptyFields, "ProviderEmail")
// 	}
// 	if updatedData.ProviderLocation.Latitude == "" {
// 		emptyFields = append(emptyFields, "ProviderLocation.Latitude")
// 	}
// 	if updatedData.ProviderLocation.Longitude == "" {
// 		emptyFields = append(emptyFields, "ProviderLocation.Longitude")
// 	}
// 	if updatedData.ProviderPaymentOption == "" {
// 		emptyFields = append(emptyFields, "ProviderPaymentOption")
// 	}
// 	if updatedData.CustomerID == "" {
// 		emptyFields = append(emptyFields, "CustomerID")
// 	}
// 	if updatedData.CustomerName == "" {
// 		emptyFields = append(emptyFields, "CustomerName")
// 	}
// 	if updatedData.CustomerPhoneNumber == "" {
// 		emptyFields = append(emptyFields, "CustomerPhoneNumber")
// 	}
// 	if updatedData.CustomerEmail == "" {
// 		emptyFields = append(emptyFields, "CustomerEmail")
// 	}
// 	if updatedData.CustomerLocation.Latitude == "" {
// 		emptyFields = append(emptyFields, "CustomerLocation.Latitude")
// 	}
// 	if updatedData.CustomerLocation.Longitude == "" {
// 		emptyFields = append(emptyFields, "CustomerLocation.Longitude")
// 	}

// 	if len(emptyFields) > 0 {
// 		// Prepare response for empty fields
// 		responseBody := ErrorResponse{
// 			Message: fmt.Sprintf("Missing required fields: %s", strings.Join(emptyFields, ", ")),
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Update the specific records in MongoDB
// 	collection := client.Database(dbName).Collection(customer_collection)
// 	filter := bson.M{"_id": objID}
// 	///update := bson.M{"$set": updatedData}

// 	fmt.Println("incoming data-->", updatedData)

// 	itemID := generateItemID()

// 	// Assign ItemID to updatedData
// 	updatedData.ItemID = itemID
// 	updatedData.CreatedAt = ConvertToNairobiTime()

// 	//push partener services
// 	update := bson.M{
// 		"$push": bson.M{
// 			"cart": updatedData,
// 		},
// 	}

// 	_, err = collection.UpdateOne(context.Background(), filter, update)
// 	if err != nil {
// 		http.Error(w, "Error updating Customer", http.StatusInternalServerError)
// 		return
// 	}

// 	// Respond with a success message
// 	//fmt.Fprintf(w, "Facility updated successfully")

// 	response := map[string]string{"message": "Cart updated successfully"}
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(response)

// }

func removeItemFromCartHandler(w http.ResponseWriter, r *http.Request) {
	// Parse request parameters
	id := r.URL.Query().Get("id")
	itemID := r.URL.Query().Get("item_id")

	// Get MongoDB collection
	collection := client.Database(dbName).Collection(customer_collection)

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Filter to find the document by ID
	filter := bson.M{"_id": objID}

	// Update operation to remove item from cart
	update := bson.M{"$pull": bson.M{"cart": bson.M{"item_id": itemID}}}

	// Perform update operation
	updateResult, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		// Check if document not found error
		if err == mongo.ErrNoDocuments {
			http.Error(w, "Document not found", http.StatusNotFound)
			log.Println("Document not found:", err)
			return
		}

		// Otherwise, handle other errors
		http.Error(w, "Failed to update document", http.StatusInternalServerError)
		log.Println("Failed to update document:", err)
		return
	}

	// Check if any document was modified
	if updateResult.ModifiedCount == 0 {
		// If the item was not found in the cart, return status code 400
		http.Error(w, "Item not found in cart", http.StatusBadRequest)
		log.Println("Item not found in cart")
		return
	}

	// Fetch the updated document from the database
	var updatedDocument Customer
	err = collection.FindOne(context.Background(), filter).Decode(&updatedDocument)
	if err != nil {
		http.Error(w, "Failed to fetch updated document", http.StatusInternalServerError)
		log.Println("Failed to fetch updated document:", err)
		return
	}

	// Serialize the updated document to JSON
	responseJSON, err := json.Marshal(updatedDocument)
	if err != nil {
		http.Error(w, "Failed to serialize response", http.StatusInternalServerError)
		log.Println("Failed to serialize response:", err)
		return
	}

	// Return the serialized updated document as the response
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(responseJSON)
}

// /ADD SERVICES TO CART
// func addServiceToCart(w http.ResponseWriter, r *http.Request) {
// 	// Get the document ID and item ID from the request parameters
// 	params := mux.Vars(r)
// 	docID := params["doc_id"]
// 	itemID := params["item_id"]

// 	fmt.Println("itemID", docID)

// 	// Convert docID to ObjectID
// 	objID, err := primitive.ObjectIDFromHex(docID)
// 	if err != nil {
// 		fmt.Println("ERR", err)
// 		http.Error(w, "Invalid Document ID", http.StatusBadRequest)
// 		return
// 	}
// 	Service_ID := generateItemID()
// 	fmt.Println("Service ID", Service_ID)

// 	// Parse JSON data from the request
// 	decoder := json.NewDecoder(r.Body)
// 	var serviceToAdd PartnerServiceObject
// 	err = decoder.Decode(&serviceToAdd)
// 	if err != nil {
// 		fmt.Println("ERROR", err)
// 		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
// 		return
// 	}

// 	serviceToAdd.ServiceID = Service_ID
// 	serviceToAdd.CreatedAt = ConvertToNairobiTime()

// 	// Update the specific record in MongoDB
// 	collection := client.Database(dbName).Collection(customer_collection)
// 	filter := bson.M{"_id": objID, "cart.item_id": itemID}

// 	// Prepare update operation
// 	update := bson.M{
// 		"$addToSet": bson.M{
// 			"cart.$.services": serviceToAdd,
// 		},
// 	}

// 	// Perform update operation
// 	// _, err = collection.UpdateOne(context.Background(), filter, update)
// 	// if err != nil {
// 	// 	http.Error(w, "Error updating services in cart", http.StatusInternalServerError)
// 	// 	return
// 	// }
// 	// Perform update operation and retrieve the updated customer document
// 	var updatedCustomer Customer
// 	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
// 	err = collection.FindOneAndUpdate(context.Background(), filter, update, options).Decode(&updatedCustomer)
// 	if err != nil {
// 		http.Error(w, "Error updating services in cart", http.StatusInternalServerError)
// 		return
// 	}
// 	// Respond with the updated customer document
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(updatedCustomer)

// 	// Respond with a success message
// 	// response := map[string]string{"message": "Service added to cart successfully"}
// 	// w.Header().Set("Content-Type", "application/json")
// 	// json.NewEncoder(w).Encode(response)
// }

// func addServiceToCart(w http.ResponseWriter, r *http.Request) {
// 	// Get the document ID and item ID from the query parameters
// 	query := r.URL.Query()
// 	docID := query.Get("id")
// 	itemID := query.Get("cart_item_id")

// 	fmt.Println("itemID", docID)

// 	// Validate document ID and item ID
// 	if docID == "" || itemID == "" {
// 		http.Error(w, "Missing required parameters", http.StatusBadRequest)
// 		return
// 	}

// 	// Convert docID to ObjectID
// 	objID, err := primitive.ObjectIDFromHex(docID)
// 	if err != nil {
// 		fmt.Println("ERR", err)
// 		http.Error(w, "Invalid Document ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Generate a service ID
// 	serviceID := generateItemID()
// 	fmt.Println("Service ID", serviceID)

// 	// Parse JSON data from the request
// 	decoder := json.NewDecoder(r.Body)
// 	var serviceToAdd PartnerServiceObject
// 	err = decoder.Decode(&serviceToAdd)
// 	if err != nil {
// 		fmt.Println("ERROR", err)
// 		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
// 		return
// 	}

// 	// Set service ID and creation timestamp
// 	serviceToAdd.ServiceID = serviceID
// 	serviceToAdd.CreatedAt = ConvertToNairobiTime()

// 	// Update the specific record in MongoDB
// 	collection := client.Database(dbName).Collection(customer_collection)
// 	filter := bson.M{"_id": objID, "cart.item_id": itemID}

// 	// Prepare update operation
// 	update := bson.M{
// 		"$addToSet": bson.M{
// 			"cart.$.services": serviceToAdd,
// 		},
// 	}

// 	// Perform update operation and retrieve the updated customer document
// 	var updatedCustomer Customer
// 	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
// 	err = collection.FindOneAndUpdate(context.Background(), filter, update, options).Decode(&updatedCustomer)
// 	if err != nil {
// 		http.Error(w, "Error updating services in cart", http.StatusInternalServerError)
// 		return
// 	}

// 	// Respond with the updated customer document
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(updatedCustomer)
// }

func addServiceToCart(w http.ResponseWriter, r *http.Request) {
	// Get the document ID and item ID from the query parameters
	query := r.URL.Query()
	docID := query.Get("id")
	itemID := query.Get("cart_item_id")

	fmt.Println("itemID", docID)

	// Validate document ID and item ID
	if docID == "" || itemID == "" {
		http.Error(w, "Missing required parameters", http.StatusBadRequest)
		return
	}

	// Convert docID to ObjectID
	objID, err := primitive.ObjectIDFromHex(docID)
	if err != nil {
		fmt.Println("ERR", err)
		http.Error(w, "Invalid Document ID", http.StatusBadRequest)
		return
	}

	// Generate a service ID
	serviceID := generateItemID()
	fmt.Println("Service ID", serviceID)

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var serviceToAdd PartnerServiceObject
	err = decoder.Decode(&serviceToAdd)
	if err != nil {
		fmt.Println("ERROR", err)
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Set service ID and creation timestamp
	serviceToAdd.ServiceID = serviceID
	serviceToAdd.CreatedAt = ConvertToNairobiTime()

	// Update the specific record in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID, "cart.item_id": itemID}

	// Prepare update operation
	update := bson.M{
		"$addToSet": bson.M{
			"cart.$.services": serviceToAdd,
		},
	}

	// Perform update operation and retrieve the updated customer document
	var updatedCustomer Customer
	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	err = collection.FindOneAndUpdate(context.Background(), filter, update, options).Decode(&updatedCustomer)
	if err != nil {
		http.Error(w, "Error updating services in cart", http.StatusInternalServerError)
		return
	}

	// Get the last added service
	var lastServiceAdded PartnerServiceObject
	if len(updatedCustomer.Cart) > 0 {
		for _, cart := range updatedCustomer.Cart {
			if cart.ItemID == itemID && len(cart.Services) > 0 {
				lastServiceAdded = cart.Services[len(cart.Services)-1]
				break
			}
		}
	}

	// Respond with the last added service
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(lastServiceAdded)
}

func removeServiceFromCart(w http.ResponseWriter, r *http.Request) {
	// Get the document ID, item ID, and service ID from the query parameters
	fmt.Println("Service removal endpoint hit")
	query := r.URL.Query()
	docID := query.Get("id")
	itemID := query.Get("cart_id")
	serviceID := query.Get("service_id")

	// Validate document ID, item ID, and service ID
	if docID == "" || itemID == "" || serviceID == "" {
		http.Error(w, "Missing required parameters", http.StatusBadRequest)
		return
	}

	// Convert docID to ObjectID
	objID, err := primitive.ObjectIDFromHex(docID)
	if err != nil {
		http.Error(w, "Invalid Document ID", http.StatusBadRequest)
		return
	}

	// Update the specific record in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID, "cart.item_id": itemID}

	// Prepare update operation to remove the specific service
	update := bson.M{
		"$pull": bson.M{
			"cart.$.services": bson.M{"service_id": serviceID},
		},
	}

	// Perform update operation and retrieve the updated customer document
	var updatedCustomer Customer
	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	err = collection.FindOneAndUpdate(context.Background(), filter, update, options).Decode(&updatedCustomer)
	if err != nil {
		http.Error(w, "Error removing service from cart", http.StatusInternalServerError)
		return
	}

	// Find the updated cart and return its services array
	var updatedServices []PartnerServiceObject
	for _, cart := range updatedCustomer.Cart {
		if cart.ItemID == itemID {
			updatedServices = cart.Services
			break
		}
	}

	// Respond with the updated services array
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(updatedServices)
}
