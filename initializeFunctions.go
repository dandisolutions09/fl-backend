package main

import (
	"context"
	"fmt"

	"github.com/patrickmn/go-cache"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

var c *cache.Cache

// func initializePartnersStatus() error {

// 	fmt.Println("checking for partner status....")

// 	collection := client.Database(dbName).Collection(partner_collection)

// 	// Define a filter to retrieve all documents
// 	filter := bson.M{}

// 	// Retrieve documents from the collection that match the filter
// 	cursor, err := collection.Find(context.Background(), filter)
// 	if err != nil {
// 		return err
// 	}
// 	defer cursor.Close(context.Background())

// 	// Iterate through the cursor and update the status of partners
// 	for cursor.Next(context.Background()) {
// 		var record Partner
// 		if err := cursor.Decode(&record); err != nil {
// 			return err
// 		}

// 		if record.MainBusinessName == "" ||
// 			record.Logo == "" ||
// 			record.PartnerAvailability.Monday.OpeningTime == "" ||
// 			record.Location.Latitude == 0 ||
// 			record.Location.Longitude == 0 ||
// 			len(record.PartnerServices) <= 0 ||
// 			record.ServiceType == "" {
// 			record.Status = "INACTIVE"
// 		} else {
// 			record.Status = "ACTIVE"
// 		}

// 		// Update the status of the partner in the collection
// 		_, err := collection.UpdateOne(
// 			context.Background(),
// 			bson.M{"_id": record.ID},
// 			bson.M{"$set": bson.M{"status": record.Status}},
// 		)
// 		if err != nil {
// 			return err
// 		}
// 	}

// 	return nil
// }

//////////////////////

// func initializeCustomerStatus() error {

// 	fmt.Println("checking for customer status....")

// 	collection := client.Database(dbName).Collection(customer_collection)

// 	// Define a filter to retrieve all documents
// 	filter := bson.M{}

// 	// Retrieve documents from the collection that match the filter
// 	cursor, err := collection.Find(context.Background(), filter)
// 	if err != nil {
// 		return err
// 	}
// 	defer cursor.Close(context.Background())

// 	// Iterate through the cursor and update the status of partners
// 	for cursor.Next(context.Background()) {
// 		var record Partner
// 		if err := cursor.Decode(&record); err != nil {
// 			return err
// 		}

// 		if record.Location.Latitude == 0 ||
// 			record.Location.Longitude == 0 ||
// 			record.PhoneNumber == "" {
// 			record.Status = "INACTIVE"
// 		} else {
// 			record.Status = "ACTIVE"
// 		}

// 		// Update the status of the partner in the collection
// 		_, err := collection.UpdateOne(
// 			context.Background(),
// 			bson.M{"_id": record.ID},
// 			bson.M{"$set": bson.M{"status": record.Status}},
// 		)
// 		if err != nil {
// 			return err
// 		}
// 	}

// 	return nil
// }

// func initializePartnersStatus() error {
// 	fmt.Println("checking for partner status....")

// 	// // Check if the result is already cached
// 	// if _, found := c.Get("partners_status"); found {
// 	// 	fmt.Println("Using cached partner status")
// 	// 	return nil
// 	// }

// 	// Connect to MongoDB
// 	collection := client.Database(dbName).Collection(partner_collection)

// 	// Define a filter to retrieve all documents
// 	filter := bson.M{}

// 	// Retrieve documents from the collection that match the filter
// 	cursor, err := collection.Find(context.Background(), filter)
// 	if err != nil {
// 		return err
// 	}
// 	defer cursor.Close(context.Background())

// 	// Iterate through the cursor and update the status of partners
// 	for cursor.Next(context.Background()) {
// 		var record Partner
// 		if err := cursor.Decode(&record); err != nil {
// 			return err
// 		}

// 		if record.MainBusinessName == "" ||
// 			record.Logo == "" ||
// 			record.PartnerAvailability.Monday.OpeningTime == "" ||
// 			record.Location.Latitude == 0 ||
// 			record.Location.Longitude == 0 ||
// 			len(record.PartnerServices) <= 0 ||
// 			record.ServiceType == "" {
// 			record.Status = "INACTIVE"
// 		} else {
// 			record.Status = "ACTIVE"
// 		}

// 		// Update the status of the partner in the collection
// 		_, err := collection.UpdateOne(
// 			context.Background(),
// 			bson.M{"_id": record.ID},
// 			bson.M{"$set": bson.M{"status": record.Status}},
// 		)
// 		if err != nil {
// 			return err
// 		}
// 	}

// 	// Cache the result
// 	//c.Set("partners_status", true, cache.DefaultExpiration)

// 	return nil
// }
// func main() {
// 	// Initialize the cache
// 	c = cache.New(5*time.Minute, 10*time.Minute)

// 	// Initialize MongoDB client
// 	client, _ = mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))

// 	// Call initializePartnersStatus
// 	err := initializePartnersStatus()
// 	if err != nil {
// 		fmt.Println("Error:", err)
// 	}
// }

func initializePartnersStatus() error {
	fmt.Println("Checking for partner status....")

	// Connect to MongoDB
	collection := client.Database(dbName).Collection(partner_collection)

	// Define a filter to retrieve all documents
	filter := bson.M{}

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return err
	}
	defer cursor.Close(context.Background())

	// Prepare a slice to hold update operations
	var updates []mongo.WriteModel

	// Iterate through the cursor and update the status of partners
	for cursor.Next(context.Background()) {
		var record Partner
		if err := cursor.Decode(&record); err != nil {
			return err
		}

		//fmt.Println("record", record.Status)

		// Determine partner status
		status := "ACTIVE"
		if record.MainBusinessName == "" ||
			record.Logo == "" ||
			record.PartnerAvailability.Monday.OpeningTime == "" ||
			record.Location.Latitude == 0 ||
			record.Location.Longitude == 0 ||
			len(record.PartnerServices) <= 0 ||
			record.ServiceType == "" {
			status = "INACTIVE"
		}

		// Append update operation to the slice
		update := mongo.NewUpdateOneModel()
		update.SetFilter(bson.M{"_id": record.ID})
		update.SetUpdate(bson.M{"$set": bson.M{"status": status}})
		updates = append(updates, update)
	}

	// Execute bulk write operation to update statuses

	fmt.Print("")
	_, err = collection.BulkWrite(context.Background(), updates)
	if err != nil {
		return err
	}

	//invalidatePartnerCache()

	return nil
}

// CUSTOMER UPDATE
func initializeCustomerStatus() error {
	fmt.Println("Checking for customer status....")

	// Connect to MongoDB
	collection := client.Database(dbName).Collection(customer_collection)

	// Define a filter to retrieve all documents
	filter := bson.M{}

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return err
	}
	defer cursor.Close(context.Background())

	// Prepare a slice to hold update operations
	var updates []mongo.WriteModel

	// Iterate through the cursor and update the status of customers
	for cursor.Next(context.Background()) {
		var record Customer
		if err := cursor.Decode(&record); err != nil {
			return err
		}

		// Determine customer status
		status := "ACTIVE"
		if record.Location.Latitude == 0 ||
			record.Location.Longitude == 0 ||
			record.PhoneNumber == "" {
			status = "INACTIVE"
		}

		// Append update operation to the slice
		update := mongo.NewUpdateOneModel()
		update.SetFilter(bson.M{"_id": record.ID})
		update.SetUpdate(bson.M{"$set": bson.M{"status": status}})
		updates = append(updates, update)
	}

	// Execute bulk write operation to update statuses
	_, err = collection.BulkWrite(context.Background(), updates)
	if err != nil {
		return err
	}

	//invalidatePartnerCache()

	return nil
}
