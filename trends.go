package main

import (
	"context"
	"encoding/json"
	"fmt"

	//"strings"

	//"encoding/json"
	//"fmt"
	//"strings"
	"time"

	//"fmt"
	"log"
	"net/http"
	"strconv"

	//"time"

	//"github.com/gohugoio/hugo/tpl/strings"
	"go.mongodb.org/mongo-driver/bson"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
)

func handleGetTrends(w http.ResponseWriter, r *http.Request) {

	collection := client.Database(dbName).Collection(partner_collection)

	// Parse query parameters for month and year
	monthStr := r.URL.Query().Get("month")
	yearStr := r.URL.Query().Get("year")

	wkStr := r.URL.Query().Get("weekNumber")

	// Convert month and year to integers
	month, err := strconv.Atoi(monthStr)
	if err != nil {
		http.Error(w, "Invalid month parameter", http.StatusBadRequest)
		return
	}
	year, err := strconv.Atoi(yearStr)
	if err != nil {
		http.Error(w, "Invalid year parameter", http.StatusBadRequest)
		return
	}

	wkNumber, err := strconv.Atoi(wkStr)
	if err != nil {
		http.Error(w, "Invalid week parameter", http.StatusBadRequest)
		return
	}

	// Validate month and year values
	if month < 1 || month > 12 {
		http.Error(w, "Month should be between 1 and 12", http.StatusBadRequest)
		return
	}
	if year < 1970 || year > 2100 {
		http.Error(w, "Year should be between 1970 and 2100", http.StatusBadRequest)
		return
	}

	// Define start and end dates based on month and year
	//start := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
	//end := start.AddDate(0, 1, 0).Add(-time.Nanosecond)

	// MongoDB aggregation pipeline to filter by date range
	pipeline := bson.A{
		bson.D{
			{"$match", bson.D{
				{"created_at", bson.D{
					{"$gte", fmt.Sprintf("%d-%02d-01 00:00:00", year, month)},
				}},
			}},
		},
		bson.D{
			{"$match", bson.D{
				{"created_at", bson.D{
					{"$lte", fmt.Sprintf("%d-%02d-31 23:59:59", year, month)},
				}},
			}},
		},
	}

	// Perform aggregation
	cursor, err := collection.Aggregate(context.Background(), pipeline)
	if err != nil {
		http.Error(w, "Error retrieving data", http.StatusInternalServerError)
		log.Println("Error aggregating data:", err)
		return
	}
	defer cursor.Close(context.Background())

	// Slice to hold results
	var partners []Partner

	// Iterate through the results
	for cursor.Next(context.Background()) {
		var result Partner
		if err := cursor.Decode(&result); err != nil {
			log.Println("Error decoding result:", err)
			continue
		}
		partners = append(partners, result)
	}

	fmt.Println("length", len(partners))

	// Check for cursor errors
	if err := cursor.Err(); err != nil {
		http.Error(w, "Error iterating results", http.StatusInternalServerError)
		log.Println("Cursor error:", err)
		return
	}

	// // Initialize maps to store counts for each week
	// weekCounts := make(map[string]map[string]int)

	// // Iterate through users and count by day of creation for each week
	// for _, user := range partners {
	// 	// Parse created_at date
	// 	createdAt, err := time.Parse("2006-01-02 15:04:05", user.CreatedAt)
	// 	if err != nil {
	// 		fmt.Println("Error parsing created_at:", err)
	// 		continue
	// 	}
	// 	// Calculate the week number
	// 	weekNumber := int((createdAt.Day() - 1) / 7) + 1
	// 	weekKey := fmt.Sprintf("wk%d", weekNumber)
	// 	// Initialize week map if not already
	// 	if _, ok := weekCounts[weekKey]; !ok {
	// 		weekCounts[weekKey] = make(map[string]int)
	// 	}
	// 	// Get day of the week (0 = Sunday, 1 = Monday, ..., 6 = Saturday)
	// 	dayOfWeek := createdAt.Weekday().String()
	// 	// Increment count for the day of creation in the respective week
	// 	weekCounts[weekKey][strings.ToLower(dayOfWeek[:3])]++
	// }

	// // Format the output as an array of objects
	// var result []map[string]interface{}
	// for week, counts := range weekCounts {
	// 	weekObj := make(map[string]interface{})
	// 	weekObj[week] = counts
	// 	result = append(result, weekObj)
	// }

	// // Print the result array
	// fmt.Println(result)

	// weekCounts := make(map[string][]int)

	// // Iterate through users and count by day of creation for each week
	// for _, user := range partners {
	// 	// Parse created_at date
	// 	createdAt, err := time.Parse("2006-01-02 15:04:05", user.CreatedAt)
	// 	if err != nil {
	// 		fmt.Println("Error parsing created_at:", err)
	// 		continue
	// 	}
	// 	// Calculate the week number
	// 	weekNumber := int((createdAt.Day()-1)/7) + 1
	// 	// Initialize week array if not already
	// 	if _, ok := weekCounts[fmt.Sprintf("wk%d", weekNumber)]; !ok {
	// 		weekCounts[fmt.Sprintf("wk%d", weekNumber)] = make([]int, 7)
	// 	}
	// 	// Get day of the week (0 = Sunday, 1 = Monday, ..., 6 = Saturday)
	// 	dayOfWeek := int(createdAt.Weekday())
	// 	// Increment count for the day of creation in the respective week
	// 	weekCounts[fmt.Sprintf("wk%d", weekNumber)][dayOfWeek]++
	// }

	// // Format the output as an array of integers representing counts for each day of the week
	// var weekly []int
	// for _, counts := range weekCounts {
	// 	for _, count := range counts {
	// 		weekly = append(weekly, count)
	// 	}
	// }

	getWeeklyCounts := func(weekNumber int) []int {
		// Initialize maps to store counts for each week
		weekCounts := make(map[string][]int)

		// Iterate through users and count by day of creation for each week
		for _, user := range partners {
			// Parse created_at date
			createdAt, err := time.Parse("2006-01-02 15:04:05", user.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing created_at:", err)
				continue
			}
			// Calculate the week number
			currentWeekNumber := int((createdAt.Day()-1)/7) + 1
			if currentWeekNumber != weekNumber {
				continue
			}

			fmt.Println("current week", currentWeekNumber)
			// Initialize week array if not already
			if _, ok := weekCounts[fmt.Sprintf("wk%d", weekNumber)]; !ok {
				weekCounts[fmt.Sprintf("wk%d", weekNumber)] = make([]int, 7)
			}
			// Get day of the week (0 = Sunday, 1 = Monday, ..., 6 = Saturday)
			dayOfWeek := int(createdAt.Weekday())
			// Increment count for the day of creation in the respective week
			weekCounts[fmt.Sprintf("wk%d", weekNumber)][dayOfWeek]++
		}

		// Return counts for the specific week as an array of integers
		var weekly []int
		if counts, ok := weekCounts[fmt.Sprintf("wk%d", weekNumber)]; ok {
			for _, count := range counts {
				weekly = append(weekly, count)
			}
		}
		return weekly
	}

	// Example: Get weekly counts for week 1
	week1Counts := getWeeklyCounts(wkNumber)
	fmt.Println("weekly =", week1Counts)

	// Return JSON response with results
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(week1Counts); err != nil {
		http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
		log.Println("JSON encoding error:", err)
		return
	}
}
