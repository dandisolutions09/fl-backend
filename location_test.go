// location.go
package main

// import "go.mongodb.org/mongo-driver/bson/primitive"

// Location is a GeoJSON type.
type Location_test struct {
    Type        string    `json:"type" bson:"type"`
    Coordinates []float64 `json:"coordinates" bson:"coordinates"`
}


// type Point struct {
//     ID       primitive.ObjectID `json:"id" bson:"_id"`
//     Title    string             `json:"title"`
//     Location Location           `json:"location"`
// }
// NewPoint returns a GeoJSON Point with longitude and latitude.
// func NewPoint(long, lat float64) Location {
//     return Location{
//         "Point",
//         []float64{long, lat},
//     }
// }