package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	// "io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"reflect"
	"strconv"

	//"reflect"
	"strings"
	"time"

	//"github.com/containerd/console"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	//"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	//"go.mongodb.org/mongo-driver/mongo"
)

type BookingObject struct {
	ID                    string      `json:"_id,omitempty" bson:"_id,omitempty"`
	BookingID             string      `json:"booking_id" bson:"booking_id"`
	Status                string      `json:"status" bson:"status"`
	ProviderName          string      `json:"provider_name" bson:"provider_name"`
	ProviderID            string      `json:"provider_id" bson:"provider_id"`
	ProviderPhoneNumber   string      `json:"provider_phone_number" bson:"provider_phone_number"`
	ProviderEmail         string      `json:"provider_email" bson:"provider_email"`
	ProviderLocation      Coordinates `json:"provider_location" bson:"provider_location"`
	ProviderPaymentOption string      `json:"provider_payment_option" bson:"provider_payment_option"`
	ProviderServiceType   string      `json:"provider_service_type" bson:"provider_service_type"`
	ProviderBusinessName  string      `json:"provider_businessname" bson:"provider_businessname"`

	CustomerID          string      `json:"customer_id" bson:"customer_id"`
	CustomerName        string      `json:"customer_name" bson:"customer_name"`
	CustomerPhoneNumber string      `json:"customer_phone_number" bson:"customer_phone_number"`
	CustomerEmail       string      `json:"customer_email" bson:"customer_email"`
	CustomerLocation    Coordinates `json:"customer_location" bson:"customer_location"`

	ServiceLocation Coordinates `json:"service_location" bson:"service_location"`

	BookingDate          string `json:"booking_date" bson:"booking_date"`
	BookingTime          string `json:"booking_time" bson:"booking_time"`
	Reason               string `json:"reason" bson:"reason"`
	BookingPaymentOption string `json:"booking_payment_option" bson:"booking_payment_option"`

	//Services  []PartnerServiceObject `json:"services" bson:"services"`
	Cart          []PartnerServiceObject `json:"cart" bson:"cart"`
	CompletedDate string                 `json:"completed_date" bson:"completed_date"`

	CreatedAt string `json:"created_at" bson:"created_at"`
}

type CartItem struct {
	ServiceID string `json:"service_id"`
	Number    string `json:"number"`
}

type Order struct {
	CustomerID  string     `json:"customer_id"`
	PartnerID   string     `json:"partner_id"`
	BookingTime string     `json:"booking_time"`
	BookingDate string     `json:"booking_date"`
	Cart        []CartItem `json:"cart"`
}

type BookingData struct {
	// Message    string  `json:"message"`
	//Token      string  `json:"token"`
	BookingDetails BookingObject `json:"booking_details"`
	//Partner_ID string  `json:"partner_id"`
	//PartnerID string  `json:"token,omitempty"`
	// Status string `json:"status,omitempty"`
}

type BookingDataPartnerDetails struct {
	// Message    string  `json:"message"`
	//Token      string  `json:"token"`
	BookingDetails []BookingObject `json:"booking_details"`
	Partner        Partner         `json:"partner"`
	//PartnerID string  `json:"token,omitempty"`
	// Status string `json:"status,omitempty"`
}

type BookingResponse struct {
	Message string `json:"message"`
	// Token      string  `json:"token"`
	// Partner    Partner `json:"partner"`
	// Partner_ID string  `json:"partner_id"`
	Data BookingData `json:"data"`

	Status string `json:"status"`
}

//BookingDataPartnerDetails

type BookingResponseWithPartnerDetails struct {
	Message string `json:"message"`
	// Token      string  `json:"token"`
	// Partner    Partner `json:"partner"`
	// Partner_ID string  `json:"partner_id"`
	Data BookingDataPartnerDetails `json:"data"`

	Status string `json:"status"`
}

// generateAlphanumeric generates a 10-character alphanumeric text in uppercase
func generateAlphanumeric() string {
	rand.Seed(time.Now().UnixNano())
	const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	result := make([]byte, 10)
	for i := 0; i < 10; i++ {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return string(result)
}

func AddPendingBooking(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData BookingObject
	err = decoder.Decode(&updatedData)
	if err != nil {
		fmt.Println("ERROR", err)
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("updated Data", updatedData)

	// Check for empty fields
	var emptyFields []string
	if updatedData.CustomerEmail == "" {
		emptyFields = append(emptyFields, "CustomerEmail")
	} else if updatedData.CustomerID == "" {
		emptyFields = append(emptyFields, "CustomerID")
	} else if updatedData.CustomerLocation.Latitude == 0 {
		emptyFields = append(emptyFields, "CustomerLocation's Latitude")
	} else if updatedData.CustomerLocation.Longitude == 0 {
		emptyFields = append(emptyFields, "CustomerLocation's Longitude")
	} else if updatedData.CustomerName == "" {
		emptyFields = append(emptyFields, "CustomerName")
	} else if updatedData.CustomerPhoneNumber == "" {
		emptyFields = append(emptyFields, "CustomerPhoneNumber")

	} else if updatedData.ProviderName == "" {
		emptyFields = append(emptyFields, "ProviderName")
	} else if updatedData.ProviderID == "" {
		emptyFields = append(emptyFields, "ProviderID")
	} else if updatedData.ProviderPhoneNumber == "" {
		emptyFields = append(emptyFields, "ProviderPhoneNumber")
	} else if updatedData.ProviderLocation.Latitude == 0 {
		emptyFields = append(emptyFields, "ProviderLocation's Latitude")
	} else if updatedData.ProviderLocation.Longitude == 0 {
		emptyFields = append(emptyFields, "ProviderLocation's Longitude")
	} else if updatedData.ProviderPaymentOption == "" {
		emptyFields = append(emptyFields, "ProviderPaymentOption")
	} else if updatedData.ProviderEmail == "" {
		emptyFields = append(emptyFields, "ProviderPaymentOption")

	}

	if len(emptyFields) > 0 {
		// Prepare response for empty fields
		responseBody := ErrorResponse{
			Message: fmt.Sprintf("Missing required fields: %s", strings.Join(emptyFields, ", ")),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Update the specific records in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}

	itemID := generateItemID()

	// Assign ItemID to updatedData
	updatedData.BookingID = itemID
	updatedData.CreatedAt = ConvertToNairobiTime()
	//updatedData.Services=[]
	updatedData.Cart = []PartnerServiceObject{} //empty cart

	fmt.Println("updated services", updatedData)

	// Push updated data to the cart array
	update := bson.M{
		"$set": bson.M{
			"pending_booking": updatedData,
		},
	}

	// Perform update operation
	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		fmt.Println("ERR", err)
		http.Error(w, "Error updating Customer", http.StatusInternalServerError)
		return
	}

	// Retrieve the updated customer document
	var updatedCustomer Customer
	err = collection.FindOne(context.Background(), filter).Decode(&updatedCustomer)
	if err != nil {
		fmt.Println("ERR", err)
		http.Error(w, "Error retrieving updated Customer", http.StatusInternalServerError)
		return
	}

	// Respond with the updated customer model
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(updatedCustomer)
}

func AddBookingTime(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	//params := mux.Vars(r)
	//facilityID := params["id"]

	userID := r.URL.Query().Get("id")

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData BookingObject
	err = decoder.Decode(&updatedData)
	if err != nil {
		fmt.Println("ERROR", err)
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("updated Data", updatedData)

	// Check for empty fields
	var emptyFields []string
	if updatedData.BookingDate == "" {
		emptyFields = append(emptyFields, "BookingDate")
	} else if updatedData.BookingTime == "" {
		emptyFields = append(emptyFields, "BookingTime")

	}

	if len(emptyFields) > 0 {
		// Prepare response for empty fields
		responseBody := ErrorResponse{
			Message: fmt.Sprintf("Missing required fields: %s", strings.Join(emptyFields, ", ")),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Update the specific records in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}

	//itemID := generateItemID()

	// Assign ItemID to updatedData
	//updatedData.ItemID = itemID
	//updatedData.CreatedAt = ConvertToNairobiTime()
	//updatedData.Services=[]
	//updatedData.Cart = []PartnerServiceObject{} //empty cart

	fmt.Println("updated services", updatedData)

	// Push updated data to the cart array
	update := bson.M{
		"$set": bson.M{
			"pending_booking.booking_time": updatedData.BookingTime,
			"pending_booking.booking_date": updatedData.BookingDate,
		},
	}

	// Perform update operation
	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		fmt.Println("ERR", err)
		http.Error(w, "Error updating Customer", http.StatusInternalServerError)
		return
	}

	// Retrieve the updated customer document
	var updatedCustomer Customer
	err = collection.FindOne(context.Background(), filter).Decode(&updatedCustomer)
	if err != nil {
		fmt.Println("ERR", err)
		http.Error(w, "Error retrieving updated Customer", http.StatusInternalServerError)
		return
	}

	// Respond with the updated customer model
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(updatedCustomer.PendingBooking)
}

func AddServiceToBooking(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData PartnerServiceObject
	err = decoder.Decode(&updatedData)
	if err != nil {
		fmt.Println("ERROR", err)
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("updated Data", updatedData)

	// Check for empty fields
	var emptyFields []string

	// {
	// 	"type": "full-shave",
	// 	"status": "active",
	// 	"description": "the best",
	// 	"subcategory": "Mens Hair",
	// 	"created_at": "222",
	// 	"price": "400",
	// 	"ServiceDuration": "2hrs"
	// }

	if updatedData.Type == "" {
		emptyFields = append(emptyFields, "Type")
	} else if updatedData.Status == "" {
		emptyFields = append(emptyFields, "Status")
	} else if updatedData.Price == "" {
		emptyFields = append(emptyFields, "Price")
	} else if updatedData.ServiceDuration == "" {
		emptyFields = append(emptyFields, "ServiceDuration")

	}

	if len(emptyFields) > 0 {
		// Prepare response for empty fields
		responseBody := ErrorResponse{
			Message: fmt.Sprintf("Missing required fields: %s", strings.Join(emptyFields, ", ")),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Update the specific records in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}

	//itemID := generateItemID()

	// Assign ItemID to updatedData
	//updatedData.ServiceID = itemID
	updatedData.CreatedAt = ConvertToNairobiTime()
	//updatedData.Services=[]
	//updatedData.Cart = []PartnerServiceObject{} //empty cart

	fmt.Println("INCOMING SERVICES ID:->", updatedData.ServiceID)

	// Push updated data to the cart array
	// update := bson.M{
	// 	"$set": bson.M{
	// 		"pending_booking": updatedData,
	// 	},

	// }

	// Push updated data to the cart array
	update := bson.M{
		"$push": bson.M{
			"pending_booking.cart": updatedData, // Assuming Cart is an array of objects
		},
	}

	// Perform update operation
	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		fmt.Println("ERR", err)
		http.Error(w, "Error updating Service", http.StatusInternalServerError)
		return
	}

	// Retrieve the updated customer document
	var updatedCustomer Customer
	err = collection.FindOne(context.Background(), filter).Decode(&updatedCustomer)
	if err != nil {
		fmt.Println("ERR", err)
		http.Error(w, "Error retrieving updated Service", http.StatusInternalServerError)
		return
	}

	//latestService := updatedCustomer.Cart[len(updatedCustomer.Cart)-1].Services[len(updatedCustomer.Cart[len(updatedCustomer.Cart)-1].Services)-1]
	//latestCartItem := updatedCustomer.PendingBooking[len(updatedCustomer.PendingBooking)-1]

	// Respond with the updated customer model
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(updatedCustomer)
}

func handleRemoveItemFromCart(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Remove item from cart has been called....")

	// Parse request parameters
	customerID := r.URL.Query().Get("customer_id")
	serviceID := r.URL.Query().Get("service_id")

	// Convert customer ID to ObjectID
	customerObjID, err := primitive.ObjectIDFromHex(customerID)
	if err != nil {
		http.Error(w, "Invalid Customer ID", http.StatusBadRequest)
		return
	}

	// Convert service ID to string
	serviceObjID := serviceID // Assuming service ID is already a string

	// Get the customer document from MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	var customer Customer
	err = collection.FindOne(context.Background(), bson.M{"_id": customerObjID}).Decode(&customer)
	if err != nil {
		fmt.Println("ERR", err)
		response := map[string]string{"message": "Customer not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Find and remove the service from the cart
	var updatedCart []PartnerServiceObject
	for _, item := range customer.PendingBooking.Cart {
		if item.ServiceID != serviceObjID {
			updatedCart = append(updatedCart, item)
		}
	}

	// If updated cart is empty, set it as an empty slice
	if len(updatedCart) == 0 {
		updatedCart = []PartnerServiceObject{}
	}

	// Update the customer document with the modified cart
	update := bson.M{"$set": bson.M{"pending_booking.cart": updatedCart}}
	_, err = collection.UpdateOne(context.Background(), bson.M{"_id": customerObjID}, update)
	if err != nil {
		fmt.Println("ERR", err)
		//response := map[string]string{"error": "Failed to update cart"}
		response := map[string]string{"message": "Failed to Update Cart", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the updated cart
	response := map[string]interface{}{"message": "Item removed from cart successfully", "cart": updatedCart}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleGetBookings(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(booking_collection)

	fmt.Println("Get all customers called")

	// Define a slice to store retrieved records
	var customers []BookingObject

	// Define a filter to retrieve only active records
	//filter := bson.M{"status": "active"}
	filter := bson.M{}

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the records slice
	for cursor.Next(context.Background()) {
		var record BookingObject
		if err := cursor.Decode(&record); err != nil {
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}
		customers = append(customers, record)
	}

	// Respond with the retrieved records in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(customers)
}

func handleGetBookingStatus_Partner(w http.ResponseWriter, r *http.Request) {
	// Define the response structure
	type BookingStatusResponse struct {
		Bookings []BookingObject `json:"bookings"`
		Partner  Partner         `json:"partner"`
		Status   string          `json:"status"`
	}

	type BookingStatusResponseEmpty struct {
		Bookings string  `json:"bookings,omitempty"`
		Partner  Partner `json:"partner,omitempty"`
		Status   string  `json:"status,omitempty"`
	}

	fmt.Println("Get admin by ID has been called....")

	// Extract the user ID from the query parameters
	userID := r.URL.Query().Get("id")

	fmt.Println("Passed user ID:", userID)

	// Access the bookings collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	filter := bson.M{
		"provider_id": userID,
		"status": bson.M{
			"$in": []string{"ONGOING", "UNRATED", "PAID"},
		},
	}

	// Define options to sort by timestamp
	options := options.Find().SetSort(bson.D{{"timestamp", 1}})

	// Find documents
	cursor, err := collection.Find(context.Background(), filter, options)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())

	// Slice to store the found bookings
	var bookings []BookingObject

	// Iterate over the cursor and decode each document into a BookingObject
	for cursor.Next(context.Background()) {
		var result BookingObject
		if err := cursor.Decode(&result); err != nil {
			log.Fatal(err)
		}
		bookings = append(bookings, result)
	}
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	retrievedPartner, err := RetrievePartnerByID(userID, client, dbName, partner_collection)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Partner not found",
			Status:  "Success",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
	} else {

		// If there are no bookings, set the bookings array to an empty array
		if len(bookings) == 0 {

			//BookingStatusResponse.Bookings = []BookingObject{}

			// responseBody := BookingStatusResponse{
			// 	Bookings: []BookingObject{},
			// 	Partner:  retrievedPartner,
			// 	Status:   "1",
			// 	// other fields...
			// }

			data := BookingDataPartnerDetails{

				// Partner_ID: storedUser.ID.Hex(),
				BookingDetails: []BookingObject{},
				Partner:        retrievedPartner,
				// Token:      token,
			}

			responseBody := BookingResponseWithPartnerDetails{
				Message: "Partner Booking Details Retrieved successfully",
				// Partner_ID: storedUser.ID.Hex(),
				// Partner:    retrievedPartner,
				// Token:      token,
				Data:   data,
				Status: "Success",
			}

			fmt.Println("response body", responseBody)

			//	Respond with the retrieved bookings or message in JSON format
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(responseBody)

		} else {

			// 	data := BookingData{

			// 	// Partner_ID: storedUser.ID.Hex(),
			// 	BookingDetails: updated_booking,
			// 	// Token:      token,
			// }

			// responseBody := BookingStatusResponse{
			// 	Bookings: bookings,
			// 	Partner:  retrievedPartner,
			// 	Status:   "1",
			// }

			data := BookingDataPartnerDetails{

				// Partner_ID: storedUser.ID.Hex(),
				BookingDetails: bookings,
				Partner:        retrievedPartner,
				// Token:      token,
			}

			responseBody := BookingResponseWithPartnerDetails{
				Message: "Partner Booking Details Retrieved successfully",
				// Partner_ID: storedUser.ID.Hex(),
				// Partner:    retrievedPartner,
				// Token:      token,
				Data:   data,
				Status: "Success",
			}

			//fmt.Println("response body", responseBody)

			// Respond with the retrieved bookings or message in JSON format
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(responseBody)

		}

	}

	// responseBody := BookingStatusResponse{
	// 	Bookings: bookings,
	// 	Partner:  retrievedPartner,
	// 	Status:   "1",
	// }

	//fmt.Println("response body", responseBody)

	// Respond with the retrieved bookings or message in JSON format
	//w.Header().Set("Content-Type", "application/json")
	//json.NewEncoder(w).Encode(responseBody)

}

func handleGetBookingStatus_Customer(w http.ResponseWriter, r *http.Request) {
	// Define the response structure
	type CustomerBookingStatusResponse struct {
		Bookings []BookingObject `json:"bookings"`
		Customer Customer        `json:"customer"`
		Status   string          `json:"status"`
	}

	type BookingStatusResponseEmpty struct {
		Bookings string  `json:"bookings,omitempty"`
		Partner  Partner `json:"partner,omitempty"`
		Status   string  `json:"status,omitempty"`
	}

	fmt.Println("Get admin by ID has been called....")

	// Extract the user ID from the query parameters
	userID := r.URL.Query().Get("id")

	fmt.Println("Passed user ID:", userID)

	// Access the bookings collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	filter := bson.M{
		"customer_id": userID,
		"status": bson.M{
			"$in": []string{"ONGOING", "UNRATED", "PAID"},
		},
	}

	// Define options to sort by timestamp
	options := options.Find().SetSort(bson.D{{"timestamp", 1}})

	// Find documents
	cursor, err := collection.Find(context.Background(), filter, options)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())

	// Slice to store the found bookings
	var bookings []BookingObject

	// Iterate over the cursor and decode each document into a BookingObject
	for cursor.Next(context.Background()) {
		var result BookingObject
		if err := cursor.Decode(&result); err != nil {
			log.Fatal(err)
		}
		bookings = append(bookings, result)
	}
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	retrievedCustomer, err := RetrieveCustomerByID(userID, client, dbName, customer_collection)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Customer not found",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
	}

	// If there are no bookings, set the bookings array to an empty array
	if len(bookings) == 0 {

		//BookingStatusResponse.Bookings = []BookingObject{}

		responseBody := CustomerBookingStatusResponse{
			Bookings: []BookingObject{},
			Status:   "1",
			Customer: retrievedCustomer,
			// other fields...
		}

		fmt.Println("response body", responseBody)

		//	Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	} else {

		responseBody := CustomerBookingStatusResponse{
			Bookings: bookings,
			Customer: retrievedCustomer,
			Status:   "1",
		}

		///Get

		//fmt.Println("response body", responseBody)

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

	// responseBody := BookingStatusResponse{
	// 	Bookings: bookings,
	// 	Partner:  retrievedPartner,
	// 	Status:   "1",
	// }

	//fmt.Println("response body", responseBody)

	// Respond with the retrieved bookings or message in JSON format
	//w.Header().Set("Content-Type", "application/json")
	//json.NewEncoder(w).Encode(responseBody)

}

func insertBookingRecord(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	//func handleGetCustomerId(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get admin by ID has been called....")
	userID := r.URL.Query().Get("id")

	fmt.Println("Passed user ID:", userID)

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific admin from MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	var customer Customer
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&customer)
	if err != nil {
		fmt.Println("ERR", err)
		response := map[string]string{"message": "Customer not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved admin in JSON format
	//w.Header().Set("Content-Type", "application/json")
	//json.NewEncoder(w).Encode(admin)
	//}

	//customer.PendingBooking.ItemID = generateAlphanumeric()
	//customer.PendingBooking.Status = "ONGOING"

	//bookingRecord := customer.PendingBooking

	fmt.Println("Provider Business Name:->", customer.PendingBooking.ProviderBusinessName)

	// fmt.Println("Booking Record Name", bookingRecord.ProviderName)
	// fmt.Println("Booking Record Provider ID", bookingRecord.ProviderID)
	// fmt.Println("Booking Record Provider Phonenumber", bookingRecord.ProviderPhoneNumber)
	// fmt.Println("Booking Record Provider Email", bookingRecord.ProviderEmail)

	//	fmt.Println("final booking object", customer.PendingBooking.Cart)

	counts := countServices(customer.PendingBooking.Cart)
	var order CartItem

	var orders []CartItem

	for id, count := range counts {
		fmt.Printf("Service ID: %s, Count: %d\n", id, count)
		order.ServiceID = id
		order.Number = strconv.Itoa(count)
		fmt.Println("new order format", order)
		orders = append(orders, order)

		//bookings = append(bookings, booking)

	}

	fmt.Println("orders", orders)

	objID_partnerID, err := primitive.ObjectIDFromHex(customer.PendingBooking.ProviderID) //passed providerID
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	var bookingRecord BookingObject

	collection_partners := client.Database(dbName).Collection(partner_collection)
	var retrievedPartner Partner
	err = collection_partners.FindOne(context.Background(), bson.M{"_id": objID_partnerID}).Decode(&retrievedPartner)
	if err != nil {
		fmt.Println("ERR", err)
		response := map[string]string{"message": "Partner not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	fmt.Println("retrieved partner SERVICES", retrievedPartner.PartnerServices)
	var cart []PartnerServiceObject

	for _, cartItem := range orders {
		if service, exists := findServiceByID(cartItem.ServiceID, retrievedPartner.PartnerServices); exists {
			fmt.Printf("ServiceID %s exists in the services array.\n", cartItem.ServiceID)
			service.Number = cartItem.Number // Update the number from the cart item
			cart = append(cart, service)
		} else {
			fmt.Printf("ServiceID %s does not exist in the services array.\n", cartItem.ServiceID)
		}
	}

	fmt.Println("PENDING CART ", cart)

	bookingRecord.BookingID = generateAlphanumeric()
	bookingRecord.Status = "UPCOMING"
	bookingRecord.ProviderName = customer.PendingBooking.ProviderName
	bookingRecord.ProviderID = customer.PendingBooking.ProviderID
	bookingRecord.ProviderPhoneNumber = customer.PendingBooking.ProviderPhoneNumber
	bookingRecord.ProviderEmail = customer.PendingBooking.ProviderEmail
	bookingRecord.ProviderLocation = customer.PendingBooking.ProviderLocation
	bookingRecord.ProviderPaymentOption = customer.PendingBooking.ProviderPaymentOption
	bookingRecord.ProviderServiceType = customer.PendingBooking.ProviderServiceType
	bookingRecord.ProviderBusinessName = customer.PendingBooking.ProviderBusinessName
	bookingRecord.CustomerID = customer.PendingBooking.CustomerID
	bookingRecord.CustomerName = customer.PendingBooking.CustomerName
	bookingRecord.CustomerPhoneNumber = customer.PendingBooking.CustomerPhoneNumber
	bookingRecord.CustomerEmail = customer.PendingBooking.CustomerEmail
	bookingRecord.CustomerLocation = customer.PendingBooking.CustomerLocation
	bookingRecord.BookingDate = customer.PendingBooking.BookingDate
	bookingRecord.BookingTime = customer.PendingBooking.BookingTime
	bookingRecord.Cart = cart
	bookingRecord.CreatedAt = customer.PendingBooking.CreatedAt
	bookingRecord.ServiceLocation = customer.PendingBooking.ProviderLocation

	fmt.Println("final booking cart", bookingRecord.Cart)

	fmt.Println("booking record", bookingRecord)

	// Insert the person object into the collection
	collection2 := client.Database(dbName).Collection(booking_collection)
	insertResult, err := collection2.InsertOne(context.Background(), bookingRecord)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("BOOKING INSERTED")

	// Extract the inserted ID
	// insertedID := insertResult

	insertedID := insertResult.InsertedID.(primitive.ObjectID)

	// Retrieve the inserted record from the database
	var insertedRecord BookingObject
	err = collection2.FindOne(context.Background(), bson.M{"_id": insertedID}).Decode(&insertedRecord)
	if err != nil {
		http.Error(w, "Failed to retrieve inserted booking", http.StatusInternalServerError)
		return
	}

	response := map[string]interface{}{
		"message": "Booking created successfully",
		"status":  "Success",
		"data":    insertedRecord, // Include the whole object in the response
	}

	fmt.Print("response", response)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

	//var newBooking BookingObject

	// // Insert the person object into the collection
	// collection2 := client.Database(dbName).Collection(booking_collection)

	// _, err = collection2.InsertOne(context.Background(), bookingRecord)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// fmt.Println("BOOKING INSERTED")

	// fmt.Println("Record inserted successfully!")

	// response := map[string]interface{}{
	// 	"message": "Booking created successfully",
	// 	"status":  "Success",
	// 	//"ID":      updatedCustomer,
	// }
	// json.NewEncoder(w).Encode(response)
	// var resetBooking BookingObject

	// resetBooking.BookingID = ""
	// resetBooking.Status = ""
	// resetBooking.ProviderName = ""
	// resetBooking.ProviderID = ""
	// resetBooking.ProviderPhoneNumber = ""
	// resetBooking.ProviderEmail = ""
	// resetBooking.ProviderLocation.Address = ""
	// resetBooking.ProviderLocation.Latitude = 0
	// resetBooking.ProviderLocation.Longitude = 0
	// resetBooking.ProviderPaymentOption = ""
	// resetBooking.ProviderServiceType = ""
	// resetBooking.ProviderBusinessName = ""
	// resetBooking.CustomerID = ""
	// resetBooking.CustomerName = ""
	// resetBooking.CustomerPhoneNumber = ""
	// resetBooking.CustomerEmail = ""
	// resetBooking.CustomerLocation.Latitude = 0
	// resetBooking.CustomerLocation.Longitude = 0
	// resetBooking.CustomerLocation.Address = ""
	// resetBooking.BookingDate = ""
	// resetBooking.BookingTime = ""
	// resetBooking.Cart = []PartnerServiceObject{}
	// resetBooking.CreatedAt = ""

	// //resetBookingObject(&resetBooking)

	// // Push updated data to the cart array
	// update := bson.M{
	// 	"$set": bson.M{
	// 		"pending_booking": resetBooking,
	// 	},
	// }

	// filter := bson.M{"_id": objID}

	// // Perform update operation
	// _, err = collection.UpdateOne(context.Background(), filter, update)
	// if err != nil {
	// 	fmt.Println("ERR", err)
	// 	http.Error(w, "Error updating Customer", http.StatusInternalServerError)
	// 	return
	// }

	// // Retrieve the updated customer document
	// var updatedCustomer Customer
	// err = collection.FindOne(context.Background(), filter).Decode(&updatedCustomer)
	// if err != nil {
	// 	fmt.Println("ERR", err)
	// 	http.Error(w, "Error retrieving updated Customer", http.StatusInternalServerError)
	// 	return
	// }

	//fmt.Println("pending bookin reset successfully", insertResult)

}

func countServices(services []PartnerServiceObject) map[string]int {
	counts := make(map[string]int)
	for _, service := range services {
		counts[service.ServiceID]++
	}
	return counts
}

func handleGetBookingsSummary(w http.ResponseWriter, r *http.Request) {
	//collection := client.Database(dbName).Collection(booking_collection)

	type BookingSummaryResponseData struct {
		TotalBookings         int `json:"total_completed_Bookings"`
		TotalMoney            int `json:"total_money"`
		TotalUpcomingBookings int `json:"total_upcoming_bookings"`
		// Status                string `json:"status"`
	}

	type BookingSummaryResponse struct {
		Message string `json:"message"`
		// Token      string  `json:"token"`
		// Partner    Partner `json:"partner"`
		// Partner_ID string  `json:"partner_id"`
		Data BookingSummaryResponseData `json:"data"`

		Status string `json:"status"`
	}

	//RegistrationResponse

	type BookingSummaryResponseEmpty struct {
		Bookings string  `json:"bookings,omitempty"`
		Partner  Partner `json:"partner,omitempty"`
		Status   string  `json:"status,omitempty"`
	}

	fmt.Println("Get bookings made in the last 24 hours called")

	userID := r.URL.Query().Get("id")

	fmt.Println("Passed user ID:", userID)

	var bookings []BookingObject
	collection := client.Database(dbName).Collection(booking_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	filter := bson.M{
		"provider_id": userID,
		// "status": bson.M{
		// 	"$in": []string{"ONGOING", "UNRATED", "PAID"},
		// },
	}

	// cursor, err := collection.Find(context.Background(), bson.D{})
	// if err != nil {
	// 	http.Error(w, err.Error(), http.StatusInternalServerError)
	// 	return
	// }

	// Find documents
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var booking BookingObject
		err := cursor.Decode(&booking)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		//fmt.Println("Booking", booking)
		bookings = append(bookings, booking)
	}

	//retrievedPartner, err := RetrievePartnerByID(client, dbName, partner_collection, objID)

	retrievedPartner, err := RetrievePartnerByID(userID, client, dbName, partner_collection)

	fmt.Println("error retrieving partner", err)

	if err != nil {
		responseBody := ErrorResponse{
			Message: "Partner Not Found",
			Status:  "Error",
		}

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

		return

	}

	openingTime, closingTime, err := getTodayAvailability(retrievedPartner)

	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error retrieving today availability",
			Status:  "Error",
		}

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

	//fmt.Print("openingtime", openingTime, closingTime)

	var filteredBookings []BookingObject
	var allBookings []BookingObject
	var upcomingBookings []BookingObject

	var completedBookings []BookingObject

	//	var filteredBookings []BookingObject

	for _, obj := range bookings {

		fmt.Println("ALL BOOKINGS->>", obj.Status)

		if obj.Status == "UPCOMING" {
			fmt.Println("UPCOMING FOR 665976174c3880766f41965b", obj.Status)
			upcomingBookings = append(upcomingBookings, obj)

		} else {
			fmt.Print("other bookings")

		}

	}
	for _, obj := range bookings {
		createdAt, err := time.Parse("2006-01-02 15:04:05", obj.CreatedAt)
		completed_date, err := time.Parse("2006-01-02 15:04:05", obj.CompletedDate)

		//fmt.Println("created at", createdAt)
		if err != nil {
			fmt.Println("Error parsing time:", err)
			continue
		}

		fmt.Println("opening time", openingTime)
		fmt.Println("closing time", closingTime)

		openingTimeParsed, _ := time.Parse("03:04 PM", openingTime)
		closingTimeParsed, _ := time.Parse("03:04 PM", closingTime)

		// fmt.Println("parsed opening time", openingTimeParsed)
		// fmt.Println("parsed closing time", closingTimeParsed)

		// fmt.Println("Formated opening time", openingTimeParsed.Format("03:04 PM"))
		// fmt.Println("Formated closing time", closingTimeParsed.Format("03:04 PM"))

		// if createdAtTimeObj.After(openingTimeParsed) && createdAtTimeObj.Before(closingTimeParsed) {
		// 	fmt.Println("The time is within the opening and closing hours.", obj.CreatedAt)
		// } else {
		// 	fmt.Println("The time is outside the opening and closing hours.")
		// }
		fmt.Println("ALL BOOKINGS", obj.Status)

		if obj.Status == "UPCOMING" {
			fmt.Println("UPCOMING FOR 665976174c3880766f41965b", obj.Status)
			allBookings = append(allBookings, obj)

		} else {
			fmt.Print("other bookings")

		}

		// Parse string to time.Time object
		createdAtTimeObj, err := time.Parse("2006-01-02 15:04:05", obj.CreatedAt)
		if err != nil {
			fmt.Println("Error parsing createdAt:", err)
			return
		}

		completedTimeObj, err := time.Parse("2006-01-02 15:04:05", obj.CompletedDate)
		if err != nil {
			fmt.Println("Error parsing completed_date:", err)
			return
		}

		// fmt.Println("Created At Year", createdAtTimeObj.Year())
		// fmt.Println("Created At YearDay", createdAtTimeObj.YearDay())

		// fmt.Println("current year", time.Now().Year())
		// fmt.Println("current year day", time.Now().YearDay())

		// Check if createdAt matches today's date
		if createdAtTimeObj.Year() == time.Now().Year() && createdAtTimeObj.YearDay() == time.Now().YearDay() {
			// Convert to 12-hour clock format
			fmt.Println("--->matches todays date")
			createdAt12Hour := createdAtTimeObj.Format("03:04 PM")

			// Extract time portion
			createdAtTime := createdAt.Format("15:04:05")

			//fmt.Println("created")

			// Parse time string to time.Time object
			createdAtTimeObj2, _ := time.Parse("15:04:05", createdAtTime)

			// Check if createdAt12Hour is between opening and closing time
			if createdAtTimeObj2.After(openingTimeParsed) && createdAtTimeObj2.Before(closingTimeParsed) {
				fmt.Println("The time is within the opening and closing hours:", createdAt12Hour)
				filteredBookings = append(filteredBookings, obj)
			} else {
				fmt.Println("The time is outside the opening and closing hours:", createdAt12Hour)
			}
		}

		if completedTimeObj.Year() == time.Now().Year() && completedTimeObj.YearDay() == time.Now().YearDay() {
			// Convert to 12-hour clock format
			fmt.Println("--->matches todays date ---completed bookings", obj)
			completedTime12Hour := completedTimeObj.Format("03:04 PM")

			// Extract time portion
			completedDate_Time := completed_date.Format("15:04:05")

			fmt.Println("completed time in 24 hours-time portion", completedDate_Time)

			//fmt.Println("created")

			// Parse time string to time.Time object
			completedDate_TimeObj2, _ := time.Parse("15:04:05", completedDate_Time)
			fmt.Println("completed time in 24 hours", completedDate_TimeObj2)

			// Check if createdAt12Hour is between opening and closing time
			if completedDate_TimeObj2.After(openingTimeParsed) && completedDate_TimeObj2.Before(closingTimeParsed) {
				fmt.Println("The time is within the opening and closing hours, opening-->:openingTimeParsed, ", completedTime12Hour, openingTimeParsed, closingTimeParsed)
				completedBookings = append(completedBookings, obj)
				fmt.Println("within", obj)
			} else {
				fmt.Println("The time is outside the opening and closing hours:", completedTime12Hour)
			}
		}

	}

	// for _, obj := range bookings {
	// 	createdAt, err := time.Parse("2006-01-02 15:04:05", obj.CreatedAt)
	// 	if err != nil {
	// 		fmt.Println("Error parsing time:", err)
	// 		continue
	// 	}

	// 	if createdAt.After(opening) && createdAt.Before(closing) {
	// 		filteredBookings = append(filteredBookings, obj)
	// 	}
	// }

	// Respond with the count

	totalAmount := 0
	for _, obj := range completedBookings {
		// Check if the status is "finished"
		if obj.Status == "COMPLETED" {
			for _, item := range obj.Cart {
				price, err := strconv.Atoi(item.Price)
				if err != nil {
					fmt.Println("Error converting price to integer:", err)
					return
				}
				totalAmount += price
			}

			//completedBookings =

			//completedBookings = append(completedBookings, obj)
		}
	}

	fmt.Println("Total amount in the cart of all objects:", totalAmount)

	data := BookingSummaryResponseData{

		TotalBookings:         len(completedBookings),
		TotalMoney:            totalAmount,
		TotalUpcomingBookings: len(upcomingBookings),
		//Status:                "1",
	}

	// data := Data{

	// 	Partner_ID: storedUser.ID.Hex(),
	// 	Partner:    retrievedPartner,
	// 	Token:      token,
	// }

	responseBody := BookingSummaryResponse{
		Message: "Booking Summary Successful",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	//fmt.Println("response body", responseBody)

	// Respond with the retrieved bookings or message in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

	//fmt.Fprintf(w, "Number of bookings made in day %d", len(filteredBookings))
	//fmt.Fprintf(w, "amount of money made in day %d", totalAmount)
}

// func handleCompleteBooking(w http.ResponseWriter, r *http.Request) {
// 	// Get ID from URL params
// 	//vars := mux.Vars(r)

// 	type CompletedBookingResponse struct {
// 		Message string `json:"message"`
// 		//Token   string  `json:"token,omitempty"`
// 		Booking BookingObject `json:"booking"`
// 		Status  string        `json:"status"`
// 	}

// 	BookingID := r.URL.Query().Get("id")

// 	// Convert the facility ID to a MongoDB ObjectID
// 	// objID, err := primitive.ObjectIDFromHex(BookingID)
// 	// if err != nil {
// 	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
// 	// 	return
// 	// }

// 	// Define filter to find document by ID

// 	// Define update to set status to YES
// 	update := bson.M{"$set": bson.M{"status": "COMPLETED"}}

// 	//fmt.Println("update Data:-->", updateData)

// 	// Update the status of the record at the specified index in MongoDB
// 	collection := client.Database(dbName).Collection(booking_collection)
// 	filter := bson.M{"booking_id": BookingID}

// 	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
// 	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

// 	// Check if an error occurred during the update
// 	if updatedDoc.Err() != nil {
// 		// Handle error
// 		// ...

// 		// Return error response
// 		responseBody := ErrorResponse{
// 			Message: "Error updating record",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		fmt.Println("Error updating record:", updatedDoc.Err())
// 		return
// 	}

// 	//fmt.Println("updated doc", updatedDoc)

// 	var updated_booking BookingObject

// 	err := updatedDoc.Decode(&updated_booking)
// 	if err != nil {
// 		// Handle decoding error
// 		fmt.Println("Error decoding updated document:", err)
// 		return
// 	}

// 	// Encode and send the updated document as the response
// 	// w.Header().Set("Content-Type", "application/json")
// 	// json.NewEncoder(w).Encode(updated_partner)

// 	responseBody := CompletedBookingResponse{
// 		Message: "Booking Completed Successfully",
// 		Booking: updated_booking,
// 		Status:  "Success",
// 	}
// 	invalidatePartnerCache()

// 	// Encode the updated document as JSON and send in response
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

// func handleCompleteBooking(w http.ResponseWriter, r *http.Request) {
// 	type CompletedBookingResponse struct {
// 		Message string        `json:"message"`
// 		Booking BookingObject `json:"booking"`
// 		Status  string        `json:"status"`
// 	}

// 	// Get Booking ID from URL params
// 	BookingID := r.URL.Query().Get("id")
// 	if BookingID == "" {
// 		responseBody := ErrorResponse{
// 			Message: "Booking ID is required",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	// Get the current date and time
// 	currentTime := ConvertToNairobiTime()

// 	// Define the filter to find the document by Booking ID
// 	filter := bson.M{"booking_id": BookingID}

// 	// Define the update to set status to "COMPLETED" and completed_date to the current time
// 	update := bson.M{
// 		"$set": bson.M{
// 			"status":         "COMPLETED",
// 			"completed_date": currentTime,
// 		},
// 	}

// 	// Set options to return the updated document
// 	findOneAndUpdateOptions := options.FindOneAndUpdate().SetReturnDocument(options.After)

// 	// Get the booking collection
// 	collection := client.Database(dbName).Collection(booking_collection)

// 	// Update the booking document
// 	var updated_booking BookingObject
// 	err := collection.FindOneAndUpdate(context.Background(), filter, update, findOneAndUpdateOptions).Decode(&updated_booking)
// 	if err != nil {
// 		responseBody := ErrorResponse{
// 			Message: "Error updating record: " + err.Error(),
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		fmt.Println("Error updating record:", err)
// 		return
// 	}

// 	// Prepare the response body
// 	responseBody := CompletedBookingResponse{
// 		Message: "Booking Completed Successfully",
// 		Booking: updated_booking,
// 		Status:  "Success",
// 	}

// 	// Invalidate cache if necessary
// 	invalidatePartnerCache()

// 	// Encode and send the response
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

func handleCompleteBooking(w http.ResponseWriter, r *http.Request) {
	type CompletedBookingResponse struct {
		Message string        `json:"message"`
		Booking BookingObject `json:"booking"`
		Status  string        `json:"status"`
	}

	// Get Booking ID from URL params
	BookingID := r.URL.Query().Get("id")
	if BookingID == "" {
		responseBody := ErrorResponse{
			Message: "Booking ID is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Define the filter to find the document by Booking ID
	filter := bson.M{"booking_id": BookingID}

	// Get the booking collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Retrieve the booking document to check its current status
	var existing_booking BookingObject
	err := collection.FindOne(context.Background(), filter).Decode(&existing_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error fetching record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error fetching record:", err)
		return
	}

	// Check if the booking is already completed
	if existing_booking.Status == "COMPLETED" {
		responseBody := ErrorResponse{
			Message: "Booking is already completed",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Get the current date and time
	currentTime := ConvertToNairobiTime()

	// Define the update to set status to "COMPLETED" and completed_date to the current time
	update := bson.M{
		"$set": bson.M{
			"status":         "COMPLETED",
			"completed_date": currentTime,
		},
	}

	// Set options to return the updated document
	findOneAndUpdateOptions := options.FindOneAndUpdate().SetReturnDocument(options.After)

	// Update the booking document
	var updated_booking BookingObject
	err = collection.FindOneAndUpdate(context.Background(), filter, update, findOneAndUpdateOptions).Decode(&updated_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error updating record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", err)
		return
	}

	// Prepare the response body
	responseBody := CompletedBookingResponse{
		Message: "Booking Completed Successfully",
		Booking: updated_booking,
		Status:  "Success",
	}

	// Invalidate cache if necessary
	invalidatePartnerCache()

	// Encode and send the response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

func handleStartJob(w http.ResponseWriter, r *http.Request) {
	type CompletedBookingResponse struct {
		Message string        `json:"message"`
		Booking BookingObject `json:"booking"`
		Status  string        `json:"status"`
	}

	// Get Booking ID from URL params
	BookingID := r.URL.Query().Get("booking_id")
	if BookingID == "" {
		responseBody := ErrorResponse{
			Message: "Booking ID is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// Get the current date and time
	//currentTime := ConvertToNairobiTime()

	// Define the filter to find the document by Booking ID
	filter := bson.M{"booking_id": BookingID}

	// Define the update to set status to "COMPLETED" and completed_date to the current time
	update := bson.M{
		"$set": bson.M{
			"status": "ONGOING",
			//"completed_date": currentTime,
		},
	}

	// Set options to return the updated document
	findOneAndUpdateOptions := options.FindOneAndUpdate().SetReturnDocument(options.After)

	// Get the booking collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Update the booking document
	var updated_booking BookingObject
	err := collection.FindOneAndUpdate(context.Background(), filter, update, findOneAndUpdateOptions).Decode(&updated_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error updating record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", err)
		return
	}

	// Prepare the response body
	responseBody := CompletedBookingResponse{
		Message: "Job Started Successfully",
		Booking: updated_booking,
		Status:  "Success",
	}

	// Invalidate cache if necessary
	invalidatePartnerCache()

	// Encode and send the response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

// /GET UPCOMING BOOKINGS
func handleGet_PartnerUpcomingBookings(w http.ResponseWriter, r *http.Request) {
	// Define the response structure
	type UpcomingBookingsResponse struct {
		Upcoming_Bookings []BookingObject `json:"upcoming_bookings"`
		//Partner           Partner         `json:"partner"`
		Status string `json:"status"`
	}

	type BookingStatusResponseEmpty struct {
		Bookings string `json:"bookings,omitempty"`
		//Partner  Partner `json:"partner,omitempty"`
		Status string `json:"status,omitempty"`
	}

	fmt.Println("Get admin by ID has been called....")

	// Extract the user ID from the query parameters
	userID := r.URL.Query().Get("id")

	fmt.Println("Passed user ID:", userID)

	// Access the bookings collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	filter := bson.M{
		"provider_id": userID,
		"status":      "UPCOMING",
	}

	//filter := bson.M{"isFeatured": "YES"}

	// Define options to sort by timestamp
	options := options.Find().SetSort(bson.D{{"timestamp", 1}})

	// Find documents
	cursor, err := collection.Find(context.Background(), filter, options)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())

	// Slice to store the found bookings
	var bookings []BookingObject

	// Iterate over the cursor and decode each document into a BookingObject
	for cursor.Next(context.Background()) {
		var result BookingObject
		if err := cursor.Decode(&result); err != nil {
			log.Fatal(err)
		}
		bookings = append(bookings, result)
	}
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	// retrievedPartner, err := RetrievePartnerByID(userID, client, dbName, partner_collection)
	// if err != nil {
	// 	responseBody := ErrorResponse{
	// 		Message: "Partner not found",
	// 		Status:  "Success",
	// 	}

	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)
	// }

	// If there are no bookings, set the bookings array to an empty array
	if len(bookings) == 0 {

		//BookingStatusResponse.Bookings = []BookingObject{}

		// responseBody := UpcomingBookingsResponse{
		// 	Upcoming_Bookings: []BookingObject{},
		// 	//Partner:           retrievedPartner,
		// 	Status: "1",
		// 	// other fields...
		// }

		// responseBody := map[string]interface{}{
		// 	"message": "Retrieved Featured Partner Successfully",
		// 	"status":  "Success",
		// 	"data": map[string]interface{}{
		// 		"featured_partners": partners, // Include the whole object in the response
		// 	},
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Partner Upcoming Bookings Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"upcoming_bookings": []BookingObject{}, // Include the whole object in the response
			},
		}

		fmt.Println("response body", responseBody)

		//	Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	} else {

		// responseBody := UpcomingBookingsResponse{
		// 	Upcoming_Bookings: bookings,
		// 	//Partner:           retrievedPartner,
		// 	Status: "1",
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Partner Upcoming Bookings Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"upcoming_bookings": bookings, // Include the whole object in the response
			},
		}

		//fmt.Println("response body", responseBody)

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

}

// handle customer upcoming bookings
func handleGet_CustomerUpcomingBookings(w http.ResponseWriter, r *http.Request) {
	// Define the response structure
	type UpcomingBookingsResponse struct {
		Upcoming_Bookings []BookingObject `json:"upcoming_bookings"`
		//Partner           Partner         `json:"partner"`
		Status string `json:"status"`
	}

	type BookingStatusResponseEmpty struct {
		Bookings string `json:"bookings,omitempty"`
		//Partner  Partner `json:"partner,omitempty"`
		Status string `json:"status,omitempty"`
	}

	fmt.Println("Get admin by ID has been called....")

	// Extract the user ID from the query parameters
	userID := r.URL.Query().Get("id")

	fmt.Println("Passed user ID:", userID)

	// Access the bookings collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	filter := bson.M{
		"customer_id": userID,
		"status":      "UPCOMING",
	}

	//filter := bson.M{"isFeatured": "YES"}

	// Define options to sort by timestamp
	options := options.Find().SetSort(bson.D{{"timestamp", 1}})

	// Find documents
	cursor, err := collection.Find(context.Background(), filter, options)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())

	// Slice to store the found bookings
	var bookings []BookingObject

	// Iterate over the cursor and decode each document into a BookingObject
	for cursor.Next(context.Background()) {
		var result BookingObject
		if err := cursor.Decode(&result); err != nil {
			log.Fatal(err)
		}
		bookings = append(bookings, result)
	}
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	// retrievedPartner, err := RetrievePartnerByID(userID, client, dbName, partner_collection)
	// if err != nil {
	// 	responseBody := ErrorResponse{
	// 		Message: "Partner not found",
	// 		Status:  "Success",
	// 	}

	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)
	// }

	// If there are no bookings, set the bookings array to an empty array
	if len(bookings) == 0 {

		//BookingStatusResponse.Bookings = []BookingObject{}

		// responseBody := UpcomingBookingsResponse{
		// 	Upcoming_Bookings: []BookingObject{},
		// 	//Partner:           retrievedPartner,
		// 	Status: "1",
		// 	// other fields...
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Customer Upcoming Bookings Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"upcoming_bookings": []BookingObject{}, // Include the whole object in the response
			},
		}

		fmt.Println("response body", responseBody)

		//	Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	} else {

		// responseBody := UpcomingBookingsResponse{
		// 	Upcoming_Bookings: bookings,
		// 	//Partner:           retrievedPartner,
		// 	Status: "1",
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Customer Upcoming Bookings Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"upcoming_bookings": bookings, // Include the whole object in the response
			},
		}

		//fmt.Println("response body", responseBody)

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

}

// handle get customer Cancelled bookings
func handleGet_CustomerCancelledBookings(w http.ResponseWriter, r *http.Request) {
	// Define the response structure
	type UpcomingBookingsResponse struct {
		Upcoming_Bookings []BookingObject `json:"upcoming_bookings"`
		//Partner           Partner         `json:"partner"`
		Status string `json:"status"`
	}

	type BookingStatusResponseEmpty struct {
		Bookings string `json:"bookings,omitempty"`
		//Partner  Partner `json:"partner,omitempty"`
		Status string `json:"status,omitempty"`
	}

	fmt.Println("Get admin by ID has been called....")

	// Extract the user ID from the query parameters
	userID := r.URL.Query().Get("id")

	fmt.Println("Passed user ID:", userID)

	// Access the bookings collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	filter := bson.M{
		"customer_id": userID,
		"status":      "CANCELLED",
	}

	//filter := bson.M{"isFeatured": "YES"}

	// Define options to sort by timestamp
	options := options.Find().SetSort(bson.D{{"timestamp", 1}})

	// Find documents
	cursor, err := collection.Find(context.Background(), filter, options)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())

	// Slice to store the found bookings
	var bookings []BookingObject

	// Iterate over the cursor and decode each document into a BookingObject
	for cursor.Next(context.Background()) {
		var result BookingObject
		if err := cursor.Decode(&result); err != nil {
			log.Fatal(err)
		}
		bookings = append(bookings, result)
	}
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	// retrievedPartner, err := RetrievePartnerByID(userID, client, dbName, partner_collection)
	// if err != nil {
	// 	responseBody := ErrorResponse{
	// 		Message: "Partner not found",
	// 		Status:  "Success",
	// 	}

	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)
	// }

	// If there are no bookings, set the bookings array to an empty array
	if len(bookings) == 0 {

		//BookingStatusResponse.Bookings = []BookingObject{}

		// responseBody := UpcomingBookingsResponse{
		// 	Upcoming_Bookings: []BookingObject{},
		// 	//Partner:           retrievedPartner,
		// 	Status: "1",
		// 	// other fields...
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Customer Cancelled Bookings Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"cancelled_bookings": []BookingObject{}, // Include the whole object in the response
			},
		}

		fmt.Println("response body", responseBody)

		//	Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	} else {

		// responseBody := UpcomingBookingsResponse{
		// 	Upcoming_Bookings: bookings,
		// 	//Partner:           retrievedPartner,
		// 	Status: "1",
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Customer Cancelled Bookings Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"cancelled_bookings": bookings, // Include the whole object in the response
			},
		}

		//fmt.Println("response body", responseBody)

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

}

// handle get completed bookings
func handleGet_CustomerCompletedBookings(w http.ResponseWriter, r *http.Request) {
	// Define the response structure
	type UpcomingBookingsResponse struct {
		Upcoming_Bookings []BookingObject `json:"upcoming_bookings"`
		//Partner           Partner         `json:"partner"`
		Status string `json:"status"`
	}

	type BookingStatusResponseEmpty struct {
		Bookings string `json:"bookings,omitempty"`
		//Partner  Partner `json:"partner,omitempty"`
		Status string `json:"status,omitempty"`
	}

	fmt.Println("Get admin by ID has been called....")

	// Extract the user ID from the query parameters
	userID := r.URL.Query().Get("id")

	fmt.Println("Passed user ID:", userID)

	// Access the bookings collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	filter := bson.M{
		"customer_id": userID,
		"status":      "COMPLETED",
	}

	//filter := bson.M{"isFeatured": "YES"}

	// Define options to sort by timestamp
	options := options.Find().SetSort(bson.D{{"timestamp", 1}})

	// Find documents
	cursor, err := collection.Find(context.Background(), filter, options)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())

	// Slice to store the found bookings
	var bookings []BookingObject

	// Iterate over the cursor and decode each document into a BookingObject
	for cursor.Next(context.Background()) {
		var result BookingObject
		if err := cursor.Decode(&result); err != nil {
			log.Fatal(err)
		}
		bookings = append(bookings, result)
	}
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	// retrievedPartner, err := RetrievePartnerByID(userID, client, dbName, partner_collection)
	// if err != nil {
	// 	responseBody := ErrorResponse{
	// 		Message: "Partner not found",
	// 		Status:  "Success",
	// 	}

	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)
	// }

	// If there are no bookings, set the bookings array to an empty array
	if len(bookings) == 0 {

		//BookingStatusResponse.Bookings = []BookingObject{}

		// responseBody := UpcomingBookingsResponse{
		// 	Upcoming_Bookings: []BookingObject{},
		// 	//Partner:           retrievedPartner,
		// 	Status: "1",
		// 	// other fields...
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Customer Completed Bookings Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"completed_bookings": []BookingObject{}, // Include the whole object in the response
			},
		}

		fmt.Println("response body", responseBody)

		//	Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	} else {

		// responseBody := UpcomingBookingsResponse{
		// 	Upcoming_Bookings: bookings,
		// 	//Partner:           retrievedPartner,
		// 	Status: "1",
		// }

		responseBody := map[string]interface{}{
			"message": "Retrieved Customer Completed Bookings Successfully",
			"status":  "Success",
			"data": map[string]interface{}{
				"completed_bookings": bookings, // Include the whole object in the response
			},
		}

		//fmt.Println("response body", responseBody)

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

}

//handle Get bookings by date

func handleGetPastBookings(w http.ResponseWriter, r *http.Request) {
	//collection := client.Database(dbName).Collection(booking_collection)

	// type BookingSummaryResponse struct {
	// 	TotalBookings         int    `json:"total_Bookings"`
	// 	TotalMoney            int    `json:"total_money"`
	// 	TotalUpcomingBookings int    `json:"total_upcoming_bookings"`
	// 	Status                string `json:"status"`
	// }

	type PastBookingsResponse struct {
		PastBookings []BookingObject `json:"past_bookings"`
		// TotalMoney            int             `json:"total_money"`
		// TotalUpcomingBookings int             `json:"total_upcoming_bookings"`
		Status string `json:"status"`
	}

	type BookingSummaryResponseEmpty struct {
		Bookings string  `json:"bookings,omitempty"`
		Partner  Partner `json:"partner,omitempty"`
		Status   string  `json:"status,omitempty"`
	}

	fmt.Println("Get bookings made in the last 24 hours called")

	userID := r.URL.Query().Get("id")
	incoming_date := r.URL.Query().Get("date")

	fmt.Println("Passed user ID:", userID)

	var bookings []BookingObject
	collection := client.Database(dbName).Collection(booking_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	filter := bson.M{
		"provider_id": userID,
		// "status": bson.M{
		// 	"$in": []string{"ONGOING", "UNRATED", "PAID"},
		// },
	}

	// Find documents
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var booking BookingObject
		err := cursor.Decode(&booking)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		//fmt.Println("Booking", booking)
		bookings = append(bookings, booking)
	}

	//retrievedPartner, err := RetrievePartnerByID(client, dbName, partner_collection, objID)

	retrievedPartner, err := RetrievePartnerByID(userID, client, dbName, partner_collection)

	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error Retreiving Partner",
			Status:  "Error",
		}

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

	openingTime, closingTime, err := getTodayAvailability(retrievedPartner)

	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error retrieving today availability",
			Status:  "Error",
		}

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

	//fmt.Print("openingtime", openingTime, closingTime)

	var filteredBookings []BookingObject
	var allBookings []BookingObject

	//	var filteredBookings []BookingObject
	for _, obj := range bookings {
		createdAt, err := time.Parse("2006-01-02 15:04:05", obj.CreatedAt)

		//fmt.Println("created at", createdAt)
		if err != nil {
			fmt.Println("Error parsing time:", err)
			continue
		}

		openingTimeParsed, _ := time.Parse("03:04 PM", openingTime)
		closingTimeParsed, _ := time.Parse("03:04 PM", closingTime)

		// fmt.Println("parsed opening time", openingTimeParsed)
		// fmt.Println("parsed closing time", closingTimeParsed)

		// fmt.Println("Formated opening time", openingTimeParsed.Format("03:04 PM"))
		// fmt.Println("Formated closing time", closingTimeParsed.Format("03:04 PM"))

		// if createdAtTimeObj.After(openingTimeParsed) && createdAtTimeObj.Before(closingTimeParsed) {
		// 	fmt.Println("The time is within the opening and closing hours.", obj.CreatedAt)
		// } else {
		// 	fmt.Println("The time is outside the opening and closing hours.")
		// }

		if obj.Status != "COMPLETED" {
			allBookings = append(allBookings, obj)

		}

		// Parse string to time.Time object
		createdAtTimeObj, err := time.Parse("2006-01-02 15:04:05", obj.CreatedAt)
		if err != nil {
			fmt.Println("Error parsing createdAt:", err)
			return
		}

		incoming_dateObj, err := time.Parse("2006-01-02", incoming_date)
		if err != nil {
			fmt.Println("Error parsing createdAt:", err)
			return
		}

		fmt.Println("incoming date Year and YearDay", incoming_dateObj.Year(), incoming_dateObj.YearDay())

		// fmt.Println("Created At Year", createdAtTimeObj.Year())
		// fmt.Println("Created At YearDay", createdAtTimeObj.YearDay())

		// fmt.Println("current year", time.Now().Year())
		// fmt.Println("current year day", time.Now().YearDay())

		// Check if createdAt matches today's date

		//if createdAtTimeObj.Year() == time.Now().Year() && createdAtTimeObj.YearDay() == time.Now().YearDay() {
		if createdAtTimeObj.Year() == incoming_dateObj.Year() && createdAtTimeObj.YearDay() == incoming_dateObj.YearDay() {

			// Convert to 12-hour clock format
			fmt.Println("--->matches incoming date")
			createdAt12Hour := createdAtTimeObj.Format("03:04 PM")

			// Extract time portion
			createdAtTime := createdAt.Format("15:04:05")

			//fmt.Println("created")

			// Parse time string to time.Time object
			createdAtTimeObj2, _ := time.Parse("15:04:05", createdAtTime)

			// Check if createdAt12Hour is between opening and closing time
			if createdAtTimeObj2.After(openingTimeParsed) && createdAtTimeObj2.Before(closingTimeParsed) {
				fmt.Println("The time is within the opening and closing hours:", createdAt12Hour)
				filteredBookings = append(filteredBookings, obj)
			} else {
				fmt.Println("The time is outside the opening and closing hours:", createdAt12Hour)
			}
		}

	}

	// for _, obj := range bookings {
	// 	createdAt, err := time.Parse("2006-01-02 15:04:05", obj.CreatedAt)
	// 	if err != nil {
	// 		fmt.Println("Error parsing time:", err)
	// 		continue
	// 	}

	// 	if createdAt.After(opening) && createdAt.Before(closing) {
	// 		filteredBookings = append(filteredBookings, obj)
	// 	}
	// }

	// Respond with the count

	totalAmount := 0
	for _, obj := range filteredBookings {
		// Check if the status is "finished"
		if obj.Status == "COMPLETED" {
			for _, item := range obj.Cart {
				price, err := strconv.Atoi(item.Price)
				if err != nil {
					fmt.Println("Error converting price to integer:", err)
					return
				}
				totalAmount += price
			}
		}
	}

	fmt.Println("Total amount in the cart of all objects:", totalAmount)

	// responseBody := PastBookingsResponse{
	// 	PastBookings: filteredBookings,
	// 	// TotalMoney:            totalAmount,
	// 	// TotalUpcomingBookings: len(allBookings),
	// 	Status: "1",
	// }

	if len(filteredBookings) == 0 {

		//BookingStatusResponse.Bookings = []BookingObject{}

		responseBody := PastBookingsResponse{
			PastBookings: []BookingObject{},
			Status:       "1",
			// other fields...
		}

		fmt.Println("response body", responseBody)

		//	Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	} else {

		responseBody := PastBookingsResponse{
			PastBookings: filteredBookings,

			Status: "1",
		}

		//fmt.Println("response body", responseBody)

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

	//fmt.Println("response body", responseBody)

	// // Respond with the retrieved bookings or message in JSON format
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(responseBody)

	//fmt.Fprintf(w, "Number of bookings made in day %d", len(filteredBookings))
	//fmt.Fprintf(w, "amount of money made in day %d", totalAmount)
}

// Provider Earnings
func handleGetProviderEarnings(w http.ResponseWriter, r *http.Request) {

	type MonthlyEarnings struct {
		January_Earnings   int `json:"Jan"`
		February_Earnings  int `json:"Feb"`
		March_Earnings     int `json:"Mar"`
		April_Earnings     int `json:"Apr"`
		May_Earnings       int `json:"May"`
		June_Earnings      int `json:"June"`
		July_Earnings      int `json:"July"`
		August_Earnings    int `json:"Aug"`
		September_Earnings int `json:"Sept"`
		October_Earnings   int `json:"Oct"`
		November_Earnings  int `json:"Nov"`
		December_Earnings  int `json:"Dec"`
		// TotalMoney            int             `json:"total_money"`
		// TotalUpcomingBookings int             `json:"total_upcoming_bookings"`
	}

	type MonthlyEarningsResponse struct {
		MonthlyEarnings MonthlyEarnings `json:"monthly_earnings"`
		Status          string          `json:"status,omitempty"`
	}

	type BookingSummaryResponseEmpty struct {
		Bookings string  `json:"bookings,omitempty"`
		Partner  Partner `json:"partner,omitempty"`
		Status   string  `json:"status,omitempty"`
	}

	fmt.Println("Get bookings made in the last 24 hours called")

	userID := r.URL.Query().Get("id")
	//incoming_date := r.URL.Query().Get("date")
	incoming_year_str := r.URL.Query().Get("year")

	fmt.Println("Passed user ID:", userID)

	var bookings []BookingObject
	collection := client.Database(dbName).Collection(booking_collection)

	// Define filter to find documents with the given provider_id and status "ONGOING", "UNRATED", or "PAID"
	filter := bson.M{
		"provider_id": userID,
		// "status": bson.M{
		// 	"$in": []string{"ONGOING", "UNRATED", "PAID"},
		// },
	}

	// Find documents
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var booking BookingObject
		err := cursor.Decode(&booking)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		//fmt.Println("Booking", booking)
		bookings = append(bookings, booking)
	}

	//retrievedPartner, err := RetrievePartnerByID(client, dbName, partner_collection, objID)

	//retrievedPartner, err := RetrievePartnerByID(userID, client, dbName, partner_collection)

	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error Retreiving Partner",
			Status:  "Error",
		}

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

	//openingTime, closingTime, err := getTodayAvailability(retrievedPartner)

	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error retrieving today availability",
			Status:  "Error",
		}

		// Respond with the retrieved bookings or message in JSON format
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)

	}

	//fmt.Print("openingtime", openingTime, closingTime)

	//var filteredBookings []BookingObject

	var January_Bookings []BookingObject
	var February_Bookings []BookingObject
	var March_Bookings []BookingObject
	var April_Bookings []BookingObject
	var May_Bookings []BookingObject
	var June_Bookings []BookingObject
	var July_Bookings []BookingObject
	var August_Bookings []BookingObject
	var September_Bookings []BookingObject
	var October_Bookings []BookingObject
	var November_Bookings []BookingObject
	var December_Bookings []BookingObject
	//var allBookings []BookingObject

	//	var filteredBookings []BookingObject
	for _, obj := range bookings {
		//createdAt, err := time.Parse("2006-01-02 15:04:05", obj.CreatedAt)

		//fmt.Println("created at", createdAt)
		if err != nil {
			fmt.Println("Error parsing time:", err)
			continue
		}

		// Parse string to time.Time object
		createdAtTimeObj, err := time.Parse("2006-01-02 15:04:05", obj.CreatedAt)
		if err != nil {
			fmt.Println("Error parsing createdAt:", err)
			return
		}

		// incoming_dateObj, err := time.Parse("2006-01-02", incoming_date)
		// if err != nil {
		// 	fmt.Println("Error parsing createdAt:", err)
		// 	return
		// }

		incoming_year, err := strconv.Atoi(incoming_year_str)
		if err != nil {
			fmt.Println("Error parsing year:", err)
			return
		}

		//fmt.Println("incoming date Year and YearDay", incoming_dateObj.Year(), incoming_dateObj.YearDay())

		// fmt.Println("Created At Year", createdAtTimeObj.Year())
		// fmt.Println("Created At YearDay", createdAtTimeObj.YearDay())

		// fmt.Println("current year", time.Now().Year())
		// fmt.Println("current year day", time.Now().YearDay())

		// Check if createdAt matches today's date

		//if createdAtTimeObj.Year() == time.Now().Year() && createdAtTimeObj.YearDay() == time.Now().YearDay() {
		//if createdAtTimeObj.Year() == incoming_dateObj.Year() && createdAtTimeObj.YearDay() == incoming_dateObj.YearDay() {
		if createdAtTimeObj.Year() == incoming_year {

			// Convert to 12-hour clock format
			fmt.Println("--->matches incoming year", int(createdAtTimeObj.Month()))

			// if createdAtTimeObj.Month() == time.January {

			// 	January_Bookings = append(January_Bookings, obj)

			// } else if condition {

			// }

			switch int(createdAtTimeObj.Month()) {
			case int(time.January):
				January_Bookings = append(January_Bookings, obj)
			case int(time.February):
				February_Bookings = append(February_Bookings, obj)
			case int(time.March):
				March_Bookings = append(March_Bookings, obj)
			case int(time.April):
				April_Bookings = append(April_Bookings, obj)
			case int(time.May):
				May_Bookings = append(May_Bookings, obj)
			case int(time.June):
				June_Bookings = append(June_Bookings, obj)
			case int(time.July):
				July_Bookings = append(July_Bookings, obj)
			case int(time.August):
				August_Bookings = append(August_Bookings, obj)
			case int(time.September):
				September_Bookings = append(September_Bookings, obj)
			case int(time.October):
				October_Bookings = append(October_Bookings, obj)
			case int(time.November):
				November_Bookings = append(November_Bookings, obj)
			case int(time.December):
				December_Bookings = append(December_Bookings, obj)

			default:
				response := map[string]string{"message": "Month not valid", "status": "0"}
				fmt.Println("error", err)
				w.Header().Set("Content-Type", "application/json")
				json.NewEncoder(w).Encode(response)
				return

			}

		}

	}

	totalAmount_Jan := CalculateTotalAmount(January_Bookings)
	totalAmount_Feb := CalculateTotalAmount(February_Bookings)
	totalAmount_Mar := CalculateTotalAmount(March_Bookings)
	totalAmount_Apr := CalculateTotalAmount(April_Bookings)
	totalAmount_May := CalculateTotalAmount(May_Bookings)
	totalAmount_Jun := CalculateTotalAmount(June_Bookings)
	totalAmount_Jul := CalculateTotalAmount(July_Bookings)
	totalAmount_Aug := CalculateTotalAmount(August_Bookings)
	totalAmount_Sep := CalculateTotalAmount(September_Bookings)
	totalAmount_Oct := CalculateTotalAmount(October_Bookings)
	totalAmount_Nov := CalculateTotalAmount(November_Bookings)
	totalAmount_Dec := CalculateTotalAmount(December_Bookings)

	responseBody := MonthlyEarnings{
		January_Earnings:   totalAmount_Jan,
		February_Earnings:  totalAmount_Feb,
		March_Earnings:     totalAmount_Mar,
		April_Earnings:     totalAmount_Apr,
		May_Earnings:       totalAmount_May,
		June_Earnings:      totalAmount_Jun,
		July_Earnings:      totalAmount_Jul,
		August_Earnings:    totalAmount_Aug,
		September_Earnings: totalAmount_Sep,
		October_Earnings:   totalAmount_Oct,
		November_Earnings:  totalAmount_Nov,
		December_Earnings:  totalAmount_Dec,

		// other fields...
	}

	responseBody2 := MonthlyEarningsResponse{
		MonthlyEarnings: responseBody,

		Status: "1",
		// other fields...
	}

	fmt.Println("response body", responseBody)

	//	Respond with the retrieved bookings or message in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody2)

	// if len(January_Bookings) == 0 {

	// 	//BookingStatusResponse.Bookings = []BookingObject{}

	// 	responseBody := MonthlyEarningsResponse{
	// 		January_Earnings: totalAmount_Jan,
	// 		Status:           "1",
	// 		// other fields...
	// 	}

	// 	fmt.Println("response body", responseBody)

	// 	//	Respond with the retrieved bookings or message in JSON format
	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)

	// } else {

	// 	responseBody := PastBookingsResponse{
	// 		PastBookings: filteredBookings,

	// 		Status: "1",
	// 	}

	// 	//fmt.Println("response body", responseBody)

	// 	// Respond with the retrieved bookings or message in JSON format
	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)

	// }

	//fmt.Println("Total amount in the cart of all objects:", totalAmount)
	//fmt.Println("Total number of bookings:", len(filteredBookings))

	// responseBody := PastBookingsResponse{
	// 	PastBookings: filteredBookings,
	// 	// TotalMoney:            totalAmount,
	// 	// TotalUpcomingBookings: len(allBookings),
	// 	Status: "1",
	// }

	// if len(filteredBookings) == 0 {

	// 	//BookingStatusResponse.Bookings = []BookingObject{}

	// 	responseBody := PastBookingsResponse{
	// 		PastBookings: []BookingObject{},
	// 		Status:       "1",
	// 		// other fields...
	// 	}

	// 	fmt.Println("response body", responseBody)

	// 	//	Respond with the retrieved bookings or message in JSON format
	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)

	// } else {

	// 	responseBody := PastBookingsResponse{
	// 		PastBookings: filteredBookings,

	// 		Status: "1",
	// 	}

	// 	//fmt.Println("response body", responseBody)

	// 	// Respond with the retrieved bookings or message in JSON format
	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)

	// }

	//fmt.Println("response body", responseBody)

	// // Respond with the retrieved bookings or message in JSON format
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(responseBody)

	//fmt.Fprintf(w, "Number of bookings made in day %d", len(filteredBookings))
	//fmt.Fprintf(w, "amount of money made in day %d", totalAmount)
}

//CANCEL BOOKING

// Deactivate customer
// func cancelBooking(w http.ResponseWriter, r *http.Request) {
// 	// Get ID from URL params
// 	//vars := mux.Vars(r)

// 	bookingID := r.URL.Query().Get("booking_id")

// 	// Define update to set status to INACTIVE
// 	update := bson.M{"$set": bson.M{"status": "CANCELLED"}}

// 	//fmt.Println("update Data:-->", updateData)

// 	// Update the status of the record at the specified index in MongoDB
// 	collection := client.Database(dbName).Collection(booking_collection)
// 	filter := bson.M{"booking_id": bookingID}

// 	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
// 	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

// 	// Check if an error occurred during the update
// 	if updatedDoc.Err() != nil {
// 		// Handle error
// 		// ...

// 		// Return error response
// 		responseBody := ErrorResponse{
// 			Message: "Error updating record",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		fmt.Println("Error updating record:", updatedDoc.Err())
// 		return
// 	}

// 	//fmt.Println("updated doc", updatedDoc)

// 	var updated_booking BookingObject

// 	err := updatedDoc.Decode(&updated_booking)
// 	if err != nil {
// 		// Handle decoding error
// 		fmt.Println("Error decoding updated document:", err)
// 		return
// 	}

// 	// Encode and send the updated document as the response
// 	// w.Header().Set("Content-Type", "application/json")
// 	// json.NewEncoder(w).Encode(updated_partner)

// 	// responseBody := CustomerRegistrationResponse{
// 	// 	Message:  "Booking  Cancelled successfully",
// 	// 	Customer: updated_booking,
// 	// 	Status:   "Success",
// 	// }

// 	data := BookingData{

// 		// Partner_ID: storedUser.ID.Hex(),
// 		BookingDetails: updated_booking,
// 		// Token:      token,
// 	}

// 	responseBody := BookingResponse{
// 		Message: "Booking  Cancelled successfully",
// 		// Partner_ID: storedUser.ID.Hex(),
// 		// Partner:    retrievedPartner,
// 		// Token:      token,
// 		Data:   data,
// 		Status: "Success",
// 	}
// 	invalidatePartnerCache()

// 	// Encode the updated document as JSON and send in response
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

func cancelBooking(w http.ResponseWriter, r *http.Request) {
	// Get ID from URL params
	bookingID := r.URL.Query().Get("booking_id")
	if bookingID == "" {
		responseBody := ErrorResponse{
			Message: "Booking ID is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Define the filter to find the document by Booking ID
	filter := bson.M{"booking_id": bookingID}

	// Get the booking collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Retrieve the booking document to check its current status
	var existing_booking BookingObject
	err := collection.FindOne(context.Background(), filter).Decode(&existing_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error fetching record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error fetching record:", err)
		return
	}

	// Check if the booking is already cancelled
	if existing_booking.Status == "CANCELLED" {
		responseBody := ErrorResponse{
			Message: "Booking is already cancelled",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Define update to set status to "CANCELLED"
	update := bson.M{"$set": bson.M{"status": "CANCELLED"}}

	// Set options to return the updated document
	findOneAndUpdateOptions := options.FindOneAndUpdate().SetReturnDocument(options.After)

	// Update the status of the record at the specified index in MongoDB
	var updated_booking BookingObject
	err = collection.FindOneAndUpdate(context.Background(), filter, update, findOneAndUpdateOptions).Decode(&updated_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error updating record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", err)
		return
	}

	// Prepare the response body
	data := BookingData{
		BookingDetails: updated_booking,
	}

	responseBody := BookingResponse{
		Message: "Booking Cancelled Successfully",
		Data:    data,
		Status:  "Success",
	}

	// Invalidate cache if necessary
	invalidatePartnerCache()

	// Encode and send the response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

//RESCHEDULE BOOKING
//CANCEL BOOKING

// Deactivate customer
// func reschedule_booking(w http.ResponseWriter, r *http.Request) {
// 	// Get ID from URL params
// 	//vars := mux.Vars(r)

// 	bookingID := r.URL.Query().Get("booking_id")
// 	// bookingTime := r.URL.Query().Get("booking_time")
// 	// bookingDate := r.URL.Query().Get("booking_date")
// 	reason := r.URL.Query().Get("reason")

// 	// Define update to set status to INACTIVE
// 	//update := bson.M{"$set": bson.M{"status": "CANCELLED"}}

// 	update := bson.M{
// 		"$set": bson.M{
// 			"reason": reason, // Set booking time to current time
// 			"status": "CANCELLED",
// 			// "booking_date": bookingDate, // Set booking date to current date
// 		},
// 	}

// 	//fmt.Println("update Data:-->", updateData)

// 	// Update the status of the record at the specified index in MongoDB
// 	collection := client.Database(dbName).Collection(booking_collection)
// 	filter := bson.M{"booking_id": bookingID}

// 	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
// 	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

// 	// Check if an error occurred during the update
// 	if updatedDoc.Err() != nil {
// 		// Handle error
// 		// ...

// 		// Return error response
// 		responseBody := ErrorResponse{
// 			Message: "Error updating record",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		fmt.Println("Error updating record:", updatedDoc.Err())
// 		return
// 	}

// 	//fmt.Println("updated doc", updatedDoc)

// 	var updated_booking BookingObject

// 	err := updatedDoc.Decode(&updated_booking)
// 	if err != nil {
// 		// Handle decoding error
// 		fmt.Println("Error decoding updated document:", err)
// 		return
// 	}

// 	// Encode and send the updated document as the response
// 	// w.Header().Set("Content-Type", "application/json")
// 	// json.NewEncoder(w).Encode(updated_partner)

// 	// responseBody := CustomerRegistrationResponse{
// 	// 	Message:  "Booking  Cancelled successfully",
// 	// 	Customer: updated_booking,
// 	// 	Status:   "Success",
// 	// }

// 	data := BookingData{

// 		// Partner_ID: storedUser.ID.Hex(),
// 		BookingDetails: updated_booking,
// 		// Token:      token,
// 	}

// 	responseBody := BookingResponse{
// 		Message: "Booking  Rescheduled successfully",
// 		// Partner_ID: storedUser.ID.Hex(),
// 		// Partner:    retrievedPartner,
// 		// Token:      token,
// 		Data:   data,
// 		Status: "Success",
// 	}
// 	invalidatePartnerCache()

// 	// Encode the updated document as JSON and send in response
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

func reschedule_booking(w http.ResponseWriter, r *http.Request) {
	// Get ID from URL params
	bookingID := r.URL.Query().Get("booking_id")
	reason := r.URL.Query().Get("reason")

	if bookingID == "" {
		responseBody := ErrorResponse{
			Message: "Booking ID is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Define the filter to find the document by Booking ID
	filter := bson.M{"booking_id": bookingID}

	// Get the booking collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Retrieve the booking document to check its current status
	var existing_booking BookingObject
	err := collection.FindOne(context.Background(), filter).Decode(&existing_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error fetching record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error fetching record:", err)
		return
	}

	// Check if the booking is already cancelled
	if existing_booking.Status == "CANCELLED" {
		responseBody := ErrorResponse{
			Message: "Booking is already cancelled",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Define update to set status to "CANCELLED" and add the reason
	update := bson.M{
		"$set": bson.M{
			"reason": reason,
			"status": "CANCELLED",
		},
	}

	// Set options to return the updated document
	findOneAndUpdateOptions := options.FindOneAndUpdate().SetReturnDocument(options.After)

	// Update the status of the record at the specified index in MongoDB
	var updated_booking BookingObject
	err = collection.FindOneAndUpdate(context.Background(), filter, update, findOneAndUpdateOptions).Decode(&updated_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error updating record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", err)
		return
	}

	// Prepare the response body
	data := BookingData{
		BookingDetails: updated_booking,
	}

	responseBody := BookingResponse{
		Message: "Booking Rescheduled Successfully",
		Data:    data,
		Status:  "Success",
	}

	// Invalidate cache if necessary
	invalidatePartnerCache()

	// Encode and send the response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

///filter bookings

// Handler to retrieve services filtered by subcategory
func filterBookingsByStatus(w http.ResponseWriter, r *http.Request) {
	status := r.URL.Query().Get("status")

	fmt.Println("Status", status)

	// Check if subcategory is "all"
	if status == "ALL" {

		fmt.Println("all bookings---")
		// Fetch all services from MongoDB
		var bookings []BookingObject
		collection := client.Database(dbName).Collection(booking_collection)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var booking BookingObject
			err := cursor.Decode(&booking)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			bookings = append(bookings, booking)
		}

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(bookings)
		return
	}

	// Fetch partners from MongoDB based on subcategory
	var bookings []BookingObject
	collection := client.Database(dbName).Collection(booking_collection)
	cursor, err := collection.Find(context.Background(), bson.M{"status": status})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var booking BookingObject
		err := cursor.Decode(&booking)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		bookings = append(bookings, booking)
	}

	// Respond with the retrieved services
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(bookings)
}

// Function to handle HTTP GET requests for provider earnings
// func handleGetProviderEarnings2(w http.ResponseWriter, r *http.Request) {
// 	type MonthlyEarningsResponse struct {
// 		January_Earnings   int `json:"jan_earnings"`
// 		February_Earnings  int `json:"feb_earnings"`
// 		March_Earnings     int `json:"mar_earnings"`
// 		April_Earnings     int `json:"apr_earnings"`
// 		May_Earnings       int `json:"may_earnings"`
// 		June_Earnings      int `json:"june_earnings"`
// 		July_Earnings      int `json:"july_earnings"`
// 		August_Earnings    int `json:"aug_earnings"`
// 		September_Earnings int `json:"sep_earnings"`
// 		October_Earnings   int `json:"oct_earnings"`
// 		November_Earnings  int `json:"nov_earnings"`
// 		December_Earnings  int `json:"dec_earnings"`
// 		// TotalMoney            int             `json:"total_money"`
// 		// TotalUpcomingBookings int             `json:"total_upcoming_bookings"`
// 		Status string `json:"status"`
// 	}
// 	// Parse query parameters
// 	userID := r.URL.Query().Get("id")
// 	yearStr := r.URL.Query().Get("year")
// 	year, err := strconv.Atoi(yearStr)
// 	if err != nil {
// 		http.Error(w, "Invalid year", http.StatusBadRequest)
// 		return
// 	}

// 	// Fetch relevant bookings from the database
// 	bookings, err := getBookingsByUserIDAndYear(userID, year)
// 	if err != nil {
// 		http.Error(w, "Error fetching bookings", http.StatusInternalServerError)
// 		log.Println("Error fetching bookings:", err)
// 		return
// 	}

// 	// Calculate monthly earnings
// 	monthlyEarnings := calculateMonthlyEarnings(bookings)

// 	// Construct response
// 	response := MonthlyEarningsResponse{
// 		January_Earnings:   monthlyEarnings[time.January],
// 		February_Earnings:  monthlyEarnings[time.February],
// 		March_Earnings:     monthlyEarnings[time.March],
// 		April_Earnings:     monthlyEarnings[time.April],
// 		May_Earnings:       monthlyEarnings[time.May],
// 		June_Earnings:      monthlyEarnings[time.June],
// 		July_Earnings:      monthlyEarnings[time.July],
// 		August_Earnings:    monthlyEarnings[time.August],
// 		September_Earnings: monthlyEarnings[time.September],
// 		October_Earnings:   monthlyEarnings[time.October],
// 		November_Earnings:  monthlyEarnings[time.November],
// 		December_Earnings:  monthlyEarnings[time.December],
// 		Status:             "1",
// 	}

// 	// Send response
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(response)
// }

// // Function to fetch bookings from the database based on userID and year
// func getBookingsByUserIDAndYear(userID string, year int) ([]BookingObject, error) {
// 	// Implement database query to fetch relevant bookings
// 	// Example:
// 	collection := client.Database(dbName).Collection(booking_collection)
// 	filter := bson.M{"provider_id": userID, "created_at": bson.M{"$gte": startOfYear, "$lt": endOfYear}}
// 	cursor, err := collection.Find(context.Background(), filter)
// 	// Handle errors and return bookings
// 	return nil, nil
// }

// // Function to calculate monthly earnings from bookings
// func calculateMonthlyEarnings(bookings []BookingObject) map[time.Month]int {
// 	monthlyEarnings := make(map[time.Month]int)
// 	for _, booking := range bookings {
// 		createdAtTimeObj, err := time.Parse("2006-01-02 15:04:05", booking.CreatedAt)
// 		if err != nil {
// 			log.Println("Error parsing createdAt:", err)
// 			continue
// 		}
// 		monthlyEarnings[createdAtTimeObj.Month()] += booking.Amount
// 	}
// 	return monthlyEarnings
// }

// func startBooking(w http.ResponseWriter, r *http.Request) {
// 	var booking BookingObject
// 	err := json.NewDecoder(r.Body).Decode(&booking)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusBadRequest)
// 		return
// 	}

// 	// collection := client.Database(dbName).Collection(booking_collection)

// 	collection := client.Database(dbName).Collection(booking_collection)
// 	_, err = collection.InsertOne(context.Background(), booking)

// 	if err != nil {

// 		fmt.Println("err", err)
// 		responseBody := ErrorResponse{
// 			Message: "Error inserting Partner",
// 			Status:  "Error",
// 		}
// 		//json.NewEncoder(w).Encode(response)

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	booking.CreatedAt = ConvertToNairobiTime()
// 	// result, err := collection.InsertOne(ctx, booking)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusInternalServerError)
// 		return
// 	}

// 	// data := Data{

// 	// 	Partner_ID: storedUser.ID.Hex(),
// 	// 	Partner:    retrievedPartner,
// 	// 	Token:      token,
// 	// }

// 	responseBody := RegistrationResponse{
// 		Message: "Booking Added Sucessfullt",
// 		// Partner_ID: storedUser.ID.Hex(),
// 		// Partner:    retrievedPartner,
// 		// Token:      token,
// 		//Data:   data,
// 		Status: "Success",
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

///////////////////////////////////////

func validateBooking(booking BookingObject) error {
	v := reflect.ValueOf(booking)
	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i)
		fieldType := v.Type().Field(i)

		// Skip the ID field since it's optional and will be set by the database
		if fieldType.Name == "ID" || fieldType.Name == "CreatedAt" {
			continue
		}

		// Validate string fields
		if field.Kind() == reflect.String && strings.TrimSpace(field.String()) == "" {
			return fmt.Errorf("field %s is empty", fieldType.Name)
		}

		// Validate Coordinates fields
		if field.Kind() == reflect.Struct && fieldType.Name == "ProviderLocation" {
			if field.FieldByName("Latitude").Float() == 0 || field.FieldByName("Longitude").Float() == 0 {
				return fmt.Errorf("field %s is incomplete", fieldType.Name)
			}
		}

		if field.Kind() == reflect.Struct && fieldType.Name == "CustomerLocation" {
			if field.FieldByName("Latitude").Float() == 0 || field.FieldByName("Longitude").Float() == 0 {
				return fmt.Errorf("field %s is incomplete", fieldType.Name)
			}
		}
	}
	return nil
}

func startBooking(w http.ResponseWriter, r *http.Request) {

	type BookingInsertedData struct {
		// Message    string  `json:"message"`

		Booking BookingObject `json:"booking"`

		//PartnerID string  `json:"token,omitempty"`
		// Status string `json:"status,omitempty"`
	}

	type BookingAdditionResponse struct {
		Message string `json:"message"`
		// Token      string  `json:"token"`
		// Partner    Partner `json:"partner"`
		// Partner_ID string  `json:"partner_id"`
		Data BookingInsertedData `json:"data"`

		Status string `json:"status"`
	}
	var booking BookingObject
	err := json.NewDecoder(r.Body).Decode(&booking)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Validate booking fields
	// err = validateBooking(booking)
	// if err != nil {
	// 	responseBody := ErrorResponse{
	// 		Message: "One of the Fields is missing",
	// 		Status:  "Error",
	// 	}
	// 	w.Header().Set("Content-Type", "application/json")
	// 	json.NewEncoder(w).Encode(responseBody)
	// 	return
	// }

	if err != nil {
		responseBody := ErrorResponse{
			Message: err.Error(),
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	booking.Status = "UPCOMING"
	booking.BookingID = generateAlphanumeric()

	collection := client.Database(dbName).Collection(booking_collection)
	booking.CreatedAt = ConvertToNairobiTime()
	result, err := collection.InsertOne(context.Background(), booking)

	if err != nil {
		fmt.Println("err", err)
		responseBody := ErrorResponse{
			Message: "Error inserting booking",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Retrieve the inserted booking record
	var insertedBooking BookingObject
	filter := bson.M{"_id": result.InsertedID}
	err = collection.FindOne(context.Background(), filter).Decode(&insertedBooking)
	if err != nil {
		fmt.Println("err", err)
		responseBody := ErrorResponse{
			Message: "Error retrieving inserted booking",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// responseBody := BookingAdditionResponse{
	// 	Message: "Booking Added Successfully",
	// 	Status:  "Success",
	// 	Booking: insertedBooking,
	// }

	data := BookingInsertedData{
		Booking: insertedBooking,
	}

	responseBody := BookingAdditionResponse{
		Message: "Booking Added Successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

//delete booking

func handleDeleteBookingRecord(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid facility ID", http.StatusBadRequest)
		return
	}

	// Delete the specific facility in MongoDB
	collection := client.Database(dbName).Collection(booking_collection)
	filter := bson.M{"_id": objID}

	res, err := collection.DeleteOne(context.Background(), filter)

	//result, err = collection.DeleteOne(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error deleting driver", http.StatusInternalServerError)
		return
	}

	// Check if the document was found and deleted
	if res.DeletedCount == 0 {
		// If no documents were deleted, it might mean the document with the provided ID doesn't exist
		//http.NotFound(w, r)
		response := map[string]string{"message": "booking not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with a JSON success message
	response := map[string]string{"message": "Facility deleted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//NEW BOOKING FOR MOBILE

func handleCreateNewBooking(w http.ResponseWriter, r *http.Request) {

	// type CartItem struct {
	// 	ServiceID string `json:"service_id"`
	// 	Number    string `json:"number"`
	// }

	// type Order struct {
	// 	CustomerID  string     `json:"customer_id"`
	// 	PartnerID   string     `json:"partner_id"`
	// 	BookingTime string     `json:"booking_time"`
	// 	BookingDate string     `json:"booking_date"`
	// 	Cart        []CartItem `json:"cart"`
	// }

	fmt.Println("incoming data:", r.Body)
	decoder := json.NewDecoder(r.Body)
	var inc_data Order
	err := decoder.Decode(&inc_data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("incoming id", inc_data.CustomerID)

	if inc_data.CustomerID == "" {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: Customer ID",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// Convert the user ID to a MongoDB ObjectID
	objID_customerID, err := primitive.ObjectIDFromHex(inc_data.CustomerID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	objID_partnerID, err := primitive.ObjectIDFromHex(inc_data.PartnerID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific customer from MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	var retrievedCustomer Customer
	err = collection.FindOne(context.Background(), bson.M{"_id": objID_customerID}).Decode(&retrievedCustomer)
	if err != nil {
		fmt.Println("ERR", err)
		response := map[string]string{"message": "Customer not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	fmt.Println("retrieved customer", retrievedCustomer.First_name)

	//retrieve partner from mongoDB partner collection

	// Retrieve the specific partner from MongoDB
	collection_partners := client.Database(dbName).Collection(partner_collection)
	var retrievedPartner Partner
	err = collection_partners.FindOne(context.Background(), bson.M{"_id": objID_partnerID}).Decode(&retrievedPartner)
	if err != nil {
		fmt.Println("ERR", err)
		response := map[string]string{"message": "Partner not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	fmt.Println("retrieved partner", retrievedPartner.PartnerServices)

	var bookingRecord BookingObject

	// for _, cartItem := range inc_data.Cart {
	// 	if service, exists := findServiceByID(cartItem.ServiceID, retrievedPartner.PartnerServices); exists {
	// 		fmt.Printf("ServiceID %s exists in the services array.\n", cartItem.ServiceID)
	// 		service.Number=
	// 		bookingRecord.Cart = append(bookingRecord.Cart, service)
	// 	} else {
	// 		fmt.Printf("ServiceID %s does not exist in the services array.\n", cartItem.ServiceID)
	// 	}
	// }

	for _, cartItem := range inc_data.Cart {
		if service, exists := findServiceByID(cartItem.ServiceID, retrievedPartner.PartnerServices); exists {
			fmt.Printf("ServiceID %s exists in the services array.\n", cartItem.ServiceID)
			service.Number = cartItem.Number // Update the number from the cart item
			bookingRecord.Cart = append(bookingRecord.Cart, service)
		} else {
			fmt.Printf("ServiceID %s does not exist in the services array.\n", cartItem.ServiceID)
		}
	}

	bookingRecord.BookingID = generateAlphanumeric()
	bookingRecord.Status = "DRAFT"
	bookingRecord.CustomerID = inc_data.CustomerID
	bookingRecord.CustomerName = retrievedCustomer.First_name
	bookingRecord.CustomerPhoneNumber = retrievedCustomer.PhoneNumber
	bookingRecord.CustomerEmail = retrievedCustomer.Email
	bookingRecord.CustomerLocation = retrievedCustomer.Location
	bookingRecord.ProviderName = retrievedPartner.First_name + "" + retrievedPartner.Last_name
	bookingRecord.ProviderID = retrievedPartner.ID.Hex()
	bookingRecord.ProviderPhoneNumber = retrievedPartner.PhoneNumber
	bookingRecord.ProviderEmail = retrievedPartner.Email
	bookingRecord.ProviderLocation = retrievedPartner.Location
	bookingRecord.ProviderPaymentOption = retrievedPartner.PaymentOption
	bookingRecord.ProviderServiceType = retrievedPartner.ServiceType
	bookingRecord.ProviderBusinessName = retrievedPartner.MainBusinessName
	bookingRecord.BookingDate = inc_data.BookingDate
	bookingRecord.BookingTime = inc_data.BookingTime
	// bookingRecord.Cart = customer.PendingBooking.Cart
	// bookingRecord.CreatedAt = customer.PendingBooking.CreatedAt

	//bookingRecord.Cart = []PartnerServiceObject{}
	bookingRecord.CreatedAt = ConvertToNairobiTime()

	fmt.Println("booking record", bookingRecord)

	// Insert the person object into the collection
	collection2 := client.Database(dbName).Collection(booking_collection)
	insertResult, err := collection2.InsertOne(context.Background(), bookingRecord)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("BOOKING INSERTED")

	// Extract the inserted ID
	// insertedID := insertResult

	insertedID := insertResult.InsertedID.(primitive.ObjectID)

	// Retrieve the inserted record from the database
	var insertedRecord BookingObject
	err = collection2.FindOne(context.Background(), bson.M{"_id": insertedID}).Decode(&insertedRecord)
	if err != nil {
		http.Error(w, "Failed to retrieve inserted booking", http.StatusInternalServerError)
		return
	}

	response := map[string]interface{}{
		"message": "Booking created successfully",
		"status":  "Success",
		"data":    insertedRecord, // Include the whole object in the response
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}

func getCustomerByID(url string, token string) (string, error) {
	// Create a new request using http
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	// Add the Bearer token to the request header
	req.Header.Add("Authorization", "Bearer "+token)

	// Perform the GET request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	// Return the response body as a string
	return string(body), nil
}

// Function to check if ServiceID exists in the array of services
func serviceIDExists(serviceID string, services []PartnerServiceObject) bool {
	for _, service := range services {
		if service.ServiceID == serviceID {
			return true
		}
	}
	return false
}

func findServiceByID(serviceID string, services []PartnerServiceObject) (PartnerServiceObject, bool) {
	for _, service := range services {
		if service.ServiceID == serviceID {
			return service, true
		}
	}
	return PartnerServiceObject{}, false
}

// serviceExists checks if a serviceID exists in the services array
func serviceExists(serviceID string, services []PartnerServiceObject) bool {
	for _, service := range services {
		if service.ServiceID == serviceID {
			return true
		}
	}
	return false
}

//ADD TIME TO BOOKING

func handleAddTimeToBooking_Mobile(w http.ResponseWriter, r *http.Request) {
	type CompletedBookingResponse struct {
		Message string        `json:"message"`
		Booking BookingObject `json:"booking"`
		Status  string        `json:"status"`
	}

	// Get Booking ID from URL params
	BookingID := r.URL.Query().Get("booking_id")
	BookingTime := r.URL.Query().Get("booking_time")
	BookingDate := r.URL.Query().Get("booking_date")

	if BookingID == "" {
		responseBody := ErrorResponse{
			Message: "Booking ID is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	} else if BookingTime == "" {
		responseBody := ErrorResponse{
			Message: "Booking Time is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	} else if BookingDate == "" {
		responseBody := ErrorResponse{
			Message: "Booking Date is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// Get the current date and time
	//currentTime := ConvertToNairobiTime()

	// Define the filter to find the document by Booking ID
	filter := bson.M{"booking_id": BookingID}

	// Define the update to set status to "COMPLETED" and completed_date to the current time
	update := bson.M{
		"$set": bson.M{
			"booking_time": BookingTime,
			"booking_date": BookingDate,
			//"completed_date": currentTime,
		},
	}

	// Set options to return the updated document
	findOneAndUpdateOptions := options.FindOneAndUpdate().SetReturnDocument(options.After)

	// Get the booking collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Update the booking document
	var updated_booking BookingObject
	err := collection.FindOneAndUpdate(context.Background(), filter, update, findOneAndUpdateOptions).Decode(&updated_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error updating record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", err)
		return
	}

	response := map[string]interface{}{
		"message": "Booking Time Passed successfully",
		"status":  "Success",
		"data":    updated_booking, // Include the whole object in the response
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

	// // Prepare the response body
	// responseBody := CompletedBookingResponse{
	// 	Message: "Job Started Successfully",
	// 	Booking: updated_booking,
	// 	Status:  "Success",
	// }

	// Invalidate cache if necessary
	//invalidatePartnerCache()

	// Encode and send the response
	w.Header().Set("Content-Type", "application/json")
	//json.NewEncoder(w).Encode(responseBody)
}

func handleServiceLocation(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Update Record Status!")
	// params := mux.Vars(r)
	// partnerID := params["id"]

	// Get Booking ID from URL params
	BookingID := r.URL.Query().Get("booking_id")
	Lat := r.URL.Query().Get("lat")
	Long := r.URL.Query().Get("long")
	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	//fmt.Println("record id", partnerID)

	latitude_float, err := strconv.ParseFloat(Lat, 32)
	if err != nil {
		http.Error(w, "Invalid latitude", http.StatusBadRequest)
		return
	}
	longitude_float, err := strconv.ParseFloat(Long, 32)
	if err != nil {
		http.Error(w, "Invalid longitude", http.StatusBadRequest)
		return
	}

	if BookingID == "" {
		responseBody := ErrorResponse{
			Message: "Booking ID is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	} else if Lat == "" {
		responseBody := ErrorResponse{
			Message: "latitude is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	} else if Long == "" {
		responseBody := ErrorResponse{
			Message: "longitude is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	err_godotenv := godotenv.Load()
	if err_godotenv != nil {
		log.Fatal("Error loading .env file", err_godotenv)
	}

	googleKey := os.Getenv("GOOGLE_MAPS_KEY")

	compoundCode, err := getCompoundCode(Lat, Long, googleKey) // getting compound code to use in address

	fmt.Println("generated compound code", compoundCode)

	if err != nil {
		fmt.Println("Error:", err)
		json.NewEncoder(w).Encode("Something Went Wrong with Conversion...")
		return
	}

	// Define the filter to find the document by Booking ID
	filter := bson.M{"booking_id": BookingID}

	// Construct GeoJSON point
	// point := bson.M{
	// 	"type":        "Point",
	// 	"coordinates": []float64{longitude, latitude},
	// }

	// Define the update operation to update multiple fields
	// update := bson.M{
	// 	"$set": bson.M{
	// 		"location.latitude":  latitude,
	// 		"location.longitude": longitude,
	// 		"location.address":   updateData.Address,
	// 		"location_point":     point, // Add location_point field with GeoJSON point
	// 	},
	// }

	// Define the update to set status to "COMPLETED" and completed_date to the current time
	update := bson.M{
		"$set": bson.M{
			"service_location.latitude":  latitude_float,
			"service_location.longitude": longitude_float,
			"service_location.address":   compoundCode,
			//"completed_date": currentTime,
		},
	}

	// Set options to return the updated document
	findOneAndUpdateOptions := options.FindOneAndUpdate().SetReturnDocument(options.After)

	// Get the booking collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Update the booking document
	var updated_booking BookingObject
	err_1 := collection.FindOneAndUpdate(context.Background(), filter, update, findOneAndUpdateOptions).Decode(&updated_booking)
	if err_1 != nil {
		responseBody := ErrorResponse{
			Message: "Error updating record: " + err_1.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", err)
		return
	}

	response := map[string]interface{}{
		"message": "Service Location Added successfully",
		"status":  "Success",
		"data":    updated_booking, // Include the whole object in the response
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}

// getCompoundCode fetches the compound code from the Google Maps Geocoding API response.
func getCompoundCode(latitude, longitude, apiKey string) (string, error) {

	// GeocodeResponse represents the structure of the response from the Google Maps Geocoding API.
	type GeocodeResponse struct {
		PlusCode struct {
			CompoundCode string `json:"compound_code"`
			GlobalCode   string `json:"global_code"`
		} `json:"plus_code"`
	}
	baseURL := "https://maps.googleapis.com/maps/api/geocode/json"
	latlng := fmt.Sprintf("%s,%s", latitude, longitude)

	// Construct the URL with the query parameters
	params := url.Values{}
	params.Add("latlng", latlng)
	params.Add("key", apiKey)
	constructedURL := fmt.Sprintf("%s?%s", baseURL, params.Encode())

	// Make the HTTP GET request
	resp, err := http.Get(constructedURL)
	if err != nil {
		return "", fmt.Errorf("failed to make request: %v", err)
	}
	defer resp.Body.Close()

	// Read and parse the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read response body: %v", err)
	}

	var geocodeResponse GeocodeResponse
	if err := json.Unmarshal(body, &geocodeResponse); err != nil {
		return "", fmt.Errorf("failed to parse response body: %v", err)
	}

	// Return the compound code
	return geocodeResponse.PlusCode.CompoundCode, nil
}

func handleAddBookingPaymentOption(w http.ResponseWriter, r *http.Request) {
	type CompletedBookingResponse struct {
		Message string        `json:"message"`
		Booking BookingObject `json:"booking"`
		Status  string        `json:"status"`
	}

	// Get Booking ID from URL params
	BookingID := r.URL.Query().Get("booking_id")
	PaymentOption := r.URL.Query().Get("payment_option")

	if BookingID == "" {
		responseBody := ErrorResponse{
			Message: "Booking ID is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	} else if PaymentOption == "" {
		responseBody := ErrorResponse{
			Message: "Booking Time is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}

	// Get the current date and time
	//currentTime := ConvertToNairobiTime()

	// Define the filter to find the document by Booking ID
	filter := bson.M{"booking_id": BookingID}

	// Define the update to set status to "COMPLETED" and completed_date to the current time
	update := bson.M{
		"$set": bson.M{
			"booking_payment_option": PaymentOption,
			//"booking_date":           BookingDate,
			//"completed_date": currentTime,
		},
	}

	// Set options to return the updated document
	findOneAndUpdateOptions := options.FindOneAndUpdate().SetReturnDocument(options.After)

	// Get the booking collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Update the booking document
	var updated_booking BookingObject
	err := collection.FindOneAndUpdate(context.Background(), filter, update, findOneAndUpdateOptions).Decode(&updated_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error updating record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", err)
		return
	}

	response := map[string]interface{}{
		"message": "Booking Payment Option Set successfully",
		"status":  "Success",
		"data":    updated_booking, // Include the whole object in the response
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}

// confirm booking
func handleConfirmBooking(w http.ResponseWriter, r *http.Request) {
	type CompletedBookingResponse struct {
		Message string        `json:"message"`
		Booking BookingObject `json:"booking"`
		Status  string        `json:"status"`
	}

	// Get Booking ID from URL params
	BookingID := r.URL.Query().Get("booking_id")
	//BookingTime := r.URL.Query().Get("booking_time")
	//BookingDate := r.URL.Query().Get("booking_date")

	if BookingID == "" {
		responseBody := ErrorResponse{
			Message: "Booking ID is required",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	// Get the current date and time
	//currentTime := ConvertToNairobiTime()

	// Define the filter to find the document by Booking ID
	filter := bson.M{"booking_id": BookingID}

	// Define the update to set status to "COMPLETED" and completed_date to the current time
	update := bson.M{
		"$set": bson.M{
			"status": "UPCOMING",
			// "booking_date": BookingDate,
			//"completed_date": currentTime,
		},
	}

	// Set options to return the updated document
	findOneAndUpdateOptions := options.FindOneAndUpdate().SetReturnDocument(options.After)

	// Get the booking collection
	collection := client.Database(dbName).Collection(booking_collection)

	// Update the booking document
	var updated_booking BookingObject
	err := collection.FindOneAndUpdate(context.Background(), filter, update, findOneAndUpdateOptions).Decode(&updated_booking)
	if err != nil {
		responseBody := ErrorResponse{
			Message: "Error updating record: " + err.Error(),
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", err)
		return
	}

	response := map[string]interface{}{
		"message": "Booking Confirmed Successfully",
		"status":  "Success",
		"data":    updated_booking, // Include the whole object in the response
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
	// Encode and send the response
	// w.Header().Set("Content-Type", "application/json")
	//json.NewEncoder(w).Encode(responseBody)
}
