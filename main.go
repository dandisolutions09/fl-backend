package main

import (
	///"FL-backend/internal/auth"
	//"FL-backend/internal/server"
	//"fmt"

	"encoding/json"
	"fmt"
	"os"

	//"os"

	//"os"

	//"os"
	//new changes
	"time"

	//"io/ioutil"

	//"html/template"
	"context"
	"log"
	"net/http"

	//"FL-backend/internal/partner"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"

	//"github.com/joho/godotenv"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/google"
	"github.com/patrickmn/go-cache"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/joho/godotenv"
)

var client *mongo.Client
var dbName = "fl_db"
var partner_collection = "partner_collection"
var partner_collection2 = "partner_collection2"
var admin_collection = "admin_collection"
var services_collection = "services_collection"
var services_2_collection = "services_2_collection"
var logs_collection = "logs_collection"

var customer_collection = "customer_collection"

var customer_collection_mock = "customer_collection_mock"

var booking_collection = "booking_collection"

var categories_collection = "categories_collection"

var blacklist_collection = "blacklist_collection"

// func createIndexes() {
// 	collection := client.Database(dbName).Collection(partner_collection)

// 	indexModel := mongo.IndexModel{
// 		Keys:    bson.D{{"location", "2dsphere"}},
// 		Options: options.Index().SetName("location_2dsphere"),
// 	}

// 	_, err := collection.Indexes().CreateOne(context.Background(), indexModel)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("2dsphere index created!")
// }

func init() {

	err_godotenv := godotenv.Load()
	if err_godotenv != nil {
		log.Fatal("Error loading .env file", err_godotenv)
	}

	// // Get the environment variables
	username := os.Getenv("MONGO_INITDB_ROOT_USERNAME")
	password := os.Getenv("MONGO_INITDB_ROOT_PASSWORD")
	host := os.Getenv("MONGO_HOST")
	port := os.Getenv("MONGO_PORT")
	//googleKey := os.Getenv("GOOGLE_MAPS_KEY")

	mongoURI := fmt.Sprintf("mongodb://%s:%s@%s:%s", username, password, host, port)

	//var mongoURI = "mongodb://localhost:27017/"
	var err error

	// Set up MongoDB client
	clientOptions := options.Client().ApplyURI(mongoURI)

	client, err = mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		fmt.Println("Error connecting to MongoDB:", err)
		return
	}

	// Check the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		fmt.Println("Error pinging MongoDB:", err)
		return
	}

	fmt.Println("Connected to MongoDB!!")

	db := client.Database(dbName)

	collections, err := db.ListCollectionNames(context.Background(), bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("collections", collections)

	// Check if "services" collection exists
	servicesCollectionExists := false
	for _, col := range collections {
		if col == "services_collection" {
			servicesCollectionExists = true
			break
		}
	}

	if servicesCollectionExists {
		fmt.Println("Services collection exists")
	} else {
		fmt.Println("Services collection does not exist")

	}

	partnersCache = cache.New(5*time.Minute, 10*time.Minute)
	// Call initializePartnersStatus
	// err_ := initializePartnersStatus()
	// if err_ != nil {
	// 	fmt.Println("Error:", err_)
	// }

	// // if err := initializePartnersStatus(); err != nil {
	// // 	log.Fatal("Error initializing partners status:", err)
	// // }

	// if err := initializeCustomerStatus(); err != nil {
	// 	log.Fatal("Error initializing partners status:", err)
	// }
	invalidatePartnerCache()

	///initializeCustomerStatus

	//createIndexes()

	//fmt.Println("Image compression and decompression successful.")

}

func main() {

	key := "Secret-session-key" // Replace with your SESSION_SECRET or similar
	maxAge := 86400 * 30        // 30 days
	isProd := false             // Set to true when serving over https

	store := sessions.NewCookieStore([]byte(key))
	store.MaxAge(maxAge)
	store.Options.Path = "/"
	store.Options.HttpOnly = true // HttpOnly should always be enabled
	store.Options.Secure = isProd

	gothic.Store = store

	goth.UseProviders(
		google.New("416299411529-tnn656kd00sacg05hpl1ic6hcsfqbarg.apps.googleusercontent.com", "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", "http://localhost:3000/auth/google/callback", "email", "profile"),
	)

	router := mux.NewRouter()

	// Use the handlers.CORS middleware to handle CORS
	corsHandler := handlers.CORS(
		handlers.AllowedHeaders([]string{"Content-Type", "Authorization"}),
		handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}),
	)

	//googletest()

	//router.HandleFunc("/get-userbyid/{id}", handleGetUserByID).Methods("GET")

	router.HandleFunc("/auth/{provider}/callback", func(res http.ResponseWriter, req *http.Request) {

		user, err := gothic.CompleteUserAuth(res, req)
		if err != nil {
			fmt.Fprintln(res, err)
			return
		}
		//t, _ := template.New("foo").Parse(userTemplate)
		//t.Execute(res, user)
		fmt.Print(user)
		json.NewEncoder(res).Encode(user)
	})

	router.HandleFunc("/auth/{provider}", func(res http.ResponseWriter, req *http.Request) {
		gothic.BeginAuthHandler(res, req)
	})

	//addpartner := partner.handleAddPartner()

	router.HandleFunc("/login", handleLogin).Methods("POST")

	router.HandleFunc("/health", handleLogin).Methods("POST")

	router.HandleFunc("/add-partner", RegisterPartner).Methods("POST")
	// router.HandleFunc("/add-partner2", RegisterPartner2).Methods("POST")

	router.HandleFunc("/add-admin", RegisterAdmin).Methods("POST")
	router.HandleFunc("/admin-login", handleAdminLogin).Methods("POST")

	router.HandleFunc("/get-user-by-email/{email}", handleGetUserByEmail).Methods("GET")
	router.HandleFunc("/get-admin-by-email/{email}", handleGetAdminByEmail).Methods("GET")
	router.HandleFunc("/get-customer-by-email/{email}", handleGetCustomerByEmail).Methods("GET")

	// Applying authentication middleware to multiple routes
	authRouter := router.PathPrefix("").Subrouter()
	authRouter.Use(authenticate)

	//test

	authRouter.HandleFunc("/get-partners", handleGetPartnersCached).Methods("GET")
	authRouter.HandleFunc("/get-active-partners", handleGetActivePartners).Methods("GET")

	authRouter.HandleFunc("/get-userid/{userId}", handleGetUserByUserId).Methods("GET")
	authRouter.HandleFunc("/add-businessName/{id}", handleAddBusinessName).Methods("PUT")
	authRouter.HandleFunc("/add-businessLogo/{id}", handleAddBusinessLogo).Methods("PUT")
	authRouter.HandleFunc("/add-booking-preference/{id}", handleAddBookingPreference).Methods("PUT")
	authRouter.HandleFunc("/add-location/{id}", handleAddLocation).Methods("PUT")
	authRouter.HandleFunc("/add-customer-location/{id}", handleAddCustomerLocation).Methods("PUT")

	//handleAddCustomerLocation
	authRouter.HandleFunc("/add-socialmedia/{id}", handleAddSocialMedia).Methods("PUT")
	authRouter.HandleFunc("/add-availability/{id}", handleAddAvailability).Methods("PUT")
	authRouter.HandleFunc("/add-availability-dash/{id}", handleAddAvailabilityDash).Methods("PUT")

	//
	//authRouter.HandleFunc("/add-profile-pic/{id}", handleAddProfilePic).Methods("PUT")
	authRouter.HandleFunc("/update-profile-url/{id}", handleAddProfilePic).Methods("PUT")
	router.HandleFunc("/customer-reset-password/{id}", handleUpdateCustomerPassword).Methods("PUT")

	//handleUpdateCustomerPassword

	//update-profile-url

	//ADMINS
	authRouter.HandleFunc("/get-admins", handleGetAdmins).Methods("GET")
	authRouter.HandleFunc("/admin-reset-password/{id}", handleUpdateAdminPassword).Methods("PUT")
	authRouter.HandleFunc("/get-admin/{id}", handleGetAdminById).Methods("GET")
	authRouter.HandleFunc("/update-admin/{id}", handleUpdateAdmin).Methods("PUT")
	authRouter.HandleFunc("/get-partner-by-id/{id}", handleGetPartnerById).Methods("GET")
	authRouter.HandleFunc("/update-provider-info/{id}", handleUpdateProvider).Methods("PUT")
	authRouter.HandleFunc("/add-partner-services/{id}", storePartnerService).Methods("PUT")

	//AddPartnerService

	//CUSTOMER
	//authRouter.HandleFunc("/get-admins", handleGetAdmins).Methods("GET")
	//authRouter.HandleFunc("/admin-reset-password/{id}", handleUpdateAdminPassword).Methods("PUT")
	//authRouter.HandleFunc("/get-admin/{id}", handleGetAdminById).Methods("GET")
	//authRouter.HandleFunc("/update-admin/{id}", handleUpdateAdmin).Methods("PUT")

	router.HandleFunc("/add-customer", RegisterCustomer).Methods("POST")
	router.HandleFunc("/add-customer2", RegisterCustomer2).Methods("POST")

	router.HandleFunc("/get-customers-cached", handleGetPartnersCached).Methods("GET")

	router.HandleFunc("/customer-login", handleCustomerLogin).Methods("POST")
	authRouter.HandleFunc("/get-partner-by-id/{id}", handleGetPartnerById).Methods("GET")

	//handleGetCustomerId
	authRouter.HandleFunc("/get-customers", handleGetCustomers).Methods("GET")
	authRouter.HandleFunc("/get-active-customers", handleGetActiveCustomers).Methods("GET")
	authRouter.HandleFunc("/get-customer-by-id/{id}", handleGetCustomerId).Methods("GET")

	//

	authRouter.HandleFunc("/upload-customer-profile", uploadCustomerProfilePic)
	authRouter.HandleFunc("/update-customer-profile-url/{id}", handleAddCustomerProfilePicURL).Methods("PUT")
	authRouter.HandleFunc("/update-customer-info/{id}", handleUpdateCustomer).Methods("PUT")
	authRouter.HandleFunc("/update-customer-cart/{id}", storeItemToCart).Methods("PUT")

	authRouter.HandleFunc("/update-cart-customerdetails/{id}", storeCustomerDetailsToCart).Methods("PUT")
	authRouter.HandleFunc("/update-cart-provider-details/{id}", storeProviderDetailsToCart).Methods("PUT")

	authRouter.HandleFunc("/filter-customers-by-status", filterCustomersByStatus).Methods("GET")

	///storeCustomerDetailsToCart

	//authRouter.HandleFunc("/update-customer-cart/{id}", storeItemToCart).Methods("PUT")
	//authRouter.HandleFunc("/remove-from-cart/{id}", removeFromCart).Methods("PUT")
	authRouter.HandleFunc("/remove-item", removeItemFromCartHandler).Methods("PUT")
	//authRouter.HandleFunc("/add-services-to-cart/{doc_id}/{item_id}", addServiceToCart).Methods("PUT")
	//authRouter.HandleFunc("/remove-services-from-cart/{doc_id}/{item_id}/{service_id}", removeServiceFromCart).Methods("PUT")

	authRouter.HandleFunc("/add-services-to-cart", addServiceToCart).Methods("PUT")

	//
	authRouter.HandleFunc("/remove-from-cart", handleRemoveItemFromCart).Methods("PUT")

	//ADD PENDING BOOKING

	authRouter.HandleFunc("/add-pending-booking/{id}", AddPendingBooking).Methods("PUT")
	authRouter.HandleFunc("/add-service-to-booking/{id}", AddServiceToBooking).Methods("PUT")
	authRouter.HandleFunc("/get-customer-cart", handleGetCustomerCart).Methods("GET")

	//AddServiceToBooking

	//SERVICES
	router.HandleFunc("/add-service", storeService).Methods("POST")
	router.HandleFunc("/add-service-2", storeService2).Methods("POST")

	router.HandleFunc("/get-services", getServicesBySubcategory2).Methods("GET")
	router.HandleFunc("/get-sorted-services", getServicesBySubcategory2).Methods("GET")

	router.HandleFunc("/get-grouped-services", getServicesBySubcategory2_Sorted).Methods("GET")

	router.HandleFunc("/get-sorted-partner-services", getPartnerServices2).Methods("GET")

	router.HandleFunc("/get-sorted-partner-services-dash", getPartnerServices).Methods("GET")

	///TIMESLOTS

	authRouter.HandleFunc("/get-timeslots", handleGetPartnerTimeSlots).Methods("GET")

	//BOOKING TIME
	authRouter.HandleFunc("/add-booking-time", AddBookingTime).Methods("PUT")

	//GET PARTNER AVAILABILITY
	authRouter.HandleFunc("/get-partner-availability/{id}", handleGetPartnerAvailability).Methods("GET")

	//handleGetPartnerAvailability

	//AddBookingTime

	//BOOKING TIME
	router.HandleFunc("/finalize-booking", insertBookingRecord).Methods("POST")
	router.HandleFunc("/get-bookings", handleGetBookings).Methods("GET")

	//GOOGLE MAPS
	router.HandleFunc("/get-places", handleGetSuggestions).Methods("GET")

	//GET BOOKING STATUS
	authRouter.HandleFunc("/partner-booking-details", handleGetBookingStatus_Partner).Methods("GET")

	authRouter.HandleFunc("/customer-booking-details", handleGetBookingStatus_Customer).Methods("GET")

	//IMAGE UPLOAD
	authRouter.HandleFunc("/upload-profile", uploadProfilePic)
	authRouter.HandleFunc("/upload-logo", uploadLogoPic)
	//router.HandleFunc("/view", View)
	router.HandleFunc("/profile-view/{id}", handleViewProfilePic)
	router.HandleFunc("/logo-view/{id}", handleViewLogo)
	authRouter.HandleFunc("/delete-partner/{id}", handleDeletePartner)

	//IMAGE
	router.HandleFunc("/upload-pic", uploadPicCompress)
	router.HandleFunc("/test-view/{id}", handleViewTestImage)

	//LOGS
	router.HandleFunc("/create-log", handlecreateLog)
	router.HandleFunc("/get-logs", handleGetLogs)

	//ANALYTICS
	authRouter.HandleFunc("/get-stats", handleGetStatistics)

	//DEACTIVATIVATIONS
	authRouter.HandleFunc("/deactivate-partner", DeactivatePartner).Methods("PUT")
	authRouter.HandleFunc("/activate-partner", ActivatePartner).Methods("PUT")

	authRouter.HandleFunc("/activate-customer", ActivateCustomer).Methods("PUT")
	authRouter.HandleFunc("/deactivate-customer", DeactivateCustomer).Methods("PUT")

	authRouter.HandleFunc("/get-featured-partners", handleGetFeaturedPartners).Methods("GET")

	authRouter.HandleFunc("/feature-partner", handleFeaturePartner).Methods("PUT")

	//GET BOOKING SUMMARY
	authRouter.HandleFunc("/get-booking-summary", handleGetBookingsSummary).Methods("GET")

	//COMPLETE BOOKING
	authRouter.HandleFunc("/complete-booking", handleCompleteBooking).Methods("PUT")

	authRouter.HandleFunc("/start-job", handleStartJob).Methods("PUT")

	//handleStartJob

	//handleAddFeaturePartner

	//handleGetFeaturedPartners

	//${LOCAL_BASE_URL}/deactivate-customer
	authRouter.HandleFunc("/filter-partners-by-status", getPartnersByStatus).Methods("GET")

	//GET PARTNER UPCOMING BOOKINGS
	// /handleGet_PartnerUpcomingBookings
	authRouter.HandleFunc("/partner-upcoming-bookings", handleGet_PartnerUpcomingBookings).Methods("GET")

	authRouter.HandleFunc("/customer-upcoming-bookings", handleGet_CustomerUpcomingBookings).Methods("GET")

	//handleGetBookingsByDate
	authRouter.HandleFunc("/partner-past-bookings", handleGetPastBookings).Methods("GET")

	authRouter.HandleFunc("/partner-yearly-earnings", handleGetProviderEarnings).Methods("GET")

	//handleGetProviderEarnings

	//GET SERVICES
	//authRouter.HandleFunc("/get-service-category", handleGetServices).Methods("GET")

	//

	authRouter.HandleFunc("/upload-category-pic", uploadServiceCategoryPic)

	router.HandleFunc("/category-view/{id}", handleViewServiceCategoryPic)

	router.HandleFunc("/add-service-category", storeServiceCategory).Methods("POST")

	//Get all service categories

	router.HandleFunc("/get-service-categories", handleGetServiceCategories).Methods("GET")
	// router.HandleFunc("/get-child-services", handleGetChildrenServices).Methods("GET")

	router.HandleFunc("/get-child-services", getServicesBySubcategory2).Methods("GET")

	authRouter.HandleFunc("/logout", LogoutHandler).Methods("POST")

	///get nearest partners
	router.HandleFunc("/available-providers", getNearestPartners)

	//reschedule booking
	authRouter.HandleFunc("/cancel-booking", cancelBooking).Methods("PUT")

	//reschedule booking
	authRouter.HandleFunc("/reschedule-booking", reschedule_booking).Methods("PUT")

	//filter bookings by status
	authRouter.HandleFunc("/filter-bookings", filterBookingsByStatus).Methods("GET")

	//start booking for mobile

	//startBooking
	authRouter.HandleFunc("/start-booking", startBooking).Methods("POST")

	//delete partner function
	authRouter.HandleFunc("/delete-booking-record/{id}", handleDeleteBookingRecord).Methods("DELETE")

	//GRAB PARTNER TOKEN
	router.HandleFunc("/partner-social-login", handleSocialLogin).Methods("POST")

	router.HandleFunc("/customer-social-login", handleCustomerSocialLogin).Methods("POST")

	authRouter.HandleFunc("/add-phone-number/{id}", handleAddCustomerPhoneNumber).Methods("PUT")

	router.HandleFunc("/partner-reset-password/{id}", handleUpdatePartnerPassword).Methods("PUT")

	router.HandleFunc("/get-trends", handleGetTrends)

	router.HandleFunc("/events", SSEHandler)

	router.HandleFunc("/update-createdat/{id}", handleUpdateCreatedAtDate).Methods("PUT")

	//available-bookings
	authRouter.HandleFunc("/get-available-timeslots", handGetAvailableTimeslots).Methods("GET")

	//add partner Review

	authRouter.HandleFunc("/add-partner-review/{id}", handleAddPartnerReview).Methods("PUT")

	authRouter.HandleFunc("/get-partner-reviews", handleGetPartnerReviews).Methods("GET")

	authRouter.HandleFunc("/new-booking", handleCreateNewBooking).Methods("POST")

	authRouter.HandleFunc("/add-time-to-booking", handleAddTimeToBooking_Mobile).Methods("PUT")

	authRouter.HandleFunc("/add-service-location", handleServiceLocation).Methods("PUT")

	authRouter.HandleFunc("/add-booking-payment-option", handleAddBookingPaymentOption).Methods("PUT")

	authRouter.HandleFunc("/confirm-booking", handleConfirmBooking).Methods("PUT") // confirm booking endpoint

	authRouter.HandleFunc("/customer-canclled-bookings", handleGet_CustomerCancelledBookings).Methods("GET") //customer cancelled bookings

	authRouter.HandleFunc("/customer-completed-bookings", handleGet_CustomerCompletedBookings).Methods("GET") //customer cancelled bookings

	//confirm-booking

	//handleCustomerSocialLogin

	//Start the server on port 8080
	http.Handle("/", corsHandler(router))
	fmt.Println("Server listening on :8080")
	// http.ListenAndServe(":8080", nil)

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("Server failed to start: ", err)
	}

	//server := server.NewServer()
	// auth.NewAuth()

	// err := server.ListenAndServe()
	// if err != nil {
	// 	panic(fmt.Sprintf("cannot start server: %s", err))
	// }
}
