package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Logs_Struct struct {
	// gorm.Model
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	FirstName string             `json:"first_name" bson:"first_name"`
	LastName  string             `json:"last_name" bson:"last_name"`
	Email     string             `json:"email" bson:"email"`
	UserRole  string             `json:"user_role" bson:"user_role"`
	Activity  string             `json:"activity" bson:"activity"`
	CreatedAt string             `json:"created_at" bson:"created_at"`
}

func handlecreateLog(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	//fmt.Printf(decoder)
	var data Logs_Struct
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)

	data.CreatedAt = nairobiTime

	collection := client.Database(dbName).Collection(logs_collection)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting Logs", http.StatusInternalServerError)
		return
	}

	response := map[string]string{"message": "Logs inserted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleGetLogs(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(logs_collection)

	// Define a slice to store retrieved facilities
	var logs []Logs_Struct

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving nurses", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var log Logs_Struct
		if err := cursor.Decode(&log); err != nil {
			http.Error(w, "Error decoding driver", http.StatusInternalServerError)
			return
		}
		logs = append(logs, log)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(logs)
}

func insertLogs(data Logs_Struct) error {

	// Your Nairobi time conversion logic
	nairobiTime := ConvertToNairobiTime()
	fmt.Println("Nairobi Time:", nairobiTime)

	// Inserting into MongoDB
	data.CreatedAt = nairobiTime

	collection := client.Database(dbName).Collection(logs_collection)
	_, err := collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		return err
	}
	return nil
}
