package main

import (
	// "context"
	// "crypto/rand"
	// "encoding/json"
	// "fmt"
	// "io"

	// "io"

	// "io/ioutil"
	//	"net/http"
	// "os"
	"context"
	"encoding/json"
	"fmt"

	//"log"

	//"log"

	//	"log"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v5" // "github.com/gorilla/mux"
	//"github.com/patrickmn/go-cache"
	///"github.com/gorilla/mux"
	//"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	// "golang.org/x/crypto/bcrypt"
)

// type Broker_Information struct {
// 	First_name  string `json:"first_name" bson:"first_name"`
// 	Last_name   string `json:"last_name" bson:"last_name"`
// 	Email       string `json:"email" bson:"email"`
// 	PhoneNumber string `json:"phone_number" bson:"phone_number"`
// 	CreatedAt   string `json:"created_at" bson:"created_at"`
// }

type User struct {
	ID         primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	First_name string             `json:"first_name" bson:"first_name"`
	Last_name  string             `json:"last_name" bson:"last_name"`
	Username   string             `json:"username" bson:"username"`
	Email      string             `json:"email" bson:"email"`
	Password   string             `json:"password" bson:"password"`
	Salt       []byte             `json:"salt" bson:"salt"`
	Role       string             `json:"role" bson:"role"`

	Status    string `json:"status" bson:"status"`
	CreatedAt string `json:"created_at" bson:"created_at"`
}

type LogoutResponse struct {
	Message string `json:"message"`
	// Token      string  `json:"token,omitempty"`
	// Partner    Partner `json:"partner,omitempty"`
	// Partner_ID string  `json:"partner_id,omitempty"`
	//PartnerID string  `json:"token,omitempty"`
	Status string `json:"status"`
}

type TimeStamp struct {
	DateTime string `json:"date_time"`
	UTC      int    `json:"utc_time"`
}

func createToken(username string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256,
		jwt.MapClaims{
			"username": username,
			"exp":      time.Now().Add(time.Hour * 72).Unix(),
		})

	tokenString, err := token.SignedString(secretKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func verifyToken(tokenString string) error {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return secretKey, nil
	})

	if err != nil {
		return err
	}

	if !token.Valid {
		return fmt.Errorf("invalid token")

		// responseBody := ErrorResponse{
		// 	Message: "Not Authenticated with Token",
		// 	Status:  "Error",
		// }
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(responseBody)

	}

	return nil
}

// func authenticateToken(w http.ResponseWriter, r *http.Request) {
// 	w.Header().Set("Content-Type", "application/json")
// 	tokenString := r.Header.Get("Authorization")
// 	if tokenString == "" {
// 		w.WriteHeader(http.StatusUnauthorized)
// 		responseBody := ErrorResponse{
// 			Message: "Not Authenticated with Token",
// 			Status:  "Error",
// 		}
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}
// 	tokenString = tokenString[len("Bearer "):]

// 	err := verifyToken(tokenString)
// 	if err != nil {
// 		w.WriteHeader(http.StatusUnauthorized)
// 		fmt.Fprint(w, "Invalid token")
// 		return
// 	}

// 	// Proceed with further processing if token is valid
// }

func authenticate(next http.Handler) http.Handler {

	// if err := initializeCustomerStatus(); err != nil {
	// 	log.Fatal("Error initializing partners status:", err)
	// }

	// if err := initializePartnersStatus(); err != nil {
	// 	log.Fatal("Error initializing partners status:", err)
	// }

	// c = cache.New(5*time.Minute, 10*time.Minute)

	// // Call initializePartnersStatus
	// err_ := initializePartnersStatus()
	// if err_ != nil {
	// 	fmt.Println("Error:", err_)
	// }

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		tokenString := r.Header.Get("Authorization")
		if tokenString == "" {
			w.WriteHeader(http.StatusUnauthorized)
			responseBody := ErrorResponse{
				Message: "Not Authenticated with Token",
				Status:  "Error",
			}
			json.NewEncoder(w).Encode(responseBody)
			return
		}
		tokenString = tokenString[len("Bearer "):]

		filter := bson.M{"token": tokenString}

		collection := client.Database(dbName).Collection(blacklist_collection)

		// Retrieve the updated customer document
		var blacklistedToken TokenBlacklist
		err_1 := collection.FindOne(context.Background(), filter).Decode(&blacklistedToken)

		if err_1 == nil && time.Now().Before(blacklistedToken.Expiry) {
			// w.WriteHeader(http.StatusUnauthorized)

			response := LogoutResponse{
				Message: "User already logged out!",
				//Token:   "your_generated_token",
				//User:    storedUser,
				Status: "0",
			}
			json.NewEncoder(w).Encode(response)
			return
		}
		// if err_1 != nil {
		// 	fmt.Println("ERR", err_1)
		// 	http.Error(w, "Error retrieving updated Customer", http.StatusInternalServerError)
		// 	return
		// }

		err := verifyToken(tokenString)
		if err != nil {
			//w.WriteHeader(http.StatusUnauthorized)
			//fmt.Fprint(w, "Invalid token")

			responseBody := ErrorResponse{
				Message: "Token Has Expired or Invalid",
				Status:  "Error",
			}

			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(responseBody)
			return
		}

		// If token is valid, call the next handler function
		next.ServeHTTP(w, r)
	})
}
