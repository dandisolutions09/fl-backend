#!/bin/sh

# Navigate to the working directory
cd /app

# Start reflex to watch for changes and rebuild the Go application
exec reflex -r '\.go$' -s -- sh -c 'go build -o bin && ./bin'
