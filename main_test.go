package main

import (
	"bytes"
	"encoding/json"
	"reflect"

	"net/http"
	"net/http/httptest"
	"testing"
)

// Mock external request function for testing
var httpGet func(url string) (*http.Response, error)

func mockExternalRequest(url string) (*http.Response, error) {
	if httpGet != nil {
		return httpGet(url)
	}
	return nil, nil
}

func TestHandleLogin(t *testing.T) {
	// Replace http.Get with the mock function
	httpGet = mockExternalRequest

	tests := []struct {
		name               string
		requestBody        map[string]string
		expectedStatusCode int
		expectedResponse   map[string]interface{}
	}{
		{
			name: "Valid Login",
			requestBody: map[string]string{
				"email":    "test@example.com",
				"password": "password",
			},
			expectedStatusCode: http.StatusOK,
			expectedResponse: map[string]interface{}{
				"message": "Authentication successful",
				"status":  "Success",
				"data": map[string]interface{}{
					"partner_id": "",
					"partner": map[string]interface{}{
						"email":             "test@example.com",
						"is_password_reset": true,
						"id":                "",
						"salt":              []byte("mock_salt_value"), // Adjust as per your actual salt value
					},
					"token": "mock_token", // Adjust as per your actual token value
				},
			},
		},
		// {
		// 	name: "Missing Fields",
		// 	requestBody: map[string]string{
		// 		"email": "test@example.com",
		// 	},
		// 	expectedStatusCode: http.StatusBadRequest,
		// 	expectedResponse: map[string]interface{}{
		// 		"message": "Missing required fields: email, password",
		// 		"status":  "Error",
		// 	},
		// },
		// {
		// 	name: "External Request Failed",
		// 	requestBody: map[string]string{
		// 		"email":    "test@example.com",
		// 		"password": "password",
		// 	},
		// 	expectedStatusCode: http.StatusInternalServerError,
		// 	expectedResponse: map[string]interface{}{
		// 		"message": "Error making GET request",
		// 		"status":  "Error",
		// 	},
		// },
		// Add more test cases as needed
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			body, _ := json.Marshal(tt.requestBody)
			req, err := http.NewRequest("POST", "/login", bytes.NewBuffer(body))
			if err != nil {
				t.Fatal(err)
			}

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(handleLogin)

			handler.ServeHTTP(rr, req)

			if status := rr.Code; status != tt.expectedStatusCode {
				t.Errorf("handler returned wrong status code: got %v want %v",
					status, tt.expectedStatusCode)
			}

			var response map[string]interface{}
			err = json.NewDecoder(rr.Body).Decode(&response)
			if err != nil {
				t.Errorf("error decoding response body: %v", err)
			}

			// Check expected response fields
			for key, expectedValue := range tt.expectedResponse {
				if value, exists := response[key]; !exists || !reflect.DeepEqual(value, expectedValue) {
					t.Errorf("handler returned unexpected body for key %s: got %v want %v",
						key, value, expectedValue)
				}
			}
		})
	}
}
