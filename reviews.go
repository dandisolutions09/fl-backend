package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//ADD PARTNER REVIEW

func handleAddPartnerReview(w http.ResponseWriter, r *http.Request) {
	// Get the partner ID from the request parameters
	fmt.Println("Handle add partner review is called")
	params := mux.Vars(r)
	partnerID := params["id"]

	// Convert the partner ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var incomingReview PartnerReviewObject
	err = decoder.Decode(&incomingReview)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	incomingReview.CreatedAt = ConvertToNairobiTime()
	incomingReview.ReviewID = generateAlphanumeric()

	// Update the specific records in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}

	// Fetch current reviews to calculate the new average rating
	var currentPartner Partner
	err = collection.FindOne(context.Background(), filter).Decode(&currentPartner)
	if err != nil {
		fmt.Println("Error", err)
		http.Error(w, "Error fetching current reviews", http.StatusInternalServerError)
		return
	}

	// Append the new review to the current reviews
	currentPartner.PartnerReviews = append(currentPartner.PartnerReviews, incomingReview)

	// Calculate the new average rating
	totalRatings := 0.0
	for _, review := range currentPartner.PartnerReviews {
		rating, err := strconv.ParseFloat(review.Rating, 64)
		if err != nil {
			http.Error(w, "Error parsing rating", http.StatusInternalServerError)
			return
		}
		totalRatings += rating
	}
	newAverageRating := totalRatings / float64(len(currentPartner.PartnerReviews))
	totalNumberOfRatings := len(currentPartner.PartnerReviews)

	totalNumberOfRatingsStr := strconv.Itoa(totalNumberOfRatings)

	update := bson.M{
		"$push": bson.M{
			"partner_reviews": incomingReview,
		},
		"$set": bson.M{
			"average_ratings": newAverageRating,
			"total_ratings":   totalNumberOfRatingsStr,
		},
	}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	var updatedPartner Partner
	err = updatedDoc.Decode(&updatedPartner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	data := Data{
		Partner: updatedPartner,
	}

	responseBody := RegistrationResponse{
		Message: "Partner Review Updated successfully",
		Data:    data,
		Status:  "Success",
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

//GET PARTNER REVIEWS

func handleGetPartnerReviews(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Get partner reviews called")

	type DataPartnerReviews struct {
		Partner_reviews []PartnerReviewObject `json:"partner_reviews"`
	}

	type PartnerReviewsResponse struct {
		Message string `json:"message"`

		Data DataPartnerReviews `json:"data"`

		Status string `json:"status"`
	}

	// Get partner ID from request parameters
	partnerID := r.URL.Query().Get("partner_id")
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Partner ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific partner from MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	var partner Partner
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&partner)
	if err != nil {
		response := map[string]string{"message": "Partner not found", "status": "0"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Return only the reviews

	data := DataPartnerReviews{

		// Partner_ID: storedUser.ID.Hex(),
		Partner_reviews: partner.PartnerReviews,
		// Token:      token,
	}

	responseBody := PartnerReviewsResponse{
		Message: "Partner Reviews Retrieved Succesfully",
		Data:    data,
		Status:  "Success",
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

}

///ADD CUSTOMER REVIEWS

//ADD PARTNER REVIEW

// func handleAddCustomerReview(w http.ResponseWriter, r *http.Request) {
// 	// Get the partner ID from the request parameters
// 	fmt.Println("Handle add partner review is called")
// 	params := mux.Vars(r)
// 	partnerID := params["id"]

// 	// Convert the partner ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(partnerID)
// 	if err != nil {
// 		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Parse JSON data from the request
// 	decoder := json.NewDecoder(r.Body)
// 	var incomingReview CustomerReviewObject
// 	err = decoder.Decode(&incomingReview)
// 	if err != nil {
// 		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
// 		return
// 	}

// 	incomingReview.CreatedAt = ConvertToNairobiTime()
// 	incomingReview.ReviewID = generateAlphanumeric()

// 	// Update the specific records in MongoDB
// 	collection := client.Database(dbName).Collection(customer_collection)
// 	filter := bson.M{"_id": objID}

// 	// Fetch current reviews to calculate the new average rating
// 	var currentPartner Partner
// 	err = collection.FindOne(context.Background(), filter).Decode(&currentPartner)
// 	if err != nil {
// 		fmt.Println("Error", err)
// 		http.Error(w, "Error fetching current reviews", http.StatusInternalServerError)
// 		return
// 	}

// 	// Append the new review to the current reviews
// 	currentPartner.PartnerReviews = append(currentPartner.PartnerReviews, incomingReview)

// 	// Calculate the new average rating
// 	totalRatings := 0.0
// 	for _, review := range currentPartner.PartnerReviews {
// 		rating, err := strconv.ParseFloat(review.Rating, 64)
// 		if err != nil {
// 			http.Error(w, "Error parsing rating", http.StatusInternalServerError)
// 			return
// 		}
// 		totalRatings += rating
// 	}
// 	newAverageRating := totalRatings / float64(len(currentPartner.PartnerReviews))
// 	totalNumberOfRatings := len(currentPartner.PartnerReviews)

// 	totalNumberOfRatingsStr := strconv.Itoa(totalNumberOfRatings)

// 	update := bson.M{
// 		"$push": bson.M{
// 			"partner_reviews": incomingReview,
// 		},
// 		"$set": bson.M{
// 			"average_ratings": newAverageRating,
// 			"total_ratings":   totalNumberOfRatingsStr,
// 		},
// 	}

// 	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
// 	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

// 	// Check if an error occurred during the update
// 	if updatedDoc.Err() != nil {
// 		responseBody := ErrorResponse{
// 			Message: "Error updating record",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		fmt.Println("Error updating record:", updatedDoc.Err())
// 		return
// 	}

// 	var updatedPartner Partner
// 	err = updatedDoc.Decode(&updatedPartner)
// 	if err != nil {
// 		// Handle decoding error
// 		fmt.Println("Error decoding updated document:", err)
// 		return
// 	}

// 	data := Data{
// 		Partner: updatedPartner,
// 	}

// 	responseBody := RegistrationResponse{
// 		Message: "Partner Review Updated successfully",
// 		Data:    data,
// 		Status:  "Success",
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)
// }

//GET PARTNER REVIEWS

// func handleGetCustomerReviews(w http.ResponseWriter, r *http.Request) {

// 	fmt.Println("Get partner reviews called")

// 	type DataPartnerReviews struct {
// 		Partner_reviews []PartnerReviewObject `json:"partner_reviews"`
// 	}

// 	type PartnerReviewsResponse struct {
// 		Message string `json:"message"`

// 		Data DataPartnerReviews `json:"data"`

// 		Status string `json:"status"`
// 	}

// 	// Get partner ID from request parameters
// 	partnerID := r.URL.Query().Get("partner_id")
// 	objID, err := primitive.ObjectIDFromHex(partnerID)
// 	if err != nil {
// 		http.Error(w, "Invalid Partner ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve the specific partner from MongoDB
// 	collection := client.Database(dbName).Collection(partner_collection)
// 	var partner Partner
// 	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&partner)
// 	if err != nil {
// 		response := map[string]string{"message": "Partner not found", "status": "0"}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}

// 	// Return only the reviews

// 	data := DataPartnerReviews{

// 		// Partner_ID: storedUser.ID.Hex(),
// 		Partner_reviews: partner.PartnerReviews,
// 		// Token:      token,
// 	}

// 	responseBody := PartnerReviewsResponse{
// 		Message: "Partner Reviews Retrieved Succesfully",
// 		Data:    data,
// 		Status:  "Success",
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)

// }
