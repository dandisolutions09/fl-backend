package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// func handleAddLocation(w http.ResponseWriter, r *http.Request) {

// 	fmt.Println("Update Record Status!")
// 	params := mux.Vars(r)
// 	partnerID := params["id"]
// 	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

// 	fmt.Println("record id", partnerID)

// 	// Convert the facility ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(partnerID)
// 	if err != nil {
// 		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Decode the JSON request body into a Person struct
// 	var updateData Coordinates
// 	// if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
// 	// 	http.Error(w, "Failed to decode request body", http.StatusBadRequest)
// 	// 	return
// 	// }

// 	if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
// 		//http.Error(w, "Failed to decode request body", http.StatusBadRequest)

// 		fmt.Println("ERR", err)

// 		responseBody := ErrorResponse{
// 			Message: "Failed to decode request body",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return
// 	}

// 	if updateData.Latitude == 0 || updateData.Longitude == 0 {
// 		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

// 		responseBody := ErrorResponse{
// 			Message: "Missing required fields: Latitude or Longitude",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		return

// 	}
// 	fmt.Println("updatedData", updateData)

// 	// point := bson.M{
//     //     "type":        "Point",
//     //     "coordinates": []float32{updateData.Longitude, updateData.Latitude},
//     // }

// 	// Partner

// 	// Update the status of the record at the specified index in MongoDB
// 	collection := client.Database(dbName).Collection(partner_collection)
// 	filter := bson.M{"_id": objID}

// 	// Define the update operation to update multiple fields
// 	update := bson.M{
// 		"$set": bson.M{
// 			"location.latitude":  updateData.Latitude,
// 			"location.longitude": updateData.Longitude,
// 			"location.address":   updateData.Address,
// 		},
// 	}

// 	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
// 	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

// 	// Check if an error occurred during the update
// 	if updatedDoc.Err() != nil {
// 		// Handle error
// 		// ...

// 		// Return error response
// 		responseBody := ErrorResponse{
// 			Message: "Error updating record",
// 			Status:  "Error",
// 		}

// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(responseBody)
// 		fmt.Println("Error updating record:", updatedDoc.Err())
// 		return
// 	}

// 	//fmt.Println("updated doc", updatedDoc)

// 	var updated_partner Partner

// 	err = updatedDoc.Decode(&updated_partner)
// 	if err != nil {
// 		// Handle decoding error
// 		fmt.Println("Error decoding updated document:", err)
// 		return
// 	}

// 	// Encode and send the updated document as the response
// 	// w.Header().Set("Content-Type", "application/json")
// 	// json.NewEncoder(w).Encode(updated_partner)

// 	responseBody := RegistrationResponse{
// 		Message: "Partner Location updated successfully",
// 		Partner: updated_partner,
// 		Status:  "Success",
// 	}

// 	// Respond with a success message
// 	//response := map[string]string{"message": "Partner Availability updated successfully", "status:": "1"}
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(responseBody)

// }

func handleAddLocation(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	partnerID := params["id"]
	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", partnerID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Decode the JSON request body into a Coordinates struct
	var updateData Coordinates
	if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
		// Handle JSON decoding error
		responseBody := ErrorResponse{
			Message: "Failed to decode request body",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	if updateData.Latitude == 0 || updateData.Longitude == 0 {
		// Return error response for missing fields
		responseBody := ErrorResponse{
			Message: "Missing required fields: Latitude or Longitude",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}
	fmt.Println("updatedData", updateData)

	// Construct GeoJSON point
	point := bson.M{
		"type":        "Point",
		"coordinates": []float32{updateData.Longitude, updateData.Latitude},
	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	filter := bson.M{"_id": objID}

	// Define the update operation to update multiple fields
	update := bson.M{
		"$set": bson.M{
			"location.latitude":  updateData.Latitude,
			"location.longitude": updateData.Longitude,
			"location.address":   updateData.Address,
			"location_point":     point, // Add location_point field with GeoJSON point
		},
	}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	var updated_partner Partner

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// responseBody := RegistrationResponse{
	// 	Message: "Partner Location updated successfully",
	// 	Partner: updated_partner,
	// 	Status:  "Success",
	// }
	initializePartnersStatus()
	initializeCustomerStatus()


	data := Data{

		// Partner_ID: storedUser.ID.Hex(),
		Partner: updated_partner,
		// Token:      token,
	}

	responseBody := RegistrationResponse{
		Message: "Partner Location updated successfully",
		// Partner_ID: storedUser.ID.Hex(),
		// Partner:    retrievedPartner,
		// Token:      token,
		Data:   data,
		Status: "Success",
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

func handleAddCustomerLocation(w http.ResponseWriter, r *http.Request) {

	type CustomerLocationResponse struct {
		Message string `json:"message"`
		//Token      string   `json:"token,omitempty"`
		Customer Customer `json:"partner,omitempty"`
		//Partner_ID string   `json:"partner_id,omitempty"`

		Status string `json:"status,omitempty"`
	}

	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	partnerID := params["id"]
	//partnerID := params["partnerID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", partnerID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(partnerID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Decode the JSON request body into a Person struct
	var updateData Coordinates
	// if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
	// 	http.Error(w, "Failed to decode request body", http.StatusBadRequest)
	// 	return
	// }

	if err := json.NewDecoder(r.Body).Decode(&updateData); err != nil {
		//http.Error(w, "Failed to decode request body", http.StatusBadRequest)

		fmt.Println("ERR", err)

		responseBody := ErrorResponse{
			Message: "Failed to decode request body",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return
	}

	if updateData.Latitude == 0 || updateData.Longitude == 0 {
		//http.Error(w, "Missing required fields: email, password, first_name, or last_name", http.StatusBadRequest)

		responseBody := ErrorResponse{
			Message: "Missing required fields: Latitude or Longitude",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		return

	}
	fmt.Println("updatedData", updateData)

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection(customer_collection)
	filter := bson.M{"_id": objID}

	// Define the update operation to update multiple fields
	update := bson.M{
		"$set": bson.M{
			"location.latitude":  updateData.Latitude,
			"location.longitude": updateData.Longitude,
			"location.address":   updateData.Address,
		},
	}

	options := options.FindOneAndUpdate().SetReturnDocument(options.After)
	updatedDoc := collection.FindOneAndUpdate(context.Background(), filter, update, options)

	// Check if an error occurred during the update
	if updatedDoc.Err() != nil {
		// Handle error
		// ...

		// Return error response
		responseBody := ErrorResponse{
			Message: "Error updating record",
			Status:  "Error",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseBody)
		fmt.Println("Error updating record:", updatedDoc.Err())
		return
	}

	//fmt.Println("updated doc", updatedDoc)

	var updated_partner Customer

	err = updatedDoc.Decode(&updated_partner)
	if err != nil {
		// Handle decoding error
		fmt.Println("Error decoding updated document:", err)
		return
	}

	// Encode and send the updated document as the response
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(updated_partner)

	responseBody := CustomerLocationResponse{
		Message:  "Customer Location updated successfully",
		Customer: updated_partner,
		Status:   "1",
	}

	// Respond with a success message
	//response := map[string]string{"message": "Partner Availability updated successfully", "status:": "1"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)

}
