package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

type Booking struct {
	BookingDate string `bson:"booking_date"`
	BookingTime string `bson:"booking_time"`
	Status      string `bson:"status"`
}

type AnalyticsResponse struct {
	TotalCustomers    string `json:"customers,omitempty"`
	TotalProviders    string `json:"providers,omitempty"`
	TotalAdmins       string `json:"admins,omitempty"`
	ActiveCustomers   string `json:"active_customers,omitempty"`
	ActiveProviders   string `json:"active_providers,omitempty"`
	ActiveAdmins      string `json:"active_admins,omitempty"`
	InActiveCustomers string `json:"inactive_customers,omitempty"`
	InActiveProviders string `json:"inactive_providers,omitempty"`
	InActiveAdmins    string `json:"inactive_admins,omitempty"`
	Companies         string `json:"companies,omitempty"`
	TotalJobs         string `json:"total_jobs,omitempty"`
	OngoingJobs       string `json:"ongoing_jobs,omitempty"`
	CancelledJobs     string `json:"cancelled_jobs,omitempty"`
	CompletedJobs     string `json:"completed_jobs,omitempty"`

	UpcomingJobs string `json:"upcoming_jobs,omitempty"`
	Status       string `json:"status,omitempty"`
}

func handleGetStatistics(w http.ResponseWriter, r *http.Request) {
	cusColl := client.Database(dbName).Collection(customer_collection)
	providerColl := client.Database(dbName).Collection(partner_collection)
	adminColl := client.Database(dbName).Collection(admin_collection)
	bookingColl := client.Database(dbName).Collection(booking_collection)

	fmt.Println("Get all customers called")
	filter := bson.M{}

	// Retrieve filter from query parameters
	filterType := r.URL.Query().Get("filter")
	if filterType == "" {
		filterType = "all"
	}

	// Count documents
	totalCustomers, err := cusColl.CountDocuments(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error counting documents", http.StatusInternalServerError)
		return
	}

	totalProviders, err := providerColl.CountDocuments(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error counting documents", http.StatusInternalServerError)
		return
	}

	totalAdmins, err := adminColl.CountDocuments(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error counting documents", http.StatusInternalServerError)
		return
	}

	// Fetch all bookings
	var bookings []Booking
	cursor, err := bookingColl.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error fetching bookings", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())
	if err = cursor.All(context.Background(), &bookings); err != nil {
		http.Error(w, "Error parsing bookings", http.StatusInternalServerError)
		return
	}

	// Filter bookings in Go
	var filteredBookings []Booking
	now := time.Now()
	if filterType == "today" {
		startOfDay := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
		for _, booking := range bookings {
			bookingDate, err := time.Parse("1/2/2006", booking.BookingDate)
			if err != nil {
				continue
			}
			if bookingDate.After(startOfDay) && bookingDate.Before(now) {
				filteredBookings = append(filteredBookings, booking)
			}
		}
	} else {
		filteredBookings = bookings
	}

	// Count filtered bookings based on status
	var ongoingBookings, cancelledBookings, completedBookings int64
	for _, booking := range filteredBookings {
		switch booking.Status {
		case "ONGOING":
			ongoingBookings++
		case "CANCELLED":
			cancelledBookings++
		case "COMPLETED":
			completedBookings++
		}
	}
	allBookings := int64(len(filteredBookings))

	// Count active and inactive customers, providers, admins
	filterActive := bson.M{"status": "ACTIVE"}
	activeAdmins, err := adminColl.CountDocuments(context.Background(), filterActive)
	if err != nil {
		http.Error(w, "Error counting active admins", http.StatusInternalServerError)
		return
	}

	activeCustomers, err := cusColl.CountDocuments(context.Background(), filterActive)
	if err != nil {
		http.Error(w, "Error counting active customers", http.StatusInternalServerError)
		return
	}

	activeProviders, err := providerColl.CountDocuments(context.Background(), filterActive)
	if err != nil {
		http.Error(w, "Error counting active providers", http.StatusInternalServerError)
		return
	}

	filterInactive := bson.M{"status": "INACTIVE"}
	inactiveAdmins, err := adminColl.CountDocuments(context.Background(), filterInactive)
	if err != nil {
		http.Error(w, "Error counting inactive admins", http.StatusInternalServerError)
		return
	}

	inactiveCustomers, err := cusColl.CountDocuments(context.Background(), filterInactive)
	if err != nil {
		http.Error(w, "Error counting inactive customers", http.StatusInternalServerError)
		return
	}

	inactiveProviders, err := providerColl.CountDocuments(context.Background(), filterInactive)
	if err != nil {
		http.Error(w, "Error counting inactive providers", http.StatusInternalServerError)
		return
	}

	filterUpcoming := bson.M{"status": "UPCOMING"}
	upcomingBookings, err := bookingColl.CountDocuments(context.Background(), filterUpcoming)
	if err != nil {
		http.Error(w, "Error counting inactive admins", http.StatusInternalServerError)
		return
	}

	responseBody := AnalyticsResponse{
		TotalCustomers:    strconv.FormatInt(totalCustomers, 10),
		TotalProviders:    strconv.FormatInt(totalProviders, 10),
		TotalAdmins:       strconv.FormatInt(totalAdmins, 10),
		ActiveCustomers:   strconv.FormatInt(activeCustomers, 10),
		ActiveProviders:   strconv.FormatInt(activeProviders, 10),
		ActiveAdmins:      strconv.FormatInt(activeAdmins, 10),
		InActiveCustomers: strconv.FormatInt(inactiveCustomers, 10),
		InActiveProviders: strconv.FormatInt(inactiveProviders, 10),
		InActiveAdmins:    strconv.FormatInt(inactiveAdmins, 10),
		OngoingJobs:       strconv.FormatInt(ongoingBookings, 10),
		CancelledJobs:     strconv.FormatInt(cancelledBookings, 10),
		CompletedJobs:     strconv.FormatInt(completedBookings, 10),
		UpcomingJobs:      strconv.FormatInt(upcomingBookings, 10),
		TotalJobs:         strconv.FormatInt(allBookings, 10),
		Status:            "Success",
	}

	fmt.Println("completed jobs ", responseBody.CompletedJobs)

	fmt.Println("cancelled jobs ", responseBody.CompletedJobs)
	fmt.Println("ongoing jobs ", responseBody.OngoingJobs)

	// Respond with the retrieved records in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}
