package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"

	//"strconv"

	//	"strings"
	"time"

	//"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func handleGetPartnerTimeSlots(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get admin by ID has been called....")

	type TimeSlotsResponse struct {
		Status    string   `json:"status"`
		Timeslots []string `json:"timeslots" bson:"timeslots"`
	}

	// Get the admin ID from the request parameters
	//params := mux.Vars(r)
	//userID := params["id"]

	userID := r.URL.Query().Get("id")
	Weekday := r.URL.Query().Get("day")

	fmt.Println("Passed user ID:", userID)

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific admin from MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	var admin Partner
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&admin)
	if err != nil {
		response := map[string]string{"message": "Partner not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	fmt.Println(reflect.TypeOf(admin))
	timeslots := []string{}

	if admin.PartnerAvailability.Monday.OpeningTime != "" {

		timeslots = append(timeslots, admin.PartnerAvailability.Monday.OpeningTime)

	} else if admin.PartnerAvailability.Monday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Tuesday.OpeningTime)

	} else if admin.PartnerAvailability.Tuesday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Tuesday.OpeningTime)

	} else if admin.PartnerAvailability.Tuesday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Tuesday.OpeningTime)

	} else if admin.PartnerAvailability.Wednesday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Wednesday.OpeningTime)

	} else if admin.PartnerAvailability.Thursday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Thursday.OpeningTime)

	} else if admin.PartnerAvailability.Friday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Friday.OpeningTime)

	} else if admin.PartnerAvailability.Friday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Friday.ClosingTime)

	} else if admin.PartnerAvailability.Saturday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Saturday.OpeningTime)

	} else if admin.PartnerAvailability.Saturday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Saturday.ClosingTime)

	} else if admin.PartnerAvailability.Sunday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Sunday.OpeningTime)

	} else if admin.PartnerAvailability.Sunday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Friday.ClosingTime)

	}

	fmt.Println("TimeSlots", timeslots)

	// Example usage
	///	openingTime := "7:00 AM"
	//	closingTime := "6:00 PM"

	fmt.Print("opening time", admin.PartnerAvailability.Tuesday.OpeningTime)
	fmt.Print("closing time", admin.PartnerAvailability.Tuesday.ClosingTime)

	timeSlots := map[string]struct {
		OpeningTime string
		ClosingTime string
	}{
		"Monday":    {admin.PartnerAvailability.Monday.OpeningTime, admin.PartnerAvailability.Monday.ClosingTime},
		"Tuesday":   {admin.PartnerAvailability.Tuesday.OpeningTime, admin.PartnerAvailability.Tuesday.ClosingTime},
		"Wednesday": {admin.PartnerAvailability.Wednesday.OpeningTime, admin.PartnerAvailability.Wednesday.ClosingTime},
		"Thursday":  {admin.PartnerAvailability.Thursday.OpeningTime, admin.PartnerAvailability.Thursday.ClosingTime},
		"Friday":    {admin.PartnerAvailability.Friday.OpeningTime, admin.PartnerAvailability.Friday.ClosingTime},
		"Saturday":  {admin.PartnerAvailability.Saturday.OpeningTime, admin.PartnerAvailability.Saturday.ClosingTime},
		"Sunday":    {admin.PartnerAvailability.Sunday.OpeningTime, admin.PartnerAvailability.Sunday.ClosingTime},
	}

	timeSlotSlice := getCurrentDayTimeSlots(Weekday, timeSlots)
	fmt.Println("Today's Time Slots:", timeSlotSlice)

	timeSlotsResponse := TimeSlotsResponse{Timeslots: timeSlotSlice, Status: "1"}

	//printIncrementedTimes(openingTime, closingTime)

	// Respond with the retrieved admin in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(timeSlotsResponse)
}

///

func getCurrentDayTimeSlots(day string, timeSlots map[string]struct {
	OpeningTime string
	ClosingTime string
}) []string {
	// Get current day
	//currentDay := time.Now().Weekday().String()
	currentDay := day

	// Check if the current day exists in the timeSlots map
	if times, ok := timeSlots[currentDay]; ok {
		fmt.Printf("Today's (%s) Time Slots:\n", currentDay)
		return getIncrementedTimes(times.OpeningTime, times.ClosingTime)
	} else {
		fmt.Printf("No time slots available for %s\n", currentDay)
		return nil
	}
}

func getIncrementedTimes(openingTime, closingTime string) []string {
	var timeSlots []string

	// Parse opening time
	opening, err := time.Parse("3:04 PM", openingTime)
	if err != nil {
		fmt.Println("Error parsing opening time:", err)
		return nil
	}

	// Parse closing time
	closing, err := time.Parse("3:04 PM", closingTime)
	if err != nil {
		fmt.Println("Error parsing closing time:", err)
		return nil
	}

	// Add 15 minutes to opening time until closing time
	for opening.Before(closing) {
		// Add 15 minutes
		opening = opening.Add(15 * time.Minute)
		// Append incremented time to slice
		timeSlots = append(timeSlots, opening.Format("3:04 PM"))
	}

	return timeSlots
}

func handGetAvailableTimeslots(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get admin by ID has been called....")

	type DataAvailableTimeSlots struct {
		Timeslots []string `json:"available_timeslots" bson:"available_timeslots"`
	}

	type AvailableTimeslotsResponse struct {
		Message string `json:"message"`

		Data DataAvailableTimeSlots `json:"data"`

		Status string `json:"status"`
	}

	// CartItem represents the structure of each item in the cart
	type CartItem struct {
		ServiceDuration string `json:"service_duration"`
	}

	// Booking represents the structure of each booking in the response
	type Booking struct {
		BookingDate string     `json:"booking_date"`
		BookingTime string     `json:"booking_time"`
		Cart        []CartItem `json:"cart"`
	}

	// Response represents the structure of the JSON response
	type Response struct {
		UpcomingBookings []Booking `json:"upcoming_bookings"`
	}

	type TimeSlotsResponse struct {
		Status    string   `json:"status"`
		Timeslots []string `json:"timeslots" bson:"timeslots"`
	}

	// Get the admin ID from the request parameters
	//params := mux.Vars(r)
	//userID := params["id"]

	userID := r.URL.Query().Get("id")
	// Weekday := r.URL.Query().Get("day")

	bookingDate := r.URL.Query().Get("bookingDate")
	//bookingTime := r.URL.Query().Get("bookingTime")

	fmt.Println("Passed user ID:", userID)

	// Convert the user ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	Weekday, err := getDayOfWeek(bookingDate)
	if err != nil {
		fmt.Println("Error parsing date:", err)
		return
	}

	fmt.Println("extracted day", Weekday)

	// Retrieve the specific admin from MongoDB
	collection := client.Database(dbName).Collection(partner_collection)
	var admin Partner
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&admin)
	if err != nil {
		response := map[string]string{"message": "Partner not found", "status": "0"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	fmt.Println(reflect.TypeOf(admin))
	timeslots := []string{}

	if admin.PartnerAvailability.Monday.OpeningTime != "" {

		timeslots = append(timeslots, admin.PartnerAvailability.Monday.OpeningTime)

	} else if admin.PartnerAvailability.Monday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Tuesday.OpeningTime)

	} else if admin.PartnerAvailability.Tuesday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Tuesday.OpeningTime)

	} else if admin.PartnerAvailability.Tuesday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Tuesday.OpeningTime)

	} else if admin.PartnerAvailability.Wednesday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Wednesday.OpeningTime)

	} else if admin.PartnerAvailability.Thursday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Thursday.OpeningTime)

	} else if admin.PartnerAvailability.Friday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Friday.OpeningTime)

	} else if admin.PartnerAvailability.Friday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Friday.ClosingTime)

	} else if admin.PartnerAvailability.Saturday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Saturday.OpeningTime)

	} else if admin.PartnerAvailability.Saturday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Saturday.ClosingTime)

	} else if admin.PartnerAvailability.Sunday.OpeningTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Sunday.OpeningTime)

	} else if admin.PartnerAvailability.Sunday.ClosingTime != "" {
		timeslots = append(timeslots, admin.PartnerAvailability.Friday.ClosingTime)

	}

	fmt.Println("TimeSlots", timeslots)

	// Example usage
	///	openingTime := "7:00 AM"
	//	closingTime := "6:00 PM"

	fmt.Print("opening time", admin.PartnerAvailability.Tuesday.OpeningTime)
	fmt.Print("closing time", admin.PartnerAvailability.Tuesday.ClosingTime)

	timeSlots := map[string]struct {
		OpeningTime string
		ClosingTime string
	}{
		"Monday":    {admin.PartnerAvailability.Monday.OpeningTime, admin.PartnerAvailability.Monday.ClosingTime},
		"Tuesday":   {admin.PartnerAvailability.Tuesday.OpeningTime, admin.PartnerAvailability.Tuesday.ClosingTime},
		"Wednesday": {admin.PartnerAvailability.Wednesday.OpeningTime, admin.PartnerAvailability.Wednesday.ClosingTime},
		"Thursday":  {admin.PartnerAvailability.Thursday.OpeningTime, admin.PartnerAvailability.Thursday.ClosingTime},
		"Friday":    {admin.PartnerAvailability.Friday.OpeningTime, admin.PartnerAvailability.Friday.ClosingTime},
		"Saturday":  {admin.PartnerAvailability.Saturday.OpeningTime, admin.PartnerAvailability.Saturday.ClosingTime},
		"Sunday":    {admin.PartnerAvailability.Sunday.OpeningTime, admin.PartnerAvailability.Sunday.ClosingTime},
	}

	timeSlotSlice := getCurrentDayTimeSlots(Weekday, timeSlots)
	fmt.Println("Today's Time Slots:", timeSlotSlice)

	//url := "http://localhost:8080/partner-upcoming-bookings/" + userID

	token, _ := createToken("u.First_name" + "u.Last_name")

	print("token", token)
	url := fmt.Sprintf("http://localhost:8080/partner-upcoming-bookings?id=%s&token=%s", userID, token)

	response, err := getUpcomingBookings(url, token)
	if err != nil {
		fmt.Println("Error making GET request:", err)
		return
	}

	// fmt.Println("response", response)

	// Parse the JSON response
	var res Response
	err = json.Unmarshal([]byte(response), &res)
	if err != nil {
		fmt.Println("Error parsing JSON response:", err)
		return
	}

	//var allMinusFilteredTimeSlots []string

	var similarBookingsSlice []Booking

	for _, booking := range res.UpcomingBookings {

		fmt.Printf("Booking Date: %s, Booking Time: %s\n", booking.BookingDate, booking.BookingTime)
		//totalDuration := 0

		// if booking.BookingDate != bookingDate {
		// 	fmt.Println(" dates are not equal")
		// 	fmt.Println("booking", booking)

		// }

		if booking.BookingDate != bookingDate {
			fmt.Println(" --->dates not are equal")
			fmt.Println("booking", booking)

		} else if booking.BookingDate == bookingDate {
			fmt.Println("---> dates are  equal")
			fmt.Println("booking", booking)

			similarBookingsSlice = append(similarBookingsSlice, booking)

		} // end of date comparison if statement

	}

	fmt.Println("similar boookings slice", similarBookingsSlice)

	// totalDuration := 0
	// var allMinusFilteredTimeSlots []string

	fmt.Println("ALL TIMESLOTS", timeSlotSlice)

	// Initialize allMinusFilteredTimeSlots outside the loop
	allMinusFilteredTimeSlots := make([]string, 0, len(timeSlotSlice))

	// Initialize allMinusFilteredTimeSlots outside the loop

	for _, bookingslice := range similarBookingsSlice {
		fmt.Println("Booking slice", bookingslice.BookingTime)

		totalDuration := 0 // Reset total duration for each bookingslice

		// Calculate total service duration
		for _, item := range bookingslice.Cart {
			duration, err := strconv.Atoi(item.ServiceDuration)
			if err != nil {
				fmt.Println("Error converting service duration to integer:", err)
				continue
			}
			totalDuration += duration
		}
		fmt.Printf("Total Service Duration: %d minutes\n", totalDuration)

		// Parse booking time into a time.Time object
		bookingTime, err := time.Parse("3:04 PM", bookingslice.BookingTime)
		if err != nil {
			fmt.Println("Error parsing booking time:", err)
			continue
		}

		// Add total service duration to booking time
		endBookingTime := bookingTime.Add(time.Duration(totalDuration) * time.Minute)
		fmt.Println("End booking time:", endBookingTime)

		// Filter and store timeslots excluding those falling between bookingTime and endBookingTime
		var filteredTimeSlotsSlice []string
		for _, slot := range timeSlotSlice {
			slotTime, err := time.Parse("3:04 PM", slot)
			if err != nil {
				fmt.Println("Error parsing timeslot:", err)
				continue
			}
			if slotTime.Before(bookingTime) || slotTime.After(endBookingTime) {
				filteredTimeSlotsSlice = append(filteredTimeSlotsSlice, slot)
				//fmt.Println("SLOT", slot)
			}
		}

		// Create a map for faster lookup
		filteredMap := make(map[string]bool)
		for _, slot := range filteredTimeSlotsSlice {
			filteredMap[slot] = true
		}

		// Accumulate timeslots to allMinusFilteredTimeSlots
		for _, slot := range timeSlotSlice {
			if !filteredMap[slot] {
				allMinusFilteredTimeSlots = append(allMinusFilteredTimeSlots, slot)
			}
		}

		// Print results for each bookingslice
		//fmt.Println("Remaining Time Slots:", filteredTimeSlotsSlice)
		//fmt.Println("Filtered Out Slots:", allMinusFilteredTimeSlots)
	}

	// Create a map for allMinusFilteredTimeSlots for faster lookup
	allMinusFilteredMap := make(map[string]bool)
	for _, slot := range allMinusFilteredTimeSlots {
		allMinusFilteredMap[slot] = true
	}

	// Create a new slice with all timeslots except those in allMinusFilteredTimeSlots
	var newSlots []string
	for _, slot := range timeSlotSlice {
		if !allMinusFilteredMap[slot] {
			newSlots = append(newSlots, slot)
		}
	}

	// Print the new slots

	fmt.Println("New Slots:", newSlots)

	data := DataAvailableTimeSlots{

		// Partner_ID: storedUser.ID.Hex(),
		Timeslots: newSlots,
		// Token:      token,
	}

	responseBody := AvailableTimeslotsResponse{
		Message: "Available Time Slots",

		Data:   data,
		Status: "Success",
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}

//END OF THE FUNCTION

func getDayOfWeek(dateStr string) (string, error) {
	// Define the layout for the date string
	layout := "1/2/2006"

	// Parse the date string
	date, err := time.Parse(layout, dateStr)
	if err != nil {
		return "", err
	}

	// Get the day of the week
	dayOfWeek := date.Weekday().String()

	return dayOfWeek, nil
}

func getUpcomingBookings(url string, token string) (string, error) {
	// Create a new request using http
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	// Add the Bearer token to the request header
	req.Header.Add("Authorization", "Bearer "+token)

	// Perform the GET request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	// Return the response body as a string
	return string(body), nil
}
