// mongo.go
package main
import (
    // "context"
    // "go.mongodb.org/mongo-driver/mongo"
    // "go.mongodb.org/mongo-driver/mongo/options"
)
// DBName Database name.
// const DBName = "geolocation"
// var (
//     conn       *mongo.Client
//     // ctx        = context.Background()
//     connString string = "mongodb://127.0.0.1:27017/glottery"
// )
// Database collections.
var (
    PointCollection = "points"
)
// // createDBSession Create a new connection with the database.
// func createDBSession() error {
//     var err error
//     conn, err = mongo.Connect(ctx, options.Client().
//         ApplyURI(connString))
//     if err != nil {
//         return err
//     }
//     err = conn.Ping(ctx, nil)
//     if err != nil {
//         return err
//     }
//     return nil
// }