package main

import (
	"fmt"
	"strconv"
)

func CalculateTotalAmount(filteredBookings []BookingObject) int {
	totalAmount := 0

	// Iterate over each booking
	for _, obj := range filteredBookings {
		// Check if the booking year matches the incomingYear
		// Iterate over items in the cart
		for _, item := range obj.Cart {
			// Convert item price to integer
			price, err := strconv.Atoi(item.Price)
			if err != nil {
				fmt.Println("Error converting price to integer:", err)
				continue
			}
			// Add item price to totalAmount
			totalAmount += price
		}

	}

	return totalAmount
}
